# NSI_terminale


Dépôt pour l'organisation, les activités, les exercices, les évaluations et 
les TP de l'enseignement de NSI en classe de terminale.

- organisation : chapitrage, progression, gestion des projets
- évaluations : QCM et évaluations sommatives
- TP : notebook Jupyter (pour Capytale)
- exercices : fiches d'exercices, questions Moodle
- activités : fiches d'activités (mise en situation , débranchée)
-  ...