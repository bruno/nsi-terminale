"""
Implémentation de Calendrier
qui permet de manipuler jour / mois / années
"""
from doctest import testmod


Calendrier = dict[str, int]


def calendrier_creer(jour: int, mois: int, annee: int) -> Calendrier:
    """ Structure de calendrier qui gère les jours, les mois et les années.

    Parameters
    ----------
    jour : (int) nombre entier compris entre 1 et 31
    mois : (int) nombre entier compris entre 1 et 12 
    annee : (int) nombre entier

    Returns
    -------
    (Calendrier) structure de date

    Examples
    --------
        >>> calendrier_creer(5,6,1977)
        {'jour': 5, 'mois': 6, 'annee': 1977}
        >>> calendrier_creer(31,12,1900)
        {'jour': 31, 'mois': 12, 'annee': 1900}
    """
    assert 0 < jour and jour < 32
    assert 0 < mois and mois < 13
    return {'jour': jour, 'mois': mois, 'annee': annee}


def calendrier_get_jour(date: Calendrier) -> int:
    """Renvoie le jour d'une date.

    Parameters
    ----------
    date : (Calendrier)

    Returns
    -------
    (int) jour sous la forme d'un nombre entier

    Examples
    --------
        >>> calendrier_get_jour(calendrier_creer(5,6,1977))
        5
        >>> calendrier_get_jour(calendrier_creer(31,12,1900))
        31
    """
    return date['jour']


def calendrier_get_mois(date: Calendrier) -> int:
    """Renvoie le mois d'une date.

    Parameters
    ----------
    date : (Calendrier)

    Returns
    -------
    (int) mois sous la forme d'un nombre entier

    Examples
    --------
        >>> calendrier_get_mois(calendrier_creer(5,6,1977))
        6
        >>> calendrier_get_mois(calendrier_creer(31,12,1900))
        12
    """
    return date['mois']


def calendrier_get_annee(date: Calendrier) -> int:
    """Renvoie l'année d'une date.

    Parameters
    ----------
    date : (Calendrier)

    Returns
    -------
    (int) année sous la forme d'un nombre entier

    Examples
    --------
        >>> calendrier_get_annee(calendrier_creer(5,6,1977))
        1977
        >>> calendrier_get_annee(calendrier_creer(31,12,1900))
        1900
    """
    return date['annee']


def calendrier_to_str(date: Calendrier) -> str:
    """ Renvoie une date de Calendrier sous la forme d'une chaîne de caractère.

    Parameters
    ----------
    date : (Calendrier) date à convertir

    Returns
    -------
    (str) chaîne sous la forme 'jj/mm/aaaa'. Par exemple 5/6/1977.

    Examples
    --------
        >>> d = calendrier_creer(5,6,1977)
        >>> calendrier_to_str(d)
        '5/6/1977'
        >>> calendrier_to_str(calendrier_creer(31,12,1900))
        '31/12/1900'
    """
    j: str = str(calendrier_get_jour(date))
    m: str = str(calendrier_get_mois(date))
    a: str = str(calendrier_get_annee(date))
    return j + "/" + m + "/" + a


testmod()


def calendrier_incremente_annee(date: Calendrier) -> Calendrier:
    """ Crée et renvoie une nouvelle date qui augmente de 1 année
    la date passée en argument sans la modifier.

    Parameters
    ----------
    date : (Calendrier) date de référence qui ne doit pas changer
    
    Returns
    -------
    (Calendrier) Nouvelle date un an de plus que la date de référence

    Examples
    --------
        >>> d_0 = calendrier_creer(5,6,1977)
        >>> d_1 = calendrier_incremente_annee(d_0)
        >>> calendrier_to_str(d_0)
        '5/6/1977'
        >>> calendrier_to_str(d_1)
        '5/6/1978'
        >>> d_0 = calendrier_creer(31,12,1900)
        >>> d_1 = calendrier_incremente_annee(d_0)
        >>> calendrier_to_str(d_0)
        '31/12/1900'
        >>> calendrier_to_str(d_1)
        '31/12/1901'
    """
    j: int = calendrier_get_jour(date)
    m: int = calendrier_get_mois(date)
    a: int = calendrier_get_annee(date)
    return calendrier_creer(j, m, a+1)


def calendrier_incremente_mois(date: Calendrier) -> Calendrier:
    """ Crée et renvoie une nouvelle date qui est égale à 
    la date de référence augmentée d'un mois.
    Si le mois était 12, alors il y a aussi un changement d'année.

    Parameters
    ----------
    date : (Calendrier) date de référence à ne pas modifier

    Returns
    -------
    (Calendrier) Nouvelle date un mois de plus que la date de référence
    
    Examples
    --------
    --------
        >>> d_0 = calendrier_creer(5,6,1977)
        >>> d_1 = calendrier_incremente_mois(d_0)
        >>> calendrier_to_str(d_0)
        '5/6/1977'
        >>> calendrier_to_str(d_1)
        '5/7/1977'
        >>> d_0 = calendrier_creer(31,12,1900)
        >>> d_1 = calendrier_incremente_mois(d_0)
        >>> calendrier_to_str(d_0)
        '31/12/1900'
        >>> calendrier_to_str(d_1)
        '31/1/1901'
    """
    j: int = calendrier_get_jour(date)
    m: int = calendrier_get_mois(date)
    a: int = calendrier_get_annee(date)
    if m < 12:
        return calendrier_creer(j, m+1, a)  # changement de mois
    else:  # cas du mois de décembre
        return calendrier_creer(j, 1, a+1)  # changement d'année


def calendrier_incremente_jour(date: Calendrier) -> Calendrier:
    """ Crée et renvoie une nouvelle date qui vaut
    1 jour de plus que la date de référence.
    Si c'est la fin d'un mois, alors, change le mois (et l'année si besoin).
    Ne prend pas en compte les années bissextiles.

    Parameters
    ----------
    date : (Calendrier) date de référence à ne pas modifier
    
    Examples
    --------
        >>> d_0 = calendrier_creer(5,6,1977)
        >>> d_1 = calendrier_incremente_jour(d_0)
        >>> calendrier_to_str(d_0)
        '5/6/1977'
        >>> calendrier_to_str(d_1)
        '6/6/1977'
        >>> d_0 = calendrier_creer(30,6,2000)
        >>> d_1 = calendrier_incremente_jour(d_0)
        >>> calendrier_to_str(d_0)
        '30/6/2000'
        >>> calendrier_to_str(d_1)
        '1/7/2000'
        >>> d_0 = calendrier_creer(31,12,1900)
        >>> d_1 = calendrier_incremente_jour(d_0)
        >>> calendrier_to_str(d_0)
        '31/12/1900'
        >>> calendrier_to_str(d_1)
        '1/1/1901'
    """
    j: int = calendrier_get_jour(date)
    m: int = calendrier_get_mois(date)
    a: int = calendrier_get_annee(date)
    if  (j == 28 and m == 2)                \
        or (j == 30 and m in [4,6,9,11])    \
        or (j == 31 and m in [1, 3, 5, 7, 8, 10, 12]):
            date_1 = calendrier_incremente_mois(date)  # augmentation d'un mois
            a = calendrier_get_annee(date_1)  # nouvelle année (éventuellement)
            m = calendrier_get_mois(date_1)  # nouveau mois
            return calendrier_creer(1,m,a)
    else:
        return calendrier_creer(j+1, m, a)


testmod()
