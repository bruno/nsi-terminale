Rubriques/Contenus :

- Structures de données : Interface et implémentation
- Structures de données : Programmation Orientée Objet
- Structures de données : Structures linéaires
- Structures de données : Arbres
- Structures de données : Graphes

- Bases de données : Modèle relationnel
- Bases de données : Base de données relationnelle
- Bases de données : Langage SQL
- Bases de données : Service de Gestion de Base de Données

- Architectures matérielles, systèmes d’exploitation et réseaux : Systèmes sur puce
- Architectures matérielles, systèmes d’exploitation et réseaux : Processus
- Architectures matérielles, systèmes d’exploitation et réseaux : Protocoles de routage
- Architectures matérielles, systèmes d’exploitation et réseaux : Sécurisation des communications

- Langages et programmation : Calculabilité, décidabilité
- Langages et programmation : Récursivité
- Langages et programmation : Modularité
- Langages et programmation : Paradigmes de programmation
- Langages et programmation : Mise au point

- Algorithmique : Algorithmes sur les arbres
- Algorithmique : Algorithmes sur les graphes
- Algorithmique : Diviser pour régner
- Algorithmique : Programmation dynamique
- Algorithmique : Recherche textuelle


Nombre de semaines :

- Rentrée (36) -> Automne (43) : 7
- Automne (45) -> Fin année (52) : 7
- Fin année (2) -> Hiver (9) : 7
- Hiver (11) -> Printemps (17) : 6
- Printemps (19) -> Été (24) : 5

Total : 32

