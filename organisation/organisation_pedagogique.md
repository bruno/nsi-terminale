---
title : Organisation pédagogique
author : Bruno Bourgine
tags :
 - pédagogie
 - organisation
---

## Intentions

Ce document vise à expliciter le fonctionnement de l'enseignement de NSI
en classe de terminale NSI durant l'année 2023-2024. Il explicite les choix 
d'organisation structurant les séquences pédagogiques en terme de découpage 
temporels, typologie d'activités et modalités d'évaluations.

## Découpage

Le programme d'enseignement a été réparti sur 24 séquences d'une durée de 1 à 2 semaines.
Ces séquences ont été regroupées par thématique afin de donner de la cohérence
à la progression annuelle. Ces thématiques sont :

- coder proprement,
- gérer des données,
- programmer efficacement,
- gérer des machines,
- simplifier les problèmes,
- sécuriser les données,
- résoudre des problèmes complexes.

Des évaluations sommatives sur copies sont prévues au mieux avant chaque vacances, 
en cas de calendrier trop contraint il y en aura a minima une par trimestre.

Dans la mesure du possible, une épreuve de type pratique aura lieux par trimestre.

## Contenu d'une séquence

### Vérification des pré-requis

Certaines parties du programme s'apppuient sur des connaissances et des capacités
rencontrées antérieurement. Il s'agit de réactiver ces notions déjà vues et de
situer le niveau de maîtrise de l'élève.

Cette vérification se fait à l'aide de l'outil numérique, via Moodle afin de garder
une trace de la réalisation de l'élève. Cela peut se faire à l'aide de question type
QCM, ou d'un puzzle de Parson (outil Code Puzzle).

Dans l'idéal la vérification des pré-requis se fait au préalable à la séquence.

### Activité débranchée

Dans la mesure du possible, pour introduire une nouvelle notion et prendre le temps de l'assimiler
en la découplant de l'utilisation de l'outil informatique.

### Présentation orale (histoire de l'informatique)

5 min de présentation orale sur l'histoire d'un aspect de l'informatique abordé en cours.
L'exposé oral est complété par un mini-panneau (feuille A4) qui est placé sur une frise 
chronologique à l'issue de la présentation.

L'exposé est préparé seul ou à deux.
Noté sur 10, coeff 1.

### Exposé du cours

Le cours en ligne se veut exhaustif, il est consulté en préalable par les élèves et
commentés par le professeur a priori durant le cours en classe entière. C'est l'occasion
de répondre aux premières questions et de faire le lien avec l'évaluation des pré-requis.

Au terme du chapitre un temps est dévolu à un bilan, afin de faire émerger les points clés
et les notions essentielles nouvellemnt rencontrées. Ce temps doit permettre de faciliter 
la réalisation de la fiche de synthèse.

### Exercices d'entrainement

 - en ligne, intégrés au cours. Pour manipuler, expérimenter une notion nouvellement introduite,
 - à l'écrit, pour travailler les attendus de l'examen.

### TP

 - début en cours, à terminer en autonomie
 - note sur 10, coeff 1

### évaluation QCM

 - une fois par semaine
 - évaluation des connaissances
 - 10 questions portant sur le chapitre terminé + 2 ou 3 sur les chapitres précédents.
 - note sur 10, coeff 1
 - peut être refait selon le souhait de l'élève, la meilleure note est conservée.

### Évaluation sommative

 -  évaluation de connaissances et de compétences à l'écrit.
 -  au minimum une fois par trimestre.
 -  exercices de type examen
 -  attention particulière sur la qualité du raisonnement et l'expression écrite
 -  note sur 20, coeff 3

### Projet

 - travail par groupe de 2 ou 3
 - 3 projets dans l'année au maximum
 - présentation orale
 - 

### Fiche de synthèse

 - note sur 5, coeff 1
 - fait office de trace écrite
 - permet à l'élève de faire un bilan des nouvelles connaissances et capacités
 - maximum l'équivalent d'une feuille A4 recto-verso.
 - contient obligatoirement :
   - 3 questions 
   - 1 carte mentale 
   - 1 exemple de problème et sa solution 