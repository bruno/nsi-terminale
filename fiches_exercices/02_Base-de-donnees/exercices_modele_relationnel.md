---
title: "Bases de données : modèle relationnel"
date: "2023-09-25"
subject: "BDD"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
- bottom=18mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}
  \usepackage{multicol}
  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...


### Exercice 1

Deux relations modélisent la flotte de voitures d'un réseau de location de voitures.

**Relation `Agence` :**  

|id_`agence`|	ville| 	département|
|:---:|:---:|:---:|
|1 |Paris |75|
|2 	|Lyon 	|69|
|3 	|Marseille 	|13|
|4 |Aubagne 	|13|

**Relation `Voiture` :**  

|id_voiture 	|marque 	|modèle| 	kilométrage| 	couleur| 	id_agence|
|:---:|:---:|:---:|:---:|:---:|:---:|
|1| 	Rono| 	Trio |	12000 |	Rouge |	2|
|2| 	Pijot| 	102 |	22000 |	Noir |	3|
|3| 	Toutouyou| 	Yonris | 	33000 |	Noir |	3|

#### Questions
1. Combien la relation `Voiture` comporte-t-elle d'attributs ?
2. Que vaut son cardinal (= le nombre d'enregistrements) ?
3. Quel est le domaine de l'attribut *id_agence* dans la relation `Voiture` ?
4. Quel est le schéma relationnel de la relation `Agence` ?
5. Quelle est la clé primaire de la relation `Agence` ?
6. Quelle est la clé primaire de la relation `Voiture` ?
7. Quelle est la clé étrangère de la relation `Voiture` ?
8. Quel est le schéma relationnel de la relation `Voiture` ?

### Exercice 2

Considérons la base de données Tour de France 2020, contenant les relations données à la suite.

#### Questions
1. Donner le schéma de la base de données.
2. Quel temps a réalisé Guillaume MARTIN sur l'étape Sisteron / Orcières-Merlette ?
3. À l'arrivée à Privas, qui est arrivé le premier entre Primož ROGLIČ et Simone CONSONNI ?
4. Comment modifier le schéma de la base pour connaître le vainqueur de chaque étape ?
\newpage
**Relation `Equipe` :**

|code_equipe| 	nom_equipe|
|:---:|:---:|
|ALM |	AG3R La Rondiale|
|AST |	Asana Pro Team|
|TBM |	Rahrain - McLouren|
|BOH |BORO - hansrove|
|CCC| 	CC Tim|
|COF |	Confidis, Sutions Crédits|
|DQT |	Deninckx - Quick Stop|
|EF1 |	AR Pro Cicling|
|GFC |	Groupomo - SDJ|
|LTS |	Latta Soudol|
|... |	...|

**Relation `Coureur` :**

|dossard |	nom_coureur |	prenom_coureur |	code_equipe|
|:---:|:---:|:---:|:---:|
|141 |	LÓPEZ |	Miguel Ángel |	AST|
|142 |	FRAILE |	Omar |	AST|
|143 |	HOULE |	Hugo |	AST|
|11 |	ROGLIČ |	Primož |	TJV|
|12 |	BENNETT |	George |	TJV|
|41 |	ALAPHILIPPE |	Julian |	DQT|
|44 |	CAVAGNA |	Rémi |	DQT|
|45 |	DECLERCQ |	Tim |	DQT|
|121 |	MARTIN |	Guillaume |	COF|
|122 |	CONSONNI |	Simone |	COF|
|123 |	EDET |	Nicolas |	COF|
|… |	… |	… |	…|

\newpage
**Relation `Etape` :**

|num_etape 	|ville_depart| 	ville_arrivee |	km|
|:---:|:---:|:---:|:---:|
|1| 	Nice |	Nice |	156|
|2 |	Nice |	Nice |	185|
|3 |	Nice |	Sisteron |	198|
|4 |	Sisteron |	Orcières-Merlette |	160|
|5 |	Gap |	Privas 	|198|
|... |	... |	... 	|...|

**Relation `Temps` :**

|dossard |	num_etape |	temps|
|:---:|:---:|:---:|
|41 |	2 |	04:55:27|
|121 |	4 |	04:07:47|
|11 |	5 |	04:21:22|
|122 |	5 |	04:21:22|
|... |	... |	...|

### Exercice 3

On considère une base de données permettant de gérer des réservations dans une compagnie 
d'hôtels. Voici le schéma de cette base :

`Client`(*nom* TEXT, *prenom* TEXT)  
`Reservation`(*id_reservation* INT, *num_chambre* INT, *nom_hotel* TEXT)  
`Hotel`(*id_hotel* TEXT, *nom_hotel* TEXT, *adresse* TEXT)  
`Chambre`(*num_chambre* INT, *nom_hotel* TEXT, *prix* INT)

Repérer, expliquer et corriger toutes les anomalies dans le schéma relationnel de cette base.

### Exercice 4

Une sandwicherie effectuant des livraisons à domicile dispose d'une base de données dont 
certains extraits de tables sont reproduits ici.

La table Sandwich comporte les informations relatives aux sandwichs proposés à la vente :

|nom_sandwich 	|prix|
|:---:|:---:|
|Cheesburger 	|3,90
|Double cheese| 	4,90|
|Italien 	|4,90|
|Vege| 	15,00|

La table Client comporte les informations relatives aux clients :

|nom |	prenom |	adresse| numero_client|
|:---:|:---:|:---:|:---:|
|Bernard |	Alain |	9, rue Bienvenue, 13008 MARSEILLE |	42|
|Bernard |	Yves |	2, rue Vive la joie, 13400  AUBAGNE |	51|

La table Commande comporte les informations relatives aux commande passées :

|numero_client| nom_sandwich| quantite| numero_commande|date|
|:---:|:---:|:---:|:---:|:---:|
|42| Italien| 	2| 	12452| 	2020-12-11|
|42 |	Vege |	1 	|12452 |	2020-12-11|
|51 |Cheesburger|4| 13301| 	2020-12-23|

#### Questions
1. Une commande peut-elle comporter plusieurs sandwichs de types différents ?
2. Quel est le schéma des relations `Sandwich` et `Client` ? Identifier les clés 
   primaires et étrangères de ces deux tables.
3. La relation Commande possède-t-elle un attribut pouvant être clé primaire ? En l'absence 
   d'un attribut clé  primaire, un couple ou un triplet d'attributs peut-il jouer ce rôle ?
   Expliquer.
4. Cette base de données semble-t-elle bien modélisée ? Si ce n'est pas le cas, proposer des 
   modifications.

### Exercice 5

Donner la modélisation relationnelle d'un bulletin scolaire. Cette dernière doit permettre 
de mentionner :

- des élèves, possédant un numéro d'étudiant alphanumérique unique;
- un ensemble de matières fixées, mais qui ne sont pas données;
- au plus une note sur 20, par élève et par matière.
<!-- 
### Exercice 6

On considère dans cet exercice une base de données stockant des informations sur les élèves 
d'un lycée. En voici un extrait :

|nom| 	prenom| date_naissance |classe| 	option1|option2| 	option3|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|Armand |	Léa |	12/02/05 |	1G1 |	Maths| 	NSI |	LLCE Anglais|
|Joulain |	Marie |	13/01/06 |	2de3 |	DNL Anglais |	NULL |	NULL|
|Marchand| 	Anthony| 	12/12/05 |	1G1 |	HGGSP |	SES |	Maths|
|Marchand |	Sophie |	06/04/04 |	TG2 |	Maths |	Physique |	NULL|

#### Questions

1. Quel est le schéma relationnel de cette base ?
2. Quel défaut de conception majeur présente cette base de données ?
3. Certains attributs n'existent pas pour certains enregistrements. Scindez cette table 
en deux tables pour éviter ce problème. Indiquer les enregistrements de ces deux tables. -->

*Références*

Gilles Becker, d'après :   
Exercices 1 et 2 : Gilles Lassus; exercices 4 : Prépabac NSI, Terminale, G.CONNAN, V.PETROV, G.ROZSAVOLGYI, L.SIGNAC, éditions HATIER; exercice 5 : Numérique et Sciences Informatiques, 24 leçons, Terminale, T. BALABONSKI, 
 S. CONCHON, J.-C. FILLIATRE, K. NGUYEN, éditions ELLIPSES.

