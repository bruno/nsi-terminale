---
title: "Structures de données : GRAPHES"
date: "2024-03"
subject: "BDD"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
- bottom=18mm
header-includes:
- |
  ```{=latex}
  \usepackage{amsmath}
  
  ```
...

## Exercice 1

Dessiner tous les graphes non orientés ayant exactement trois sommets.

## Exercice 2

Voici deux graphes que l'on appelera respectivement G1 et G2.

![graphe G1](g3.png){width=6cm} ![graphe G2](g4.png){width=6cm}

1. Lequel est non orienté ?
2. Pour le graphe non orienté :
  - Donner deux sommets adjacents et deux sommets non adjacents.
  - Donner les voisins de A ?
  - Quels sont les degrés des sommets B, C et E ?
  - S'il y en a, donner un cycle de ce graphe.
  - Donner toutes les chaînes entre les sommets A et D.
  
3. Pour le graphe orienté :
  - Donner les successeurs et les prédecesseurs des sommets A et C.
  - S'il y en a, donner un chemin entre G et B. Et entre B et D ?
  - S'il y en a, donner un circuit de ce graphe.
  - Quel est le sommet dont le degré est le plus grand ?  

## Exercice 3

On reprend les deux graphes G1 et G2 de l'exercice 1.

1.Donner la matrice d'adjacence de chacun de ces graphes (on prendra les indices des sommets dans l'ordre alphabétique).  
2. Donner la représentation de chacun de deux graphes sous la forme d'un dictionnaire de liste de successeurs.  
3. On considère les deux graphes G3 et G4 représentés comme ci-dessous. Dessiner ces deux graphes.

$G3=\begin{pmatrix}
    0&1&1&0&1\\
    0&0&1&0&0\\
    0&0&0&1&0\\
    1&0&0&0&1\\
    0&0&0&0&0\\
    \end{pmatrix}$

$G4$ = {A:[B,C], B:[A,D,E], C: [A,E]}

