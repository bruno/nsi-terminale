def verifier_contenu(F: File, nb_rouge: int, nb_vert: int, nb_jaune: int) -> bool :
    rouge = nb_elements(F, "rouge")
    vert = nb_elements(F, "vert")
    jaune = nb_elements(F, "jaune")
    return rouge <= nb_rouge and vert <= nb_vert and jaune <= nb_jaune


def nb_elements(F: File, elt : str) -> int :
    n = 0
    file_temp = creer_file_vide()
    while not est_vide(F):
        element = defiler(F)
        if element == elt :
            n = n + 1
    while not est_vide(file_temp):
        enfiler(F, defiler(file_temp))
    return n

def former_pile(F: File) -> Pile :
    file_temp = creer_file_vide()
    pile_temp = creer_pile_vide()
    while not est_vide(F):
        element = defiler(F)
        enfiler(file_temp, element)
        empiler(pile_temp, element)
    P = creer_pile_vide()
    while not est_vide(pile_temp):
        empiler(P, depiler)
    while not est_vide(file_temp):
        enfiler(F, defiler(file_temp))
    return P



def taille_file(F : File) -> int :
    n = 0
    temp = creer_file_vide()
    while not est_vide(F) :
        enfiler(temp, defiler(F))
        n = n + 1
    while not est_vide(temp) :
        enfiler(F, defiler(temp))
    return n

def former_pile(F: File) -> Pile :
    file_temp = creer_file_vide()
    pile_temp = creer_pile_vide()
    while not est_vide(F):
        element = defiler(F)
        enfiler(file_temp, element)
        empiler(pile_temp, element)
    P = creer_pile_vide()
    while not est_vide(pile_temp):
        empiler(P, depiler(pile_temp))
    while not est_vide(file_temp):
        enfiler(F, defiler(file_temp))
    return P
