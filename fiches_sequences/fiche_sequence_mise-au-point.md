---
author : Bruno Bourgine
title : Fiche séquence
---

# Mise au point de programme


## Positionnement

### Temporel

Premier chapitre de l'année

### Référentiel

Thème : langage et programmation

Contenus : 

    - Mise au point des programmes. 
    - Gestion des bugs.


## Objectifs

- savoir répondre aux causes typiques de bugs,  
- réactiver les bonnes pratiques de programmation,  
- évaluer la maîtrise du typage,  
- rappeler le fonctionnement des tests,
- exécuter un programme pas à pas (à la main ou avec PythonTutor)

## Contenus


- Cours en ligne
    - [1. Coder proprement](https://bruno.forge.aeif.fr/numerique-et-sciences-informatiques-terminale/01_Programmation/P1_mise_au_point/#1-coder-proprement)
    - [2. Code lisible](https://bruno.forge.aeif.fr/numerique-et-sciences-informatiques-terminale/01_Programmation/P1_mise_au_point/#2-du-code-lisible)
- Activité Capytale
    - [TNSI - TD Mise au point de programmes](https://capytale2.ac-paris.fr/p/basthon/n/?kernel=python3&id=1772650&extensions=admonition)
- Cours en ligne
    - [3. Du code correct](https://bruno.forge.aeif.fr/numerique-et-sciences-informatiques-terminale/01_Programmation/P1_mise_au_point/#3-du-code-correct)
- TD - Passer sur VSCode
    - exécuter un programme Python (par exemple `compte`, `moyenne` et `recherche_min_max` du TD Capytale)
    - mettre en place les tests unitaires avec `doctest`
- Cours en ligne
    - [4. Identifier les erreurs](https://bruno.forge.aeif.fr/numerique-et-sciences-informatiques-terminale/01_Programmation/P1_mise_au_point/#4-identifier-les-erreurs)
- TD débranché [exercice](./../fiches_activites/01_Programmation/activite_mise-au-point.pdf)  et [correction](./../fiches_activites/01_Programmation/activite_mise-au-point_correction.pdf) 
- TD - Python tutor
    - exécuter les programmes précédents dans Python Tutor pour en voir le déroulement

## Évaluation

- QCM sur le typage
- 01_DS_cours_types
  - exercice 1 : nommage de variables
  - exercice 2 : nommage de fonction, signature, documentation
  - exercice 3 : déclaration de types composés
  - exercice 4 : linter et pep8
  - exercice 5 : documentation et test de fonction

## Bilan
