def matiere_inf(notes : dict[str,float]) -> list[str]:
    "renvoie les matières dont la moyenne est inférieure à 10"
    matieres = []
    for matiere, moyenne in notes.items() :
        if moyenne < 10 :
            matieres.append(matiere)
    return matieres

def matiere_inf2(notes : dict[str,float]) -> list[str]:
    "renvoie les matières dont la moyenne est inférieure à 10"
    return [matiere for matiere, moyenne in notes.items() if moyenne < 10]
