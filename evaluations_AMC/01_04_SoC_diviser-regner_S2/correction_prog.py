def compte_lettres(mot : str) -> dict[str, int] :
    dico = {}
    for lettre in mot :
        if lettre in dico :
            dico[lettre] += 1
        else :
            dico[lettre] = 1
    return dico