Note = tuple[str, float]

def note_creer(eleve : str, valeur : float) -> Note:
    """
    Créée une note à partir d'un nom d'eleve et d'une valeur décimale
    comprise entre 0 et 20.
    Exemple:
    note_creer('martin', 12.5)
    >>> ('martin', 12.5)
    """
    return (eleve, valeur)

def note_get_valeur(note : Note) -> float:
    valeur = note[1]
    return valeur

def note_get_eleve(note : Note) -> str :
    return note[0]

def note_modifier_valeur(note : Note, nouv_valeur : float) -> Note :
    eleve = note[0]
    return note_creer(eleve, nouv_valeur)

def note_modifier_eleve(note : Note, nouv_eleve : str) -> Note :
    valeur = note[1]
    return note_creer(nouv_eleve, valeur)

def note_to_str(note : Note) -> str :
    chaine = f"eleve : {note[0]}, note : {note[1]}"
    return chaine

n = note_creer('Martin', 12.5)
n2 = note_modifier_valeur(n, 16)
print(note_to_str(n2))