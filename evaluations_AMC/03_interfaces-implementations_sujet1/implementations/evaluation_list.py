Note = list[str]
Evaluation = list[str, list[Note]]

def evaluation_creer(date : str) -> Evaluation :
    """
    créée un nouvelle évaluation pour une date donnée au format
    jjmmaaa
    Exemple :
    evaluation_creer('19062023')
    >>> ['19062023', []]
    """
    return [date, []]

def evaluation_ajouter_note(eva : Evaluation, note : Note) -> None :
    """
    ajoute une note à une évaluation
    """
    notes = eva[1]
    notes.append(note)

def evaluation_get_date(eva : Evaluation) -> str :
    "renvoie la date d'une évaluation"
    return eva[0]

def evaluation_get_notes(eva : Evaluation) -> list :
    "renvoie le tableau contenant l'ensemble des notes d'une évaluation"
    return eva[1]

def evaluation_get_note(eva : Evaluation, eleve : str) -> str :
    """
    renvoie la valeur d'une note associée à un élève pour une
    évaluation donnée
    """
    notes = eva[1]
    for note in notes :
        if note[0] == eleve :
            return note[1]
    return ""

def evaluation_get_eleve(eva : Evaluation) -> list :
    """
    renvoie un tableau contenant les noms des élèves ayant été évalués
    """
    eleves = []
    notes = eva[1]
    for note in notes :
        eleves.append(note[0])
    return eleves

def evaluation_calculer_moyenne(eva : Evaluation) -> float :
    """
    calcule la moyenne de l'évaluation passée en paramètre, arrondie 
    au dixième
    """
    somme = 0
    notes = eva[1]
    for note in notes :
        somme += float(note[1])
    moy = somme/len(notes)
    return round(moy, 1)

def evaluation_to_str(eva : Evaluation) -> str :
    """
    renvoie les données contenues dans une évaluation
    sous forme de chaines de caractères avec une ligne par élève
    """
    chaine = f" Évaluation du {eva[0]}\nÉlève : Note\n"
    notes = eva[1]
    for note in notes :
        info = f"{note[0]} : {note[1]}\n"
        chaine += info
    return chaine
