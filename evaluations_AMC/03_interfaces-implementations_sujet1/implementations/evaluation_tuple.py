from doctest import testmod
Note = dict[str,float]
Evaluation = tuple[str, dict[str, float]]

def evaluation_creer(date : str) -> Evaluation :
    """
    créée un nouvelle évaluation pour une date donnée au format
    jjmmaaa
    Exemple :
    >>> evaluation_creer('19062023')
    ('19062023', {})
    """
    return (date, {})

def evaluation_ajouter_note(eva : Evaluation, note : Note) -> Evaluation :
    """
    renvoie une évaluation à laquelle on a ajouté une note
    Exemple :
    >>> evaluation_ajouter_note(evaluation_creer('19062023'), {'eleve': 'Martin', 'valeur': 13.5})
    ('19062023', {'Martin': 13.5})
    """
    date = eva[0]
    value = eva[1]
    value[note['eleve']] = note['valeur']
    return (date, value)

def evaluation_get_date(eva : Evaluation) -> str :
    """
    renvoie la date d'une évaluation
    >>> evaluation_get_date(('19062023', {'Martin': 13.5}))
    '19062023'
    """
    return eva[0]

def evaluation_get_notes(eva : Evaluation) -> dict :
    """
    renvoie le dictionnaire contenant l'ensemble des notes d'une évaluation
    >>> evaluation_get_notes(('19062023', {'Martin': 13.5}))
    {'Martin': 13.5}
    """
    return eva[1]

def evaluation_get_note(eva : Evaluation, eleve : str) -> float :
    """
    renvoie la valeur d'une note associée à un élève pour une
    évaluation donnée
    """
    notes = evaluation_get_notes(eva)
    return notes['eleve']

def evaluation_get_eleve(eva : Evaluation) -> list :
    """
    renvoie un tableau contenant les noms des élèves ayant été évalués
    >>> evaluation_get_eleve(('10112023', {'Martin': 13, 'Tamar': 11, 'Diop': 15}))
    ['Martin', 'Tamar', 'Diop']
    """
    notes = evaluation_get_notes(eva)
    return list(notes.keys())

def evaluation_calculer_moyenne(eva : Evaluation) -> float :
    """
    calcule la moyenne de l'évaluation passée en paramètre, arrondie 
    au dixième
    >>> evaluation_calculer_moyenne(('10112023', {'Martin': 10.5, 'Tamar': 13.5, 'Diop': 12}))
    12.0
    """
    notes = evaluation_get_notes(eva)
    somme = 0
    for note in notes.values() :
        somme += note
    moy = somme/len(notes)
    return round(moy, 1)

def evaluation_to_str(eva : Evaluation) -> str :
    """
    renvoie les données contenues dans une évaluation
    sous forme de chaines de caractères avec une ligne par élève
    """
    date = evaluation_get_date(eva)
    chaine = f"Évaluation du {date}\n Élève : Note\n"
    notes = evaluation_get_notes(eva)
    for eleve, note in notes.items() :
        info = f" {eleve} : {note}\n"
        chaine += info
    return chaine

testmod()