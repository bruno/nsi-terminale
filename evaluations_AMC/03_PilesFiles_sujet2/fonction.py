def myst(lst : Cell) -> ?:
    "lst est un objet de type Cell"
    if lst is None :
        return 0
    return myst(lst.suivante) + 1

def nombre_occurence(tab : list[int], n : int) -> int :
    occ = 0
    for valeur in tab :
        if valeur == n :
            occ += 1
    return occ
