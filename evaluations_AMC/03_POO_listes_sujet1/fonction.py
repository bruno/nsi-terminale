def mystere(x : int, lst : Cell) -> int:
    "x est un entier et lst est un objet de type Cell"
    if lst is None:
        return 0
    if x == lst.valeur:
        return mystere(x, lst.suivante) + 1
    return mystere(x, lst.suivante)

# def mystere(x : int, lst : Cell) -> int:
#     "x est un entier et lst est un objet de type Cell"
#     if lst is None:
#         return -1
#     if x == lst.valeur:
#         return 1
#     return mystere(x, lst.suivante) + 1

def moyenne(notes : list[int]) -> float :
    somme = 0
    for note in notes :
        somme = somme + note
    return somme / len(notes)
