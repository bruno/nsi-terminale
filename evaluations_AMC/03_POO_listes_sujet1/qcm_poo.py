class Personnage:
    def __init__(self,t,p,e):
        self.type = t
        self.points = p
        self.energie = e


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def egal(self, point) :
        return self.x == point.x and self.y == point.y

class Rectangle:
    def __init__(self,long,larg) :
        self.long=long
        self.larg=larg

    def identique(self,rect):
        return self.long == rect.long and self.larg == rect.larg


class Carte :
    def __init__(self,c :str,v : str) :
        self.couleur = c
        self.valeur = v
    

    