CREATE TABLE contact (
    nom VARCHAR(100) NOT NULL,
    prenom VARCHAR(100),
    courriel VARCHAR(2) PRIMARY KEY);

CREATE TABLE contact (
    nom TEXT ,
    prenom TEXT ,
    courriel TEXT PRIMARY KEY);    

CREATE TABLE joueur (
    joueur_id INT PRIMARY KEY,
    nom VARCHAR(100) NOT NULL);

CREATE TABLE partie(
    j1 INT REFERENCES joueur(joueur_id),
    j2 INT REFERENCES joueur(joueur_id),
    score1 INT NOT NULL,
    score2 INT NOT NULL,
    CHECK (j1<>j2));

CREATE TABLE contact (
    nom TEXT NOT NULL,
    prenom TEXT,
    courriel TEXT NOT NULL);  

CREATE TABLE contact (
    nom TEXT NOT NULL,
    prenom TEXT ,
    courriel TEXT PRIMARY KEY);  