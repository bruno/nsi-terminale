def myst(lst : Cell) -> int:
    "lst est un objet de type Cell"
    assert lst is not None
    if lst.suivante is None:
        return lst.valeur    
    return myst(lst.suivante)

# def mystere(x : int, lst : Cell) -> int:
#     "x est un entier et lst est un objet de type Cell"
#     if lst is None:
#         return -1
#     if x == lst.valeur:
#         return 1
#     return mystere(x, lst.suivante) + 1

def derniere_position(tab : list[int], n : int) -> int :
    pos = -1
    for i in range(len(tab)) :
        if tab[i] == n :
            pos = i
    return pos
