class Personnage:
    def __init__(self,t,p,e):
        self.type = t
        self.points = p
        self.energie = e


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def egal(self, point) :
        return self.x == point.x and self.y == point.y

class Rectangle:
    def __init__(self,l,h) -> None:
        self.long=l
        self.larg=h

    def identique(self,rect):
        return self.long == rect.long and self.larg == rect.larg


class Carte :
    def __init__(self,c :str,v : str) -> None:
        self.coul = c
        self.val = v

    
class Creneau :
     def __init__(self,h :str,s : str) -> None:
        self.horaire = h
        self.salle = s
def identique(a,b):
    return a.horaire == b.horaire and a.salle == b.salle

class Date :
    def __init__(self, j, m):
        self.jour = j
        self.mois = m
def idem(a, b) :
    return a.jour == b.jour and a.mois == b.mois