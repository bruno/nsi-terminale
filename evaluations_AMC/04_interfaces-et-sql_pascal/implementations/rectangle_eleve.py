""" 

DS : Implémentation de l'interface de rectangle


à faire
-------
1. décommenter les fonctions au fur et à mesure (CTRL + MAJ + :)
2. compléter les ellipses '...' par du code correct

"""


from doctest import testmod


Rectangle = ...


# def rectangle_creer(longueur:int, largeur:int) -> Rectangle:
#     """Création d'un objet de type Rectangle définit par ses
#     longueur et largeur.

#     Parameters
#     ----------
#     longueur : int
#         longueur du rectangle
#     largeur : int
#         largeur du rectangle

#     Returns
#     -------
#     Rectangle
#         nouveau rectangle de dimension donné
#     """
#     ...





# def rectangle_get_longueur(r: Rectangle) -> int:
#     """Longueur d'un rectangle donné.

#     Parameters
#     ----------
#     r : Rectangle
#         rectangle à traiter

#     Returns
#     -------
#     int
#         longueur du rectangle
    
#     Examples
#     --------
#         >>> r = rectangle_creer(42,10)
#         >>> long = rectangle_get_longueur(r)
#         >>> long
#         42
#     """
#     ...





# def rectangle_get_largeur(r: Rectangle) -> int:
#     """Largeur d'un rectangle donné.

#     Parameters
#     ----------
#     r : Rectangle
#         rectangle à traiter

#     Returns
#     -------
#     int
#         largeur du rectangle
    
#     Examples
#     --------
#         >>> r = rectangle_creer(42,10)
#         >>> larg = rectangle_get_largeur(r)
#         >>> larg
#         10
#     """
#     ...





# def rectangle_to_string(r: Rectangle) -> str:
#     """Représentation d'un rectangle sous forme
#     de chaîne de caractère.

#     Parameters
#     ----------
#     r : Rectangle
#         rectangle à représenter

#     Returns
#     -------
#     str
#         représentation sous la forme 'Rect<longueur ; largeur>'
    
#     Examples
#     --------
#         >>> r = rectangle_creer(42, 10) #nouveau rectangle
#         >>> rectangle_to_string(r)
#         'Rect<42 ; 10>'
#         >>> r = rectangle_creer(25, 12) #nouveau rectangle
#         >>> rectangle_to_string(r)
#         'Rect<25 ; 12>'
#     """
#     long = rectangle_get_longueur(r)
#     larg = rectangle_get_largeur(r)
#     text = 'Rect<' + str(long) + ' ; ' + str(larg) + '>'
#     return text





# def rectangle_aire(r: Rectangle) -> int:
#     """Aire d'un rectangle.

#     Parameters
#     ----------
#     r : Rectangle
#         rectangle à analyser

#     Returns
#     -------
#     int
#         mesure de la surface du rectangle
    
#     Examples
#     --------
#         >>> r = rectangle_creer(42, 10) #nouveau rectangle
#         >>> a = rectangle_aire(r)
#         >>> a
#         420
#         >>> r = rectangle_creer(25, 12) #nouveau rectangle
#         >>> a = rectangle_aire(r)
#         >>> a
#         300
#     """
#     ...




testmod()
