from doctest import testmod


Tableau = tuple[int,list[int]]


def tableau_creer() -> Tableau:
    """ Structure de tableau
    >>> t = tableau_creer()
    >>> t
    {'taille': 0, 'cases': []}
    """
    nombre_element = 0
    tableau_vide = []
    return {'taille' : 0, 'cases' : []}


def tableau_to_string(t:Tableau) -> str:
    """Renvoie la chaîne de caractère associée au tableau `t`.

    Parameters
    ----------
    t : Tableau
        tableau à afficher

    Returns
    -------
    str
        convertion du tableau en str
    
    Examples
    --------
        >>> t = tableau_creer()
        >>> tableau_to_string(t)
        '[]'
    """
    return str(t['cases'])


def tableau_len(t:Tableau) -> int:
    """
    >>> t = tableau_creer()
    >>> tableau_len(t)
    0
    """
    return t['taille']


def tableau_ajoute(t:Tableau, x:int) -> None:
    """ Ajoute le nombre entier `x` à la fin du tableau.

    Parameters
    ----------
    t : Tableau
        tableau qui va être modifié en recevant la nouvelle valeur `x`
    x : int
        nombre entier qui va être ajouté dans la structure de Tableau

    Examples
    --------
        >>> t = tableau_creer()
        >>> tableau_ajoute(t, 42)
        >>> tableau_to_string(t)
        '[42]'
        >>> tableau_len(t)
        1
    """
    t['taille'] += 1
    t['cases'].append(x)


def tableau_get_item(t: Tableau, i:int) -> int:
    """
    >>> t = tableau_creer()
    >>> tableau_ajoute(t, 10)
    >>> tableau_get_item(t, 0)
    10
    """
    return t['cases'][i]


def tableau_retire(t:Tableau) -> int :
    """Retire et renvoie le dernier élément du tableau.

    Parameters
    ----------
    t : Tableau
        tableau à modifier

    Returns
    -------
    int
        dernier élément du tableau

    Example
    -------
        >>> t = tableau_creer()
        >>> x = tableau_retire(t)   #erreur : exception de type IndexError
        Traceback (most recent call last):
        ...
        IndexError: pop from empty list
        >>> tableau_ajoute(t, 40)
        >>> tableau_ajoute(t, 41)
        >>> tableau_ajoute(t, 42)
        >>> tableau_to_string(t)    #tableau avec les 3 valeurs
        '[40, 41, 42]'
        >>> x = tableau_retire(t)   #suppression de la dernière valeur
        >>> x                       #dernière valeur du tableau
        42
        >>> tableau_to_string(t)    #tableau modifié
        '[40, 41]'
    """
    x = t['cases'].pop()
    return x

testmod()