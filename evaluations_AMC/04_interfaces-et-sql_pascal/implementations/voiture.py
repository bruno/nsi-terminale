from doctest import testmod



def voiture_creer():
    """ Structure de voiture. Crée une voiture initialement vide :
    la marque est 'VIDE', la couleur est 'VIDE' et l'année est 0
    """
    ...



def voiture_to_string(v) -> str:
    """Renvoie l'affichage associé à la voiture sous la forme :
    'Marque = mmmm ; Couleur = cccc ; Année = aaaa'
    avec `mmmm` la marque, `cccc` la couleur et `aaaa` l'année.

    Examples
    --------
        >>> voit_0 = voiture_creer()        #crée une voiture vide
        >>> voiture_to_string(voit_0)       #valeurs par défaut d'une voiture vide
        'Marque = VIDE ; Couleur = VIDE ; Année = 0'
    """
    ...



def voiture_get_marque(v) -> str:
    """ Renvoie la marque de la voiture v

    Parameters
    ----------
    v : Voiture

    Returns
    -------
    str
        Marque de la voiture sous forme de chaîne de caractère

    Examples
    --------
        >>> voit_0 = voiture_creer()                #crée une voiture vide
        >>> voiture_get_marque(voit_0)              #marque vide
        'VIDE'
        >>> voiture_set_marque(voit_0, 'renault')   #met à jour la marque
        >>> voiture_get_marque(voit_0)              #récupère la marque qui n'est plus vide
        'renault'
    """
    ...



def voiture_set_marque(v, m:str) -> None:
    """ Modifie la marque de la voiture `v` avec la valeur `m`

    Parameters
    ----------
    v : Voiture
        voiture à modifier en mettant à jour sa marque
    m : str
        nouvelle marque de la voiture
    
    Examples
    --------
        >>> voit_0 = voiture_creer()                #crée une voiture vide
        >>> voiture_set_marque(voit_0, 'peugeot')
        >>> voiture_to_string(voit_0)               #voit_0 modifiée
        'Marque = peugeot ; Couleur = VIDE ; Année = 0'
        >>> voiture_set_marque(voit_0, 'renault')
        >>> voiture_to_string(voit_0)               #voit_0 modifiée
        'Marque = renault ; Couleur = VIDE ; Année = 0'
    """
    ...


def voiture_set_couleur(v, c:str) -> None:
    ...

def voiture_get_couleur(v) -> str:
    ...

def voiture_set_annee(v, a:int) -> None:
    ...

def voiture_get_annee(v) -> int:
    ...


testmod()
