def f1(a, b):
    if a == 2*b :
        return (a, b)
    else :
        return (b, a)

def f2(a, b):
    for l in a :
        if l ==  2*b :
            return True
    return False

"""
Renvoie le n-ième élément de la plus longue des deux chaînes de caractères passées 
en paramètres.
Entrée :
    * chaines (???): un tuple de deux chaînes de caractères
    * n (???): un nombre d'entier
"""

"""
Affiche les clés d'un dictionnaire pour lesquelles la valeur associée correspond au 
tableau passé en paramètre.
Entrée :
    * dico (???) : un dictionnaire dont les clés sont des entiers et les valeurs 
    associée sont des tableaux de flottants.
    * tab (???) : le tableau de flottants passé en paramètre.
"""