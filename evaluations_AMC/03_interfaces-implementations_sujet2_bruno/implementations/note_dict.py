Note = dict[str, float]

def note_creer(eleve : str, valeur : float) -> Note:
    """
    Créée une note à partir d'un nom d'eleve et d'une valeur décimale
    comprise entre 0 et 20.
    Exemple:
    note_creer('martin', 12.5)
    >>> {'eleve' : 'martin', 'valeur' : 12.5}
    """
    return {'eleve' : eleve, 'valeur' : valeur}

def note_get_valeur(note : Note) -> float:
    return note['valeur']

def note_get_eleve(note : Note) -> str :
    return note['eleve']

def note_modifier_valeur(note : Note, 
    nouv_valeur : float)  :
    note['valeur'] = nouv_valeur

def note_modifier_eleve(note : Note,
    nouv_eleve : str) -> None :
    note['eleve'] = nouv_eleve

def note_to_str(note : Note) -> str :
    chaine = f"élève : {note['eleve']}, note : {note['valeur']}"
    return chaine

n = note_creer('Martin', 12.5)
note_modifier_valeur(n, 16)
print(note_to_str(n))