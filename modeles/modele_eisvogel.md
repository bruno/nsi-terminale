---
title: "Le Titre"
author: "auteur"
date: "date"
subject: "Sujet"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}
  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...


## Encadrements

::: info
Exemple d'utilisation de la boite info
:::


::: warning
Exemple d'utilisation de la boite warning
:::


::: error
Exemple d'utilisation de la boite error
:::

## et un peu de code ...

```python
a = 1
for i in range(3):
    print(f"i = {i}, a = {a}")
    a = 2 * a
```