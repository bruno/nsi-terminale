---
title: "Architecture & OS : Gestion des processus"
date: "2024-01-08"
subject: "Système d'exploitation"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...


# Activité 1 - extrait du concours Castor Informatiqe 2020

![](extrait_castor.png)

# Activité 2 - ordonnancement

Soient les différents processus suivants :

|Processus |Date d'arrivée|	Durée de traitement|
|:---:|:---:|:---:|
| P1 | 0 | 3 |
| P2 | 1 | 6 |
| P3 | 3 | 4 |
| P4 | 6 | 5 |
| P5 | 8 | 2 |

## Application de plusieurs algorithmes

1. Donner le diagramme de Gantt pour l'exécution de ces différents processus
    en utilisant successivement les algorithmes FCFS, RR (quantum = 2 unités de 
    temps) et SRT.

## Performances des algorithmes d'ordonnancement

On définit les métriques suivantes :

 - le temps de séjour (ou d'exécution) (ou de rotation) d'un processus : c'est 
  la différence entre la date de fin d'exécution et la date d'arrivée :  
\begin{center}
$T_{sej}~=~date~fin~d'exécution~-~date~d'arrivée$
\end{center}

- le temps d'attente d'un processus : c'est la différence entre le temps de séjour et la durée du processus :
\begin{center}
$T_{att}~=~T_{sej}~-~durée~du~processus$
\end{center}

- le rendement d'un processus : c'est le quotient entre la durée du processus et le temps de séjour : 
  \begin{center}
  $rendement~=~ \dfrac{durée~du~processus}{T_{sej}}$
\end{center}

2. Pour chacun des trois algorithmes, calculer le temps de séjour, le temps d'attente et le rendement de chaque processus.

3. Quel vous semble être le meilleur des trois algorithmes dans notre exemple ? Expliquer.

# Activité 3 - interblocage

Dans un bureau d’architectes, on dispose de certaines ressources qui ne peuvent
être utilisées simultanément par plus d’un processus, comme l’imprimante, la 
table traçante, le modem. Chaque programme, lorsqu’il s’exécute, demande 
l’allocation des ressources qui lui sont nécessaires. Lorsqu’il a fini de 
s’exécuter, il libère ses ressources.

![](c19e_2.png)

On appelle p1, p2 et p3 les processus associés respectivement aux programmes 1,
 2 et 3.

1. Justifier qu'une situation d'interblocage peut se produire.

2. Modifier l'ordre des instructions du programme 3 pour qu'une telle situation 
ne puisse pas se produire.

