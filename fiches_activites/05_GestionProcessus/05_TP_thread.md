---
title: "Architecture & OS : Gestion des processus"
date: "2024-01-08"
subject: "Système d'exploitation"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...

# TP : accès concurrent aux ressources, exemple d'une variable partagée

Prenons l'exemple d'une variable (= ressource logicielle) partagée entre plusieurs processus. Plus précisément, considérons un programme de jeu multi-joueur dans lequel une variable `nb_pions` représente le nombre de pions disponibles pour tous les joueurs.

Une fonction `prendre_un_pion()` permet de prendre un pion dans le tas commun de pions disponibles, s'il reste au moins un pion évidemment.

On va se mettre dans la situation où il ne reste plus qu'un pion dans le tas commun et on suppose que deux joueurs utilisent la fonction `prendre_un_pion()`, ce qui conduit à la création de deux processus `p1` et `p2`, chacun correspondant à un joueur.

Avec Python, on peut utiliser le module `multiprocessing` pour créer des processus. Le programme Python `pions.py` suivant permet de réaliser la situation de jeu décrite :

## Programme `pions.py`, version 1

```python
from multiprocessing import Process, Value
import time

def prendre_un_pion(nombre):
    if nombre.value >= 1:
        time.sleep(0.001)  # pour simuler un traitement avec des calculs
        temp = nombre.value 
        nombre.value = temp - 1  # on décrémente le nombre de pions

if __name__ == '__main__':
    # création de la variable partagée initialisée à 1
    nb_pions = Value('i', 1)
    # on crée deux processus
    p1 = Process(target=prendre_un_pion, args=[nb_pions])
    p2 = Process(target=prendre_un_pion, args=[nb_pions])
    # on démarre les deux processus 
    p1.start()
    p2.start()
    # on attend la fin des deux processus
    p1.join()
    p2.join()    
    print("nombre final de pions :", nb_pions.value)
```

### Questions :

1. Que représentent p1 et p2 ? Expliquer leur instanciation.
2. Exécuter plusieurs fois ce programme. Que remarquez-vous ?

## Programme `pions.py`, version 2

Ouvrir un nouveau fichier que vous nommerez `pions_v2.py` et recopier le programme ci-dessous.

```python
from multiprocessing import Process, Value
import time

def prendre_un_pion(nombre, numero_processus):
    print(f"début du processus {numero_processus}")
    if nombre.value >= 1:
        print(f"processus {numero_processus} : étape A")
        time.sleep(0.001)  # pour simuler un traitement avec des calculs
        print(f"processus {numero_processus} : étape B")
        temp = nombre.value 
        nombre.value = temp - 1  # on décrémente le nombre de pions
    print(f"nombre de pions restants à la fin du processus {numero_processus} : {nombre.value}")

if __name__ == '__main__':
    # création de la variable partagée initialisée à 1
    nb_pions = Value('i', 1)
    # on crée deux processus
    p1 = Process(target=prendre_un_pion, args=[nb_pions, 1])
    p2 = Process(target=prendre_un_pion, args=[nb_pions, 2])
    # on démarre les deux processus 
    p1.start()
    p2.start()
    # on attend la fin des deux processus
    p1.join()
    p2.join()
    print("nombre final de pions :", nb_pions.value)
```

### Question :

1. Quelles sont les modifications apportées par ce programme ?
2. Exécuter ce programme plusieurs fois et noter différentes sorties obtenues.
   Comment expliquer les résultats affichés?

\newpage


## Programme `pions.py`, version 3

Pour éviter les problèmes de synchronisation, onn va utiliser ce qu'on appelle un verrou : un verrou est objet partagé entre plusieurs processus mais qui garantit qu'un seul processus accède à une ressource à un instant donné.

Ouvrir un nouveau fichier que vous nommerez `pions_v3.py` et recopier le programme ci-dessous.

```python
from multiprocessing import Process, Value, Lock
import time

def prendre_un_pion(v, nombre, numero_processus):
    print(f"début du processus {numero_processus}")
    v.acquire()  # acquisition du verrou
    if nombre.value >= 1:        
        print(f"processus {numero_processus} : étape A")
        time.sleep(0.001)
        print(f"processus {numero_processus} : étape B")
        temp = nombre.value 
        nombre.value = temp - 1
    v.release()  # verrou libéré
    print(f"nombre de pions restants à la fin du processus {numero_processus} : {nombre.value}")

if __name__ == '__main__':
    # création de la variable partagée initialisée à 1
    nb_pions = Value('i', 1)
    # verrou partagé par les deux processus
    verrou = Lock()
    # on crée deux processus
    p1 = Process(target=prendre_un_pion, args=[verrou, nb_pions, 1])
    p2 = Process(target=prendre_un_pion, args=[verrou, nb_pions, 2])
    # on démarre les deux processus 
    p1.start()
    p2.start()
    # on attend la fin des deux processus
    p1.join()
    p2.join()    
    print("nombre final de pions :", nb_pions.value)
```

### Question :

1. Quelles sont les modifications apportées dans le code de ce programme ?
2. Exécuter ce programme plusieurs fois et noter différentes sorties obtenues.
   Quel est le mécanisme permettant que le nombre de pion soit toujours égal à 0 ?

\newpage

## Application

Voici un script Python dans lequel on crée une variable globale `nombre` qui vaut 0 au départ et à laquelle on ajoute 100 quatre fois successivement grâce à la fonction `ajoute_100`.

```python
import time

def ajoute_100():
    global nombre
    for i in range(100):
        time.sleep(0.001)  # pour simuler un traitement avec des calculs
        nombre = nombre + 1

if __name__ == '__main__':  
    nombre = 0
    for i in range(4):
        ajoute_100()
    print("valeur finale :", nombre)
```
### Consignes

1. Copier et exécuter le script dans un terminal pour vérifier que la valeur finale de la variable nombre est bien égale à 400.

On va maintenant supposer qu'une telle variable est partagée par 4 processus, chacun étant chargé d'ajouter 100 à cette variable.

2. En s'inspirant des programmes précédents, créer un script permettant :

- de créer une variable partagée entière appelée `nombre_partage` et initialisée à 0
- de créer 4 processus ayant pour rôle d'exécuter une fonction `ajouter_100` à laquelle on passe `nombre_partage` en argument :
  - la fonction `ajouter_100(nombre)` doit ajouter 100 à la variable partagée `nombre` en utilisant une boucle for qui incrémente 100 fois d'une unité la variable
  - on laissera une temporisation pour simuler d'autres calculs
- de démarrer les 4 processus et d'attendre la fin de leur exécution
- d'afficher la valeur finale de la variable `nombre_partage`

3. Exécuter ce script dans un terminal et observer que la valeur finale de la variable `nombre_partage` n'est pas (toujours) égale à 400 comme on pourrait s'y attendre. Comment peut-on expliquer cela ?

4. Ajouter des affichages dans la fonction `ajouter_100` pour observer ce qu'il se passe. Afficher le numéro du processus en cours d'exécution et la valeur de la variable partagée à la fin de chaque tour de boucle.