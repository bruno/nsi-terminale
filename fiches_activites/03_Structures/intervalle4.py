'''
Intervalle [a,b]
implémentation par la POO
'''

from doctest import testmod


class Intervalle:
    def __init__(self, debut, fin):
        self.a = debut
        self.b = fin


    def est_vide(self):
        return self.b < self.a


    def __len__(self):
        if self.est_vide() :
            return 0
        return self.b - self.a


    def __contains__(self, x):
        if self.est_vide():
            return False
        if x > self.b or  x < self.a:
            return False
        return True


    def __eq__(self, inter2):
        if self.est_vide() and inter2.est_vide():
            return True
        
        if self.a == inter2.a and self.b == inter2.b:
            return True

        return False 


    def __le__(self, inter2):
        if self.est_vide():
            return True

        return self.a >= inter2.a and self.b <= inter2.b


    def intersection(self, inter2):
        if self.est_vide():
            return self
        
        if inter2.est_vide():
            return inter2

        tmp_a = max(self.a, inter2.a)
        tmp_b = min(self.b, inter2.b)

        return Intervalle( tmp_a, tmp_b )
    

    def reunion(self, inter2):
        if self.est_vide():
            return inter2
        if inter2.est_vide():
            return self
        
        intersec = self.intersection(inter2)
        if intersec.est_vide():
            if len(self) <= len(inter2):
                return self
            else:
                return inter2
        
        tmp_a = min(self.a, inter2.a)
        tmp_b = max(self.b, inter2.b)

        return Intervalle(tmp_a, tmp_b)
        
        
    def __str__(self):
        if self.est_vide():
            return '[]'
        return "[" + str(self.a) + " ; " + str(self.b) + "]"



# interface
def intervalle_creer(a: float, b: float) -> Intervalle:
    """Crée l'intervalle [a;b] avec les bornes incluses.

    Parameters
    ----------
    a : (float) borne inférieure de l'intervalle
    b : (float) borne supérieure de l'intervalle

    Returns
    -------
    (Intervalle) 

    Examples
    --------
        >>> intervalle_to_str(intervalle_creer(1,2))
        '[1 ; 2]'
        >>> intervalle_to_str(intervalle_creer(2,-1))
        '[]'
    """
    return Intervalle(a,b)


def intervalle_borne_inf(i: Intervalle) -> float:
    """Renvoie la borne inf d'un intervalle.

    Parameters
    ----------
    i : (Intervalle)

    Returns
    -------
    (float) borne inf
    
    Examples
    --------
        >>> intervalle_borne_inf(intervalle_creer(1,3))
        1
        >>> intervalle_borne_inf(intervalle_creer(3,1))
        3
        >>> intervalle_borne_inf(intervalle_creer(1,1))
        1
    """
    return i.a


def intervalle_borne_sup(i: Intervalle) -> float:
    """Renvoie la borne sup d'un intervalle.

    Parameters
    ----------
    i : (Intervalle)

    Returns
    -------
    (float) borne sup
    
    Examples
    --------
        >>> intervalle_borne_sup(intervalle_creer(1,3))
        3
        >>> intervalle_borne_sup(intervalle_creer(3,1))
        1
        >>> intervalle_borne_sup(intervalle_creer(1,1))
        1
    """
    return i.b


def intervalle_est_vide(i: Intervalle) -> bool:
    """Indique si l'intervalle est vide ou pas

    Parameters
    ----------
    i : (Intervalle) intervalle à analyser

    Returns
    -------
    (bool) True ssi l'intervalle est vide

    Examples
    --------
        >>> intervalle_est_vide(intervalle_creer(1,2))
        False
        >>> intervalle_est_vide(intervalle_creer(2,2))
        False
        >>> intervalle_est_vide(intervalle_creer(1,-2))
        True
        >>> intervalle_est_vide(intervalle_creer(-1,-2))
        True
        >>> intervalle_est_vide(intervalle_creer(-2,-1))
        False
    """
    return i.est_vide()


def intervalle_to_str(i:Intervalle) -> str:
    """Renvoie un affichage pour l'intervalle.

    Parameters
    ----------
    i : (Intervalle)

    Returns
    -------
    (str)   de la forme `[a ; b]` pour l'intervalle d'extrémités `a` et `b`
            si l'intervalle est vide, renvoie `[]`

    Examples
    --------
        >>> intervalle_to_str(intervalle_creer(1, 2))
        '[1 ; 2]'
        >>> intervalle_to_str(intervalle_creer(-1.5, 2.9))
        '[-1.5 ; 2.9]'
        >>> intervalle_to_str(intervalle_creer(1.5, -2.9))
        '[]'
        >>> intervalle_to_str(intervalle_creer(2,2))
        '[2 ; 2]'
    """
    return str(i)


def intervalle_longueur(i: Intervalle) -> float:
    """Longueur d'un intervalle.

    Parameters
    ----------
    i : (Intervalle)

    Returns
    -------
    (float) 0 pour un intervalle vide et longueur b-a sinon

    Examples
    --------
        >>> intervalle_longueur(intervalle_creer(1,3))
        2
        >>> intervalle_longueur(intervalle_creer(1,1))
        0
        >>> intervalle_longueur(intervalle_creer(1,-3))
        0
        >>> intervalle_longueur(intervalle_creer(-5,-1))
        4
    """
    return len(i)


def intervalle_appartient(i: Intervalle, x: float) -> bool:
    """Indique si le nombre `x` appartient à l'intervalle `i`.

    Parameters
    ----------
    i : (Intervalle)
    x : (float) nombre décimal

    Returns
    -------
    (bool) `True` ssi x appartient à l'intervalle.
    
    Examples
    --------
        >>> intervalle_appartient(intervalle_creer(1,2),1)
        True
        >>> intervalle_appartient(intervalle_creer(1,2),2)
        True
        >>> intervalle_appartient(intervalle_creer(-2,-1),-1.5)
        True
        >>> intervalle_appartient(intervalle_creer(1,2),1)
        True
        >>> intervalle_appartient(intervalle_creer(1,-2),1)
        False
        >>> intervalle_appartient(intervalle_creer(2,2),2)
        True
    """
    return x in i


def intervalle_est_egal(i: Intervalle, j: Intervalle) -> bool:
    """Indique si deux intervalles sont égaux.

    Parameters
    ----------
    i : (Intervalle)
    j : (Intervalle)

    Returns
    -------
    (bool) `True` ssi les deux intervalles sont égaux
            (ou vide tous les deux)
    
    Examples
    --------
        >>> intervalle_est_egal(intervalle_creer(1,2), intervalle_creer(1,2))
        True
        >>> intervalle_est_egal(intervalle_creer(1,1), intervalle_creer(1,-2))
        False
        >>> intervalle_est_egal(intervalle_creer(1,2), intervalle_creer(1,3))
        False
        >>> intervalle_est_egal(intervalle_creer(0,3), intervalle_creer(1,3))
        False
    """ 
    return i == j


def intervalle_est_inclus(i: Intervalle, j:Intervalle) -> bool:
    """Indique si l'intervalle `j` est inclus dans l'intervalle `i`.

    Parameters
    ----------
    i : (Intervalle) intervalle de référence
    j : (Intervalle) intervalle à étudier

    Returns
    -------
    (bool) `True` ssi j est inclus dans i.count
    
    Examples
    --------
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(-1,2))
        True
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(-1,1))
        True
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(0,2))
        True
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(-1,-1))
        True
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(5,5))
        False
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(-1,3))
        False
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(-1.01,0))
        False
        >>> intervalle_est_inclus(intervalle_creer(1,1), intervalle_creer(2,2))
        False
        >>> intervalle_est_inclus(intervalle_creer(1,1), intervalle_creer(0,2))
        False
    """
    return j <= i


def intervalle_intersection(i: Intervalle, j: Intervalle) -> Intervalle:
    """Intersection de deux intervalles.

    Parameters
    ----------
    i : (Intervalle)
    j : (Intervalle)

    Returns
    -------
    (Intervalle) intersection de deux intervalles.

    Examples
    --------
        >>> I = intervalle_creer(1,3)
        >>> J = intervalle_creer(2,4)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[2 ; 3]'
        >>> I = intervalle_creer(-1,3)
        >>> J = intervalle_creer(3,4)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[3 ; 3]'
        >>> I = intervalle_creer(1,3)
        >>> J = intervalle_creer(5,8)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[]'
        >>> I = intervalle_creer(1,1)
        >>> J = intervalle_creer(2,4)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[]'
        >>> I = intervalle_creer(1,1)
        >>> J = intervalle_creer(0,4)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[1 ; 1]'
        >>> I = intervalle_creer(1,3)
        >>> J = intervalle_creer(2,2)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[2 ; 2]'
        >>> I = intervalle_creer(-1,3)
        >>> J = intervalle_creer(-2,5)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[-1 ; 3]'
        >>> I = intervalle_creer(-10,30)
        >>> J = intervalle_creer(-2,5)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[-2 ; 5]'
    """
    return i.intersection(j)


testmod()
