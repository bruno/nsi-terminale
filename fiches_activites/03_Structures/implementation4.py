'''
TNSI - Interface et implémentations
exemple 4 d'implémentation du temps
'''
from doctest import testmod


Temps = list[int]


def temps_creer(h: int, m: int, s: int) -> Temps:
    """Créer un temps en heure, minute et seconde.

    Parameters
    ----------
    h : int
        heure
    m : int
        minute
    s : int
        seconde

    Returns
    -------
    Temps
        un temps

    Example
    -------
        >>> temps_creer(2, 18, 3)
        [2, 18, 3]
    """
    return [h, m, s]
    
def temps_get_heures(t: Temps) -> int:
    """Renvoie l'heure associée au temps t.

    Parameters
    ----------
    t : Temps
        temps dont il faut extraire l'heure

    Returns
    -------
    int
        heure associée au temps t

    Example
    -------
        >>> temps_get_heures(temps_creer(2, 18, 3))
        2
    """
    return t[0]

def temps_get_minutes(t: Temps) -> int:
    """Renvoie les minutes associée au temps t.

    Parameters
    ----------
    t : Temps
        temps dont il faut extraire les minutes

    Returns
    -------
    int
        minutes associée au temps t

    Example
    -------
        >>> temps_get_minutes(temps_creer(2, 18, 3))
        18
    """
    return t[1]

def temps_get_secondes(t: Temps) -> int:
    """Renvoie les secondes associée au temps t.

    Parameters
    ----------
    t : Temps
        temps dont il faut extraire les secondes

    Returns
    -------
    int
        secondes associée au temps t

    Example
    -------
        >>> temps_get_secondes(temps_creer(2, 18, 3))
        3
    """
    return t[2]
    
def temps_ajouter(t_1: Temps, t_2: Temps) -> Temps:
    """Somme de deux temps.

    Parameters
    ----------
    t_1 : Temps
    t_2 : Temps

    Returns
    -------
    Temps
        Somme des deux temps.
    
    Examples
    --------
        >>> t_1 = temps_creer(2,18,3)
        >>> t_2 = temps_creer(1,50,12)
        >>> t = temps_ajouter(t_1, t_2)
        >>> temps_est_egal(t, temps_creer(4,8,15))
        True
    """
    return __int_vers_temps(__temps_vers_int(t_1) + __temps_vers_int(t_2))

def temps_soustraire(t_1: Temps, t_2: Temps) -> Temps:
    """Différence de deux temps.

    Parameters
    ----------
    t_1 : Temps supérieur à t_2
    t_2 : Temps inférieur à t_1

    Returns
    -------
    Temps
        Différence des deux temps.
    
    Examples
    --------
        >>> t_1 = temps_creer(18,15,3)
        >>> t_2 = temps_creer(2,50,38)
        >>> t = temps_soustraire(t_1, t_2)
        >>> temps_est_egal(t, temps_creer(15,24,25))
        True
    """
    assert t_1 >= t_2, "le premier temps doit Ãªtre supÃ©rieur au second"
    return __int_vers_temps(__temps_vers_int(t_1) - __temps_vers_int(t_2))

def temps_est_egal(t_1: Temps, t_2: Temps) -> bool:
    """Test l'égalité entre deux temps.

    Parameters
    ----------
    t_1 : Temps
    t_2 : Temps

    Returns
    -------
    bool
        True ssi t_1 == t_2
    
    Examples
    --------
        >>> temps_est_egal(temps_creer(1,2,3), temps_creer(1,2,3))
        True
    """
    return t_1 == t_2

def temps_to_str(t: Temps) -> str:
    """Convertit un temps en chaine de caractères.

    Parameters
    ----------
    t : Temps

    Returns
    -------
    str
        Affichage du temps sous la forme h:m:s
    
    Examples
    --------
        >>> temps_to_str(temps_creer(1,2,3))
        '1:2:3'
    """
    return (str(t[0]) + ":" + str(t[1]) + ":" + str(t[2]))

def __temps_vers_int(t):
    return t[0]*3600 + t[1]*60 + t[2]

def __int_vers_temps(secondes):
    return [divmod(divmod(secondes, 60)[0], 60)[0], divmod(divmod(secondes, 60)[0], 60)[1], divmod(secondes, 60)[1]]


if __name__ == '__main__':
    testmod()
