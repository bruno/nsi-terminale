'''
Structure de données POINT
implémentée par un dictionnaire
'''


from doctest import testmod


Point = dict[str, float]


def point_creer(abscisse: float, ordonnee: float) -> Point:
    """ Créer un point de coordonnées entières.

    Parameters
    ----------
    abscisse : (float) abscisse du point
    ordonnee : (float) ordonnée du point

    Returns
    -------
    Point
    
    Examples
    --------
        >>> point_creer(3, 1)
        {'abs': 3, 'ord': 1}
        >>> point_creer(-0.5, 2.6)
        {'abs': -0.5, 'ord': 2.6}
    """
    return {"abs": abscisse, "ord": ordonnee}


def point_abscisse(p_0: Point) -> float:
    """Abscisse d'un point.

    Parameters
    ----------
    p_0 : (Point) point dont on veut obtenir l'abscisse.

    Returns
    -------
    (float) : valeur décimale qui représente l'abscisse du point `p_0`

    Examples
    --------
        >>> point_abscisse(point_creer(3,1))
        3
        >>> point_abscisse(point_creer(0,12))
        0
        >>> point_abscisse(point_creer(-42,-24))
        -42
    """
    return p_0['abs']


def point_ordonnee(p_0: Point) -> float:
    """Ordonnée d'un point.

    Parameters
    ----------
    p_0 : (Point) point dont on veut obtenir l'ordonnée.

    Returns
    -------
    (float) : valeur entière qui représente l'ordonnée du point `p_0`

    Examples
    --------
        >>> point_ordonnee(point_creer(3,1))
        1
        >>> point_ordonnee(point_creer(0,12))
        12
        >>> point_ordonnee(point_creer(-42,-24))
        -24
    """
    return p_0['ord']


def point_milieu(p_1: Point, p_2: Point) -> Point:
    """Renvoie le point de coordonnée le milieu
    du segment [p_1, p_2].

    Parameters
    ----------
    p_1 : (Point) première extrémité du segment.
    p_2 : (Point) deuxième extrémité du segment.

    Returns
    -------
    (Point) milieu du segment 
    
    Examples
    --------
        >>> M = point_milieu(point_creer(0,0), point_creer(1,2))
        >>> point_to_str(M)
        '(0.5 ; 1.0)'
        >>> M = point_milieu(point_creer(-5.23,0.3), point_creer(5.23,-0.3))
        >>> point_to_str(M)
        '(0.0 ; 0.0)'
    """
    M_x: float = (point_abscisse(p_2)+point_abscisse(p_1)) / 2
    M_y: float = (point_ordonnee(p_2)+point_ordonnee(p_1)) / 2
    return point_creer(M_x, M_y)


def point_longueur(p_1: Point, p_2: Point) -> float:
    """Longueur du segment d'extrémité `p_1` et `p_0`.

    Parameters
    ----------
    p_1 : (Point) première extrémité du segment
    p_2 : (Point) deuxième extrémité du segment

    Returns
    -------
    (float) longueur du segment

    Examples
    --------
        >>> point_longueur(point_creer(0,0), point_creer(3,4))
        5.0
        >>> point_longueur(point_creer(-2, 1.5), point_creer(4, 9.5))
        10.0
    """
    segment_x = point_abscisse(p_2)-point_abscisse(p_1)
    segment_y = point_ordonnee(p_2)-point_ordonnee(p_1)
    return (segment_x**2 + segment_y**2)**0.5  # racine carré <=> puissance 0.5


def point_translater(p_0: Point, d_x: float, d_y: float) -> None:
    """Modifie les coordonnées du point `p_0` en appliquant
    une translation de vecteur de coordonnées (`d_x`, `d_y`).

    Parameters
    ----------
    p_0 : (Point) point qui sera modifié par la translation
    d_x : (float) abscisse du vecteur de la translation
    d_y : (float) ordonnée du vecteur de la translation

    Examples
    --------
        >>> M = point_creer(3,1)
        >>> point_translater(M, -3.0, -1.0)
        >>> point_to_str(M)
        '(0.0 ; 0.0)'
        >>> M = point_creer(2.4,-3)
        >>> point_translater(M, -4.8, 1.5)
        >>> point_to_str(M)
        '(-2.4 ; -1.5)'
    """
    p_0['abs'] = p_0['abs'] + d_x
    p_0['ord'] = p_0['ord'] + d_y


def point_egal(p_1: Point, p_2: Point) -> bool:
    """Test si deux points sont égaux (à 10^-15 près).

    Parameters
    ----------
    p_1 : (Point) premier point
    p_2 : (Point) deuxième point

    Returns
    -------
    (bool) True ssi les deux points sont situés au même endroit (à +/- 10^-15) 

    Examples
    --------
        >>> point_egal(point_creer(3,1), point_creer(3, 1))
        True
        >>> point_egal(point_creer(3,1), point_creer(3, 1.000_000_000_000_001))
        False
        >>> point_egal(point_creer(3,1), point_creer(3, 1.000_000_000_000_000_1))
        True
    """
    if abs(point_abscisse(p_1) - point_abscisse(p_2)) > 1e-15:
        return False
    if abs(point_ordonnee(p_1) - point_ordonnee(p_2)) > 1e-15:
        return False
    return True


def point_to_str(p_0: Point) -> str:
    """Convertit un point en chaîne de caractère
    pour affichage ou traitement ultérieur.

    Parameters
    ----------
    p_0 : (Point) point à convertir

    Returns
    -------
    (str) chaîne de la forme `(xxxx ; yyyy)`

    Examples
    --------
        >>> point_to_str(point_creer(3,1))
        '(3 ; 1)'
        >>> point_to_str(point_creer(-3.5 , 1.2))
        '(-3.5 ; 1.2)'
    """
    texte: str = "("
    texte += str(p_0["abs"])
    texte += " ; "
    texte += str(p_0["ord"])
    texte += ")"
    return texte


testmod()
