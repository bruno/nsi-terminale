'''
TNSI - Interface et implémentations
exemple 3 d'implémentation du temps
'''
from doctest import testmod


Temps = dict[str, int]


def temps_creer(h: int, m: int, s: int) -> Temps:
    """Créer un temps en heure, minute et seconde.

    Parameters
    ----------
    h : int
        heure
    m : int
        minute
    s : int
        seconde

    Returns
    -------
    Temps
        un temps

    Example
    -------
        >>> temps_creer(2, 18, 3)
        {'h': 2, 'm': 18, 's': 3}
    """
    return {"h": h, "m": m, "s": s}


def temps_get_heures(t: Temps) -> int:
    """Renvoie l'heure associée au temps t.

    Parameters
    ----------
    t : Temps
        temps dont il faut extraire l'heure

    Returns
    -------
    int
        heure associée au temps t

    Example
    -------
        >>> temps_get_heures(temps_creer(2, 18, 3))
        2
    """
    return t["h"]


def temps_get_minutes(t: Temps) -> int:
    """Renvoie les minutes associée au temps t.

    Parameters
    ----------
    t : Temps
        temps dont il faut extraire les minutes

    Returns
    -------
    int
        minutes associée au temps t

    Example
    -------
        >>> temps_get_minutes(temps_creer(2, 18, 3))
        18
    """
    return t["m"]


def temps_get_secondes(t: Temps) -> int:
    """Renvoie les secondes associée au temps t.

    Parameters
    ----------
    t : Temps
        temps dont il faut extraire les secondes

    Returns
    -------
    int
        secondes associée au temps t

    Example
    -------
        >>> temps_get_secondes(temps_creer(2, 18, 3))
        3
    """
    return t["s"]


def temps_ajouter(t_1: Temps, t_2: Temps) -> Temps:
    """Somme de deux temps.

    Parameters
    ----------
    t_1 : Temps
    t_2 : Temps

    Returns
    -------
    Temps
        Somme des deux temps.
    
    Examples
    --------
        >>> t_1 = temps_creer(2,18,3)
        >>> t_2 = temps_creer(1,50,12)
        >>> t = temps_ajouter(t_1, t_2)
        >>> temps_est_egal(t, temps_creer(4,8,15))
        True
    """
    s = temps_get_secondes(t_1) + temps_get_secondes(t_2)
    m = temps_get_minutes(t_1) + temps_get_minutes(t_2)
    h = temps_get_heures(t_1) + temps_get_heures(t_2)
    if s >= 60:
        s = s - 60
        m = m + 1
    if m >= 60:
        m = m - 60
        h = h + 1        
    return {"h": h, "m": m, "s": s}


def temps_soustraire(t_1: Temps, t_2: Temps) -> Temps:
    """Différence de deux temps.

    Parameters
    ----------
    t_1 : Temps supérieur à t_2
    t_2 : Temps inférieur à t_1

    Returns
    -------
    Temps
        Différence des deux temps.
    
    Examples
    --------
        >>> t1 = temps_creer(18,15,3)
        >>> t2 = temps_creer(2,50,38)
        >>> t = temps_soustraire(t1, t2)
        >>> temps_est_egal(t, temps_creer(15,24,25))
        True
    """
    s = temps_get_secondes(t_1) - temps_get_secondes(t_2)
    s = temps_get_secondes(t_1) - temps_get_secondes(t_2)
    m = temps_get_minutes(t_1) - temps_get_minutes(t_2)
    h = temps_get_heures(t_1) - temps_get_heures(t_2)
    if s < 0:
        s = 60 + s
        m = m - 1
    if m < 0:
        m = 60 + m
        h = h - 1        
    return {"h": h, "m": m, "s": s}


def temps_est_egal(t_1: Temps, t_2: Temps) -> bool:
    """Test l'égalité entre deux temps.

    Parameters
    ----------
    t_1 : Temps
    t_2 : Temps

    Returns
    -------
    bool
        True ssi t_1 == t_2
    
    Examples
    --------
        >>> temps_est_egal(temps_creer(1,2,3), temps_creer(1,2,3))
        True
    """
    return t_1 == t_2


def temps_to_str(t: Temps) -> str:
    """Convertit un temps en chaine de caractères.

    Parameters
    ----------
    t : Temps

    Returns
    -------
    str
        Affichage du temps sous la forme h:m:s
    
    Examples
    --------
        >>> temps_to_str(temps_creer(1,2,3))
        '1:2:3'
    """
    return (str(t["h"]) + ":" + str(t["m"]) + ":" + str(t["s"]))


if __name__ == '__main__':
    testmod()
