'''
TNSI - Interface et implémentations
exemple 5 d'implémentation du temps
Programmation Objets
'''

from doctest import testmod


class Temps:
    """Temps défini par heures, minutes et secondes
    """
    def __init__(self, heures: int, minutes: int, secondes: int):
        self.h: int = heures
        self.m: int = minutes
        self.s: int = secondes

    def __eq__(self, t_autre) -> bool:
        """Test l'égalité entre deux temps.

        Parameters
        ----------
        t_autre : Temps à comparer avec self

        Returns
        -------
        bool
            True ssi t_autre est egal à self.
        
        Examples
        --------
            >>> Temps(1,2,3) == Temps (1,2,3)
            True
            >>> Temps(1,2,3) == Temps (1,2,4)
            False
        """
        return self.h == t_autre.h and self.m == t_autre.m and self.s == t_autre.s

    def __add__(self, t_2):
        """Somme de deux temps.

        Parameters
        ----------
        t_1 : Temps
        t_2 : Temps

        Returns
        -------
        Temps
            Somme des deux temps.
        
        Examples
        --------
            >>> t1 = temps_creer(2,18,3)
            >>> t2 = temps_creer(1,50,12)
            >>> t1 + t2 == Temps(4,8,15)
            True
        """
        h = self.h + t_2.h
        m = self.m + t_2.m
        s = self.s + t_2.s
        if s >= 60:
            s = s - 60
            m = m + 1
        if m >= 60:
            h = h + 1
            m = m - 60
        return Temps(h, m, s)

    def __sub__(self, t_2):
        """Différence de deux temps.

        Parameters
        ----------
        t_1 : Temps supérieur à t_2
        t_2 : Temps inférieur à t_1

        Returns
        -------
        Temps
            Différence des deux temps.
        
        Examples
        --------
            >>> t1 = temps_creer(18,15,3)
            >>> t2 = temps_creer(2,50,38)
            >>> t1 - t2 == Temps(15,24,25)
            True
        """
        h = self.h - t_2.h
        m = self.m - t_2.m
        s = self.s - t_2.s
        if s < 0:
            s = 60 + s
            m = m - 1
        if m < 0:
            m = 60 + m
            h = h - 1        
        return Temps(h, m, s)

    def __str__(self) -> str:
        """Convertit un temps en chaine de caractères.

        Parameters
        ----------
        t : Temps

        Returns
        -------
        str
            Affichage du temps sous la forme h:m:s
        
        Examples
        --------
            >>> str(Temps(1,2,3))
            '1:2:3'
        """
        return (str(self.h) + ":" + str(self.m) + ":" + str(self.s))



# interface de l'énoncé
def temps_creer(h: int, m: int, s: int) -> Temps:
    """Créer un temps en heure, minute et seconde.

    Parameters
    ----------
    h : int
        heure
    m : int
        minute
    s : int
        seconde

    Returns
    -------
    Temps
        un temps

    Example
    -------
        >>> temps_to_str(temps_creer(2, 18, 3))
        '2:18:3'
    """
    return Temps(h, m, s)


def temps_get_heures(t: Temps) -> int:
    """Renvoie l'heure associée au temps t.

    Parameters
    ----------
    t : Temps
        temps dont il faut extraire l'heure

    Returns
    -------
    int
        heure associée au temps t

    Example
    -------
        >>> temps_get_heures(temps_creer(2, 18, 3))
        2
    """
    return t.h


def temps_get_minutes(t: Temps) -> int:
    """Renvoie les minutes associée au temps t.

    Parameters
    ----------
    t : Temps
        temps dont il faut extraire les minutes

    Returns
    -------
    int
        minutes associée au temps t

    Example
    -------
        >>> temps_get_minutes(temps_creer(2, 18, 3))
        18
    """
    return t.m


def temps_get_secondes(t: Temps) -> int:
    """Renvoie les secondes associée au temps t.

    Parameters
    ----------
    t : Temps
        temps dont il faut extraire les secondes

    Returns
    -------
    int
        secondes associée au temps t

    Example
    -------
        >>> temps_get_secondes(temps_creer(2, 18, 3))
        3
    """
    return t.s


def temps_ajouter(t_1: Temps, t_2: Temps) -> Temps:
    """Somme de deux temps.

    Parameters
    ----------
    t_1 : Temps
    t_2 : Temps

    Returns
    -------
    Temps
        Somme des deux temps.
    
    Examples
    --------
        >>> t1 = temps_creer(2,18,3)
        >>> t2 = temps_creer(1,50,12)
        >>> t = temps_ajouter(t1, t2)
        >>> temps_est_egal(t, temps_creer(4,8,15))
        True
    """
    return t_1 + t_2


def temps_soustraire(t_1: Temps, t_2: Temps) -> Temps:
    """Différence de deux temps.

    Parameters
    ----------
    t_1 : Temps supérieur à t_2
    t_2 : Temps inférieur à t_1

    Returns
    -------
    Temps
        Différence des deux temps.
    
    Examples
    --------
        >>> t1 = temps_creer(18,15,3)
        >>> t2 = temps_creer(2,50,38)
        >>> t = temps_soustraire(t1, t2)
        >>> temps_est_egal(t, temps_creer(15,24,25))
        True
    """
    return t_1 - t_2


def temps_est_egal(t_1: Temps, t_2: Temps) -> bool:
    """Test l'égalité entre deux temps.

    Parameters
    ----------
    t_1 : Temps
    t_2 : Temps

    Returns
    -------
    bool
        True ssi t_1 == t_2
    
    Examples
    --------
        >>> temps_est_egal(temps_creer(1,2,3), temps_creer(1,2,3))
        True
    """
    return t_1 == t_2


def temps_to_str(t: Temps) -> str:
    """Convertit un temps en chaine de caractères.

    Parameters
    ----------
    t : Temps

    Returns
    -------
    str
        Affichage du temps sous la forme h:m:s
    
    Examples
    --------
        >>> temps_to_str(temps_creer(1,2,3))
        '1:2:3'
    """
    return str(t)


if __name__ == '__main__':
    testmod()
