---
title: "Structures de données : Interface et implémentations"
author: "d'après Germain BECKER, Lycée Mounier, ANGERS"
date: "2023-09-04"
subject: "StructureDonnées"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...


# Activité 1 - Comprendre et utiliser une interface

On dispose d'un type de donnée `Point` permettant de représenter des points à afficher dans 
un programme de dessin. Chaque point a une abscisse et une ordonnée et le repère est orthonormé. 

Voici les **opérations** que l'on souhaite effectuer sur la donnée de type `Point` :

- **Créer** un point
- **Accéder** à son abscisse et à son ordonnée
- **Déterminer** le milieu d'un segment
- **Calculer** la longueur d'un segment
- **Translater** un point
- **Vérifier** si deux points sont égaux
- **Afficher** un point sous forme d'une chaîne de caractères

Voici la spécification (incomplète) des opérations que l'on peut réaliser sur la donnée `Point` :

- `point_creer(x: int, y: int) -> Point` : crée un élément de type `Point` à partir de deux 
entiers `x` (abscisse) et `y` (ordonnée).
- `point_abscisse(p: Point) -> int` : accès à l'abscisse du point `p` (renvoie un flottant).
- `point_milieu(p1: Point, p2: Point) -> Point` : renvoie un nouveau point correspondant au 
milieu du segment d'extrémités `p1` et `p2`.
- `point_longueur(p1: Point, p2: Point) -> float` : renvoie la longueur du segment d'extrémités 
`p1` et `p2`.
- `point_translater(p: Point, dx: int, dy: int)` : modifie les coordonnées d'un point `p` déjà 
créé pour qu'elles correspondent à la translation du vecteur de coordonnées (`dx`, `dy`).
- `point_est_egal(p1: Point, p2: Point) -> bool` : renvoie Vrai si les deux points `p1` et `p2` 
sont égaux, Faux sinon.
 
1. Compléter l'interface de manière à spécifier les opérations manquantes.

2. Écrire l'instruction permettant de créer le point $M(-3, 1)$ ?

3. On exécute le programme suivant, indiquer la valeur renvoyée par 
 `point_est_egal(C, D)`.

```python
A = point_creer(1, 2)
B = point_creer(-3, 5)
C = point_creer(3, 1)
D = point_creer(- point_abscisse(B), point_abscisse(A))
```

4. Déterminer la valeur renvoyée par `point_longueur(A, B)`

5. On souhaite créer le point $I$ milieu du segment $[AC]$ puis lui appliquer une translation de
 vecteur $(3, -2)$ et enfin afficher ses coordonnées. 
 - Écrire les instructions nécessaires.
 - Quelle chaîne de caractères doit s'afficher à l'écran ?

# Activité 2 : La structure de données `Temps`

On dispose d'une structure de données appelée `Temps` permettant de stocker des temps (données en 
heures, minutes, secondes). Voici les opérations que l'on souhaite effectuer sur le type abstrait 
`Temps` :

- **Créer** un temps
- **Accéder** aux heures, minutes, secondes d'un temps
- **Ajouter**, soustraire deux temps
- **Vérifier** si deux temps sont égaux ou non
- **Afficher** un temps sous forme d'une chaîne de caractères

On peut spécifier les opérations de cette structure de données en proposant l'**interface** suivante :

- `temps_creer(h: int, m: int, s: int) -> Temps` : crée un élément de type `Temps` à partir de trois
entiers `h` (heures), `m` (minutes) et `s` (secondes).
- `temps_get_heures(t: Temps) -> int` : accès aux heures du temps `t` (renvoie un entier).
- `temps_get_minutes(t: Temps) -> int` : accès aux minutes du temps `t` (renvoie un entier).
- `temps_get_secondes(t: Temps) -> int` : accès aux secondes du temps `t` (renvoie un entier).
- `temps_ajouter(t1: Temps, t2: Temps) -> int` : renvoie un nouveau temps correspondant à la somme des
temps `t1` et `t2`.
- `temps_soustraire(t1: Temps, t2: Temps) -> int` : renvoie un nouveau temps correspondant à la 
différence de `t1` et `t2` (`t1`-`t2`).
- `temps_est_egal(t1: Temps, t2: Temps) -> bool` : renvoie Vrai si les deux temps `t1` et `t2` sont égaux, Faux sinon.
- `temps_to_str(t: Temps)` : renvoie une chaîne de caractère sous forme `h:m:s`.


**Question 1**: On crée deux temps. 

```python
t1 = temps_creer(2, 18, 3)
t2 = temps_creer(1, 50, 12)
```
Quelles sont les valeurs des instructions `temps_get_heures(t1)`, `temps_get_minutes(t2)` et 
`temps_get_secondes(t1) + temps_get_secondes(t2)` ?

**Question 2** : 

1. Quelle instruction faut-il écrire pour créer un nouveau temps `t` qui est la somme des 
temps `t1` et `t2` ?
2. Que vaut alors le temps `t` ?
3. Qu'affiche alors l'instruction `print(temps_to_str(t))` ?

**Question 3** : Quelle est la valeur renvoyée par l'instruction suivante ?

```python
temps_est_egal(t, temps_creer(4, 8, 15))
```

**Question 4** : Lors d'une course de relais par équipe, les trois membres d'une équipe ont 
mis respectivement les temps suivants pour boucler leurs relais respectifs : 

0 h 51 min 14 sec ;1 h 02 min 53 sec ; 0 h 56 min 31 sec.

Quelles instructions permettent d'afficher le temps total mis par l'équipe pour terminer 
la course ?