'''
structure d'ensembles
implémentatio avec des tableaux
'''

from doctest import testmod

Ensemble = list[int]


def ensemble_creer() -> Ensemble:
    """ Structure d'ensemble. Initialement vide.

    Returns
    -------
    (Ensemble) vide
    
    Examples
    --------
        >>> e = ensemble_creer()
        >>> ensemble_to_str(e)
        'E{}'
    """
    return []


def ensemble_to_str(e:Ensemble) -> str:
    """Renvoie l'ensemble sous la forme d'une chaîne de caractère.

    Parameters
    ----------
    e : (Ensemble)

    Returns
    -------
    (str) chaine de la forme 'E{1 ; 4 ; 3 ; 2}' ou 'E{}'
    """
    texte = "E{"
    for n in e:
        texte += str(n) + " ; "
    if len(texte) > 2:
        texte = texte[:-3]  # suppression des 3 derniers caractères
    return texte + "}"


def ensemble_contient(e: Ensemble, x: int) -> bool:
    """Indique si x appartient ou pas à l'ensemble e.

    Parameters
    ----------
    e : (Ensemble)
    x : (int)

    Returns
    -------
    (bool) `True` ssi x appartient à e

    Example
    -------
        >>> e = ensemble_creer()
        >>> ensemble_contient(e, 0)
        False
        >>> ensemble_contient(e, 1)
        False
        >>> ensemble_ajoute(e, 0)
        >>> ensemble_contient(e, 0)
        True
        >>> ensemble_contient(e, 1)
        False
    """
    for i in range(len(e)):
        if e[i] == x:
            return True
    return False


def ensemble_ajoute(e: Ensemble, x: int) -> None:
    """ Modifie l'ensemble e en y ajoutant l'élément x (sans doublon)

    Parameters
    ----------
    e : (Ensemble)
    x : (int)

    Example
    -------
        >>> e = ensemble_creer()
        >>> ensemble_ajoute(e, 0)
        >>> ensemble_to_str(e)
        'E{0}'
        >>> ensemble_ajoute(e, 0)
        >>> ensemble_to_str(e)
        'E{0}'
        >>> ensemble_ajoute(e, 4)
        >>> ensemble_to_str(e)
        'E{0 ; 4}'
        >>> ensemble_ajoute(e, 3)
        >>> ensemble_to_str(e)
        'E{0 ; 4 ; 3}'
    """
    if not ensemble_contient(e,x):
        e.append(x)


def ensemble_supprime(e: Ensemble, x: int) -> None:
    """ Modifie l'ensemble e en supprimant l'élément x présent.
    Erreur si x n'est pas présent

    Parameters
    ----------
    e : (Ensemble)
    x : (int)

    Example
    -------
        >>> e = ensemble_creer()
        >>> ensemble_ajoute(e, 0)
        >>> ensemble_ajoute(e, 4)
        >>> ensemble_supprime(e, 0)
        >>> ensemble_contient(e, 0)
        False
        >>> ensemble_contient(e, 4)
        True
        >>> e = ensemble_creer()
        >>> ensemble_supprime(e, 0)
        Traceback (most recent call last):
        ...
        IndexError
    """
    if not ensemble_contient(e, x):
        raise IndexError
    
    e.remove(x)


def ensemble_taille(e: Ensemble) -> int:
    """Renvoie le nombre d'élément de l'ensemble e.

    Parameters
    ----------
    e : (Ensemble)

    Returns
    -------
    (int) 0 si l'ensemble est vide et le nombre d'élément 
            de e (sans doublon) sinon
    
    Exemple
    -------
        >>> e = ensemble_creer()
        >>> ensemble_taille(e)
        0
        >>> ensemble_ajoute(e, 0)
        >>> ensemble_taille(e)
        1
    """
    return len(e)


e_0 = ensemble_creer()
assert not ensemble_contient(e_0, 42)  # ne contient pas 42
assert ensemble_to_str(e_0) == 'E{}'  # affichage de l'ensemble vide
ensemble_ajoute(e_0, 42)
assert ensemble_contient(e_0, 42)  # contient 42
assert ensemble_to_str(e_0) == 'E{42}'  # 42 bien ajouté
ensemble_ajoute(e_0, 42)
ensemble_ajoute(e_0, 42)
assert ensemble_to_str(e_0) == 'E{42}'  # pas de doublons
ensemble_ajoute(e_0, 666)
assert ensemble_to_str(e_0) == 'E{42 ; 666}'  # pas de doublons
assert ensemble_taille(e_0) == 2
ensemble_supprime(e_0, 42)    # suppression d'un élément
assert ensemble_to_str(e_0) == 'E{666}'

# levée d'exception et récupération de IndexError
try :
    ensemble_supprime(e_0, 0)
except IndexError:
    assert True  # exception IndexError levée : normal
else:
    assert False  # autre exception ou rien : anormal

testmod()
