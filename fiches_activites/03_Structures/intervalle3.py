'''
Intervalle [a,b]
implémentation avec des tableaux
'''

from doctest import testmod


Intervalle = list[float]


def intervalle_creer(a: float, b: float) -> Intervalle:
    """Crée l'intervalle [a;b] avec les bornes incluses.

    Parameters
    ----------
    a : (float) borne inférieure de l'intervalle
    b : (float) borne supérieure de l'intervalle

    Returns
    -------
    (Intervalle) 

    Examples
    --------
        >>> intervalle_to_str(intervalle_creer(1,2))
        '[1 ; 2]'
        >>> intervalle_to_str(intervalle_creer(2,-1))
        '[]'
    """
    return [a, b]


def intervalle_borne_inf(i: Intervalle) -> float:
    """Renvoie la borne inf d'un intervalle.

    Parameters
    ----------
    i : (Intervalle)

    Returns
    -------
    (float) borne inf
    
    Examples
    --------
        >>> intervalle_borne_inf(intervalle_creer(1,3))
        1
        >>> intervalle_borne_inf(intervalle_creer(3,1))
        3
        >>> intervalle_borne_inf(intervalle_creer(1,1))
        1
    """
    return i[0]


def intervalle_borne_sup(i: Intervalle) -> float:
    """Renvoie la borne sup d'un intervalle.

    Parameters
    ----------
    i : (Intervalle)

    Returns
    -------
    (float) borne sup
    
    Examples
    --------
        >>> intervalle_borne_sup(intervalle_creer(1,3))
        3
        >>> intervalle_borne_sup(intervalle_creer(3,1))
        1
        >>> intervalle_borne_sup(intervalle_creer(1,1))
        1
    """
    return i[1]


def intervalle_est_vide(i: Intervalle) -> bool:
    """Indique si l'intervalle est vide ou pas

    Parameters
    ----------
    i : (Intervalle) intervalle à analyser

    Returns
    -------
    (bool) True ssi l'intervalle est vide

    Examples
    --------
        >>> intervalle_est_vide(intervalle_creer(1,2))
        False
        >>> intervalle_est_vide(intervalle_creer(2,2))
        False
        >>> intervalle_est_vide(intervalle_creer(1,-2))
        True
        >>> intervalle_est_vide(intervalle_creer(-1,-2))
        True
        >>> intervalle_est_vide(intervalle_creer(-2,-1))
        False
    """
    a: float = intervalle_borne_inf(i)
    b: float = intervalle_borne_sup(i)
    return b < a


def intervalle_to_str(i:Intervalle) -> str:
    """Renvoie un affichage pour l'intervalle.

    Parameters
    ----------
    i : (Intervalle)

    Returns
    -------
    (str)   de la forme `[a ; b]` pour l'intervalle d'extrémités `a` et `b`
            si l'intervalle est vide, renvoie `[]`

    Examples
    --------
        >>> intervalle_to_str(intervalle_creer(1, 2))
        '[1 ; 2]'
        >>> intervalle_to_str(intervalle_creer(-1.5, 2.9))
        '[-1.5 ; 2.9]'
        >>> intervalle_to_str(intervalle_creer(1.5, -2.9))
        '[]'
        >>> intervalle_to_str(intervalle_creer(2,2))
        '[2 ; 2]'
    """
    if intervalle_est_vide(i):
        return '[]'
    
    a: float = intervalle_borne_inf(i)
    b: float = intervalle_borne_sup(i)
    texte: str = "[" + str(a)
    texte += " ; " + str(b) + "]"
    return texte


def intervalle_longueur(i: Intervalle) -> float:
    """Longueur d'un intervalle.

    Parameters
    ----------
    i : (Intervalle)

    Returns
    -------
    (float) 0 pour un intervalle vide et longueur b-a sinon

    Examples
    --------
        >>> intervalle_longueur(intervalle_creer(1,3))
        2
        >>> intervalle_longueur(intervalle_creer(1,1))
        0
        >>> intervalle_longueur(intervalle_creer(1,-3))
        0
        >>> intervalle_longueur(intervalle_creer(-5,-1))
        4
    """
    if intervalle_est_vide(i):
        return 0
    
    return intervalle_borne_sup(i) - intervalle_borne_inf(i)


def intervalle_appartient(i: Intervalle, x: float) -> bool:
    """Indique si le nombre `x` appartient à l'intervalle `i`.

    Parameters
    ----------
    i : (Intervalle)
    x : (float) nombre décimal

    Returns
    -------
    (bool) `True` ssi x appartient à l'intervalle.
    
    Examples
    --------
        >>> intervalle_appartient(intervalle_creer(1,2),1)
        True
        >>> intervalle_appartient(intervalle_creer(1,2),2)
        True
        >>> intervalle_appartient(intervalle_creer(-2,-1),-1.5)
        True
        >>> intervalle_appartient(intervalle_creer(1,2),1)
        True
        >>> intervalle_appartient(intervalle_creer(1,-2),1)
        False
        >>> intervalle_appartient(intervalle_creer(2,2),2)
        True
    """
    if intervalle_est_vide(i):
        return False
    
    a: float = intervalle_borne_inf(i)
    b: float = intervalle_borne_sup(i)
    return a <= x and x <= b


def intervalle_est_egal(i: Intervalle, j: Intervalle) -> bool:
    """Indique si deux intervalles sont égaux.

    Parameters
    ----------
    i : (Intervalle)
    j : (Intervalle)

    Returns
    -------
    (bool) `True` ssi les deux intervalles sont égaux
            (ou vide tous les deux)
    
    Examples
    --------
        >>> intervalle_est_egal(intervalle_creer(1,2), intervalle_creer(1,2))
        True
        >>> intervalle_est_egal(intervalle_creer(1,1), intervalle_creer(1,-2))
        False
        >>> intervalle_est_egal(intervalle_creer(1,2), intervalle_creer(1,3))
        False
        >>> intervalle_est_egal(intervalle_creer(0,3), intervalle_creer(1,3))
        False
    """ 
    return intervalle_to_str(i) == intervalle_to_str(j)


def intervalle_est_inclus(i: Intervalle, j:Intervalle) -> bool:
    """Indique si l'intervalle `j` est inclus dans l'intervalle `i`.

    Parameters
    ----------
    i : (Intervalle) intervalle de référence
    j : (Intervalle) intervalle à étudier

    Returns
    -------
    (bool) `True` ssi j est inclus dans i.count
    
    Examples
    --------
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(-1,2))
        True
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(-1,1))
        True
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(0,2))
        True
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(-1,-1))
        True
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(5,5))
        False
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(-1,3))
        False
        >>> intervalle_est_inclus(intervalle_creer(-1,2), intervalle_creer(-1.01,0))
        False
        >>> intervalle_est_inclus(intervalle_creer(1,1), intervalle_creer(2,2))
        False
        >>> intervalle_est_inclus(intervalle_creer(1,1), intervalle_creer(0,2))
        False
    """
    if intervalle_est_vide(j):  # l'intervalle vide est inclus dans tous les intervalles
        return True

    if intervalle_est_vide(i):  # aucun intervalle (sauf le vide) n'est inclus dans l'intervalle vide
        return False
    
    a_0: float = intervalle_borne_inf(i)
    b_0: float = intervalle_borne_sup(i)
    a_1: float = intervalle_borne_inf(j)
    b_1: float = intervalle_borne_sup(j)
    return a_0 <= a_1 and b_1 <= b_0


def intervalle_intersection(i: Intervalle, j: Intervalle) -> Intervalle:
    """Intersection de deux intervalles.

    Parameters
    ----------
    i : (Intervalle)
    j : (Intervalle)

    Returns
    -------
    (Intervalle) intersection de deux intervalles.

    Examples
    --------
        >>> I = intervalle_creer(1,3)
        >>> J = intervalle_creer(2,4)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[2 ; 3]'
        >>> I = intervalle_creer(-1,3)
        >>> J = intervalle_creer(3,4)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[3 ; 3]'
        >>> I = intervalle_creer(1,3)
        >>> J = intervalle_creer(5,8)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[]'
        >>> I = intervalle_creer(1,1)
        >>> J = intervalle_creer(2,4)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[]'
        >>> I = intervalle_creer(1,1)
        >>> J = intervalle_creer(0,4)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[1 ; 1]'
        >>> I = intervalle_creer(1,3)
        >>> J = intervalle_creer(2,2)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[2 ; 2]'
        >>> I = intervalle_creer(-1,3)
        >>> J = intervalle_creer(-2,5)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[-1 ; 3]'
        >>> I = intervalle_creer(-10,30)
        >>> J = intervalle_creer(-2,5)
        >>> intervalle_to_str(intervalle_intersection(I,J))
        '[-2 ; 5]'
    """
    # intervalle(s) vide(s)
    if intervalle_est_vide(i) or intervalle_est_vide(j):
        return intervalle_creer(0,0)
    
    a_0: float = intervalle_borne_inf(i)
    b_0: float = intervalle_borne_sup(i)
    a_1: float = intervalle_borne_inf(j)
    b_1: float = intervalle_borne_sup(j)
    
    a: float = max(a_0, a_1)
    b: float = min(b_0, b_1)
    return intervalle_creer(a,b)


testmod()
