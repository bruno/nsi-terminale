----------------
-- Exercice 1 --
----------------

-- a. Afficher le contenu de la relation `Specialite` (attention, sans accent) rangé par ordre alphabétique.

SELECT intitule AS 'ex1a - intitule'
FROM Specialite
ORDER BY intitule ;



-- b. Donner les idendifiants `pid` des professeurs enseignant la spécialité au groupe dont le `grpid` est `1SVTG1`.

SELECT pid AS 'ex 1b - pid'
FROM Enseigne E
WHERE E.grpid LIKE '1SVTG1';


    
-- c. Donner le nom et le prénom des élèves de terminale.  

SELECT nom AS 'ex 1c - nom', prenom
FROM Lyceen
WHERE niveau = 'Term';



-- d. Donner le nombre d'élèves qui sont dans le groupe `1MathG1`

SELECT count(*) AS 'ex 1d - total'
FROM Affectation
WHERE grpid = '1MathG1';



-- e. Donner les identifiants des élèves qui sont dans un groupe de Math de première.
    
SELECT id AS 'ex 1e - id'
FROM Affectation
WHERE grpid LIKE '%1math%';



----------------
-- Exercice 2 --
----------------


-- a. Afficher les `nom`, `prenom` et `grpid` de tous les lycéens. Pour cela, utiliser une jointure entre les tables `lyceen` et `affectation`.

SELECT L.nom AS 'ex 2a - nom', L.prenom, A.grpid
FROM lyceen L
    JOIN affectation A ON L.id=A.id ;



   
-- b. Afficher le `nom` de tous les professeurs ainsi que l'identifiant `grpid` des groupes auxquels ils enseignent leur spécialité. Pour cela, utiliser une jointure entre les tables `professeur` et `enseigne`.

SELECT P.nom AS 'ex 2b - nom', E.grpid
FROM professeur P
    JOIN enseigne E ON P.pid=E.pid;



    
-- c. Afficher les noms et prénoms de tous les élèves affectés au groupe de spécialité 1MathG1.

SELECT L.nom AS 'ex 2c - nom', L.prenom
FROM lyceen L
    JOIN affectation A ON L.id=A.id
WHERE A.grpid = '1MathG1';


    
-- d. Afficher les noms des professeurs enseignant dans un des groupes 1PhysG1, 1PhysG2, 1PhysG3.

SELECT DISTINCT P.nom AS 'ex 2d - nom'
FROM professeur P
    JOIN enseigne E ON P.pid=E.pid
WHERE grpid LIKE '%1phys%';





----------------
-- Exercice 3 --
----------------

-- Afficher les noms des élèves suivant la spécialité d'intitulé "Physique-Chimie".

SELECT L.nom AS 'ex 3 - nom'
FROM Lyceen L
    JOIN Affectation AS A ON L.id=A.id
    JOIN Groupe G ON G.grpid=A.grpid
WHERE G.intitule = 'Physique-Chimie';




----------------
-- Exercice 4 --
----------------

    
-- a. Utiliser une jointure entre les tables `Lyceen`, `Affectation` et `Groupe` pour afficher pour chaque lycéen de Terminale ses `nom` et `prenom` avec l'`intitulé` de chacune des spécialités où il est inscrit.

SELECT L.nom AS 'ex 4a - nom', L.prenom, G.intitule
FROM Lyceen L
    JOIN Affectation AS A ON L.id=A.id
    JOIN Groupe G ON G.grpid=A.grpid
WHERE L.niveau = 'Term'
;



-- b. Afficher la liste des noms des professeurs enseignant la spécialité "Numérique et Sciences Informatiques".

SELECT P.nom AS 'ex 4b - nom'
FROM Professeur as P
    JOIN Enseigne as E on P.pid = E.pid
WHERE E.grpid LIKE '%NSI%';



-- c. Compter le nombre total d'élèves inscrits dans un groupe de spécialité ayant l'intitulé "Mathématiques".

SELECT count(*) AS 'ex 4c - total'
FROM Lyceen as L
    JOIN Affectation as A on L.id = A.id
WHERE A.grpid LIKE '%Math%';



-- d. Afficher les intitulés des spécialités où intervient Mme Germain.

SELECT G.intitule AS 'ex 4d - intitule'
FROM Professeur as P
    JOIN Enseigne as E on P.pid = E.pid
    JOIN Groupe as G on E.grpid = G.grpid
WHERE P.nom = 'Mme Germain';



-- e. Afficher la liste de tous les identifiants des groupes de spécialité de Première, avec le nom de leur enseignant et leur intitulé, par ordre alphabétique de ```grpid```.

SELECT G.grpid AS 'ex 4e - grpid', P.nom, G.intitule 
FROM Groupe as G
    JOIN Enseigne as E on G.grpid = E.grpid
    JOIN Professeur as P on E.pid = P.pid
WHERE G.niveau = 'Prem'
ORDER BY G.grpid asc;


-- f. Afficher les noms des professeurs de spécialité de Samuel Rainier.
SELECT P.nom AS 'ex 4f - nom'
FROM Lyceen L
    JOIN Affectation A ON A.id = L.id
    JOIN Enseigne E ON E.grpid = A.grpid
    JOIN Professeur P ON P.pid = E.pid
WHERE L.nom LIKE 'rainier'
;


-- g. Afficher la liste des noms des élèves ayant Mme Germain comme professeur de NSI.
SELECT L.nom AS 'ex 4g - nom'
FROM Lyceen L
    JOIN Affectation A ON A.id = L.id
    JOIN Enseigne E ON E.grpid = A.grpid
    JOIN Professeur P ON P.pid = E.pid
WHERE P.nom LIKE '%germain%'
    AND A.grpid LIKE '%nsi%'
;

