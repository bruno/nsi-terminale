PRAGMA foreign_keys=ON;
DROP TABLE IF EXISTS S;
DROP TABLE IF EXISTS R;


CREATE TABLE R 
(
    a integer PRIMARY KEY,
    b integer,
    c integer
);

CREATE TABLE S
(
    a integer REFERENCES R(a),
    e integer,
    PRIMARY KEY (a,e)
);

INSERT INTO R VALUES
    (1, 10, 100),
    (2, 20, 200),
    (3, 30, 300);

INSERT INTO S VALUES
    (1, 12),
    (1, 13),
    (1, 14),
    (3, 32),
    (3, 33);
