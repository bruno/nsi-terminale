"""
voir la page : https://docs.python.org/fr/3/library/sqlite3.html
pour un petit tuto en français sur sqlite 3 et python
"""

import sqlite3
con = sqlite3.connect("02_bases-de-donnees_activite_requetes_ex1.db")
cur = con.cursor()


print("## Tableau général")
res = cur.execute(
    """
    SELECT l.id, l.titre, a.nom, a.prenom, a.annee_naissance, l.langue, l.annee_publication, l.note
    FROM Auteur_de JOIN Livre as l ON Auteur_de.id_livre = l.id
        JOIN Auteur as a ON Auteur_de.id_auteur = a.id ;
    """
)
for row in res:
    print(row)
print()


print("## Tous les livres (seulement id et titre)")
res = cur.execute(
    """
    SELECT id, titre
    FROM Livre;
    """
)
for row in res:
    print(row)
print()


print("## Tous les auteurs")
res = cur.execute("SELECT * FROM Auteur")
for row in res:
    print(row)

con.close()
