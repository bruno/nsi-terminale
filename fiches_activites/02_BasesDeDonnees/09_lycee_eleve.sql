-- DS SQL
-- fichier à compléter et à déposer sur Moodle



----------------
-- Exercice 1 --
----------------

-- a. Afficher le contenu de la relation `Specialite` (attention, sans accent) rangé par ordre alphabétique.



-- b. Donner les idendifiants `pid` des professeurs enseignant la spécialité au groupe dont le `grpid` est `1SVTG1`.


    
-- c. Donner le nom et le prénom des élèves de terminale.  



-- d. Donner le nombre d'élèves qui sont dans le groupe `1MathG1`.



-- e. Donner les identifiants des élèves qui sont dans un groupe de Math de première.





----------------
-- Exercice 2 --
----------------


-- a. Afficher les `nom`, `prenom` et `grpid` de tous les lycéens. Pour cela, utiliser une jointure entre les tables `lyceen` et `affectation`.


   
-- b. Afficher le `nom` de tous les professeurs ainsi que l'identifiant `grpid` des groupes auxquels ils enseignent leur spécialité. Pour cela, utiliser une jointure entre les tables `professeur` et `enseigne`.



-- c. Afficher les noms et prénoms de tous les élèves affectés au groupe de spécialité 1MathG1.



-- d. Afficher les noms des professeurs enseignant dans un des groupes 1PhysG1, 1PhysG2, 1PhysG3.





----------------
-- Exercice 3 --
----------------

-- Afficher les noms des élèves suivant la spécialité d'intitulé "Physique-Chimie".






----------------
-- Exercice 4 --
----------------

    
-- a. Utiliser une jointure entre les tables `Lyceen`, `Affectation` et `Groupe` pour afficher pour chaque lycéen de Terminale ses `nom` et `prenom` avec l'`intitulé` de chacune des spécialités où il est inscrit.  



-- b. Afficher la liste des noms des professeurs enseignant la spécialité "Numérique et Sciences Informatiques".



-- c. Compter le nombre total d'élèves inscrits dans un groupe de spécialité ayant l'intitulé "Mathématiques".



-- d. Afficher les intitulés des spécialités où intervient Mme Germain.



-- e. Afficher la liste de tous les identifiants des groupes de spécialité de Première, avec le nom de leur enseignant et leur intitulé, par ordre alphabétique de ```grpid```.



-- f. Afficher les noms des professeurs de spécialité de Samuel Rainier.



-- g. Afficher la liste des noms des élèves ayant Mme Germain comme professeur de NSI.


