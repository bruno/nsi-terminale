PRAGMA foreign_keys=ON;
DROP TABLE IF EXISTS Employes;
DROP TABLE IF EXISTS Departements;


CREATE TABLE Departements
(
    numero INTEGER,
    nom TEXT,
    directeur INTEGER,
    ville TEXT,
    PRIMARY KEY (numero)
);

CREATE TABLE Employes
(
    numero INTEGER,
    nom TEXT,
    profession TEXT,
    date_embauche DATE,
    salaire REAL,
    commission BOOLEAN,
    numero_dep INTEGER,
    PRIMARY KEY (numero),
    FOREIGN KEY (numero_dep) REFERENCES Departements(numero)
);
