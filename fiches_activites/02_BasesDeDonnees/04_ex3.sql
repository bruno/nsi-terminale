PRAGMA foreign_keys=ON;
DROP TABLE IF EXISTS Programmer;
DROP TABLE IF EXISTS Musicien;
DROP TABLE IF EXISTS Representation;


CREATE TABLE Representation 
(
    idRep INTEGER,
    titreRep TEXT,
    lieu TEXT,
    PRIMARY KEY (idRep)
);


CREATE TABLE Musicien
(
    idMus INTEGER,
    nom TEXT,
    idRep INTEGER,
    PRIMARY KEY (idMus),
    FOREIGN KEY (idRep) REFERENCES Representation(idRep)
);


CREATE TABLE Programmer
(
    date DATE,
    idRep INTEGER,
    tarif REAL,
    PRIMARY KEY (date, idRep),
    FOREIGN KEY (idRep) REFERENCES Representation(idRep)
);