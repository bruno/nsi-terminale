PRAGMA foreign_keys=ON;

DROP TABLE IF EXISTS Auteur_de;
DROP TABLE IF EXISTS Livre;
DROP TABLE IF EXISTS Auteur;



CREATE TABLE Auteur
(
    ID PRIMARY KEY,
    Nom text,
    Prenom text,
    Naissance integer,
    Langue text
);


CREATE TABLE Livre
(
    ID integer PRIMARY KEY,
    Titre text,
    Publication integer,
    Note integer,
    Auteur integer REFERENCES Auteur(ID)
);


INSERT INTO Auteur (ID, Nom, Prenom, Naissance, Langue) VALUES
    (1, 'Orwell', 'George', 1903, 'anglais'),
    (2, 'Herbert', 'Frank', 1920, 'anglais'),
    (3, 'Asimov', 'Isaac', 1920, 'anglais'),
    (4, 'Huxley', 'Aldous', 1894, 'anglais'),
    (5, 'Bradbury', 'Ray', 1920, 'anglais'), 
    (6, 'K.Dick', 'Philip', 1928, 'anglais'),
    (7, 'Barjavel', 'René', 1911, 'français'),
    (8, 'Boulle', 'Pierre', 1912, 'français'),
    (9, 'Van Vogt', 'Alfred Elton', 1912, 'anglais'),
    (10, 'Verne', 'Jules', 1828, 'français');


INSERT INTO Livre (ID, Titre, Publication, Note, Auteur) VALUES
    (1, '1984', 1949, 10, 1),
    (2, 'Dune', 1965, 8, 2),
    (3, 'Fondation', 1951, 9, 3),
    (4, 'Le meilleur des mondes', 1931, 7, 4),
    (5, 'Fahrenheit 451', 1953, 7, 5),
    (6, 'Ubik', 1969, 9, 6),
    (7, 'Chroniques martiennes', 1950, 8, 5),
    (8, 'La nuit des temps', 1968, 7, 7),
    (9, 'Blade Runner', 1968, 8, 6),
    (10, 'Les Robots', 1950, 9, 3),
    (11, 'La Planète des singes', 1963, 8, 8),
    (12, 'Ravage', 1943, 8, 7),
    (13, 'Le Maître du Haut Château', 1962, 8, 6),
    (14, 'Le monde des ¯A', 1945, 7, 9),
    (15, "La Fin de l'éternité", 1955, 8, 3),
    (16, 'De la Terre à la Lune', 1865, 10, 10);



SELECT l.ID, l.Titre, a.Nom, a.Prenom, a.Naissance, a.Langue, l.Publication, l.Note
FROM Livre as l JOIN Auteur as a ON l.Auteur = a.id ;


SELECT id, titre
FROM Livre;

