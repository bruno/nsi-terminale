---
title: "Langage et programmation : Modularité"
author: ""
date: "2023-12-11"
subject: "Modularité"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...

## Activités introductives

1. Parmi les lignes qui suivent, trouver celle qui ne permet pas d'importer
et d'utiliser la totalité du module itertools :

a. import itertools
b. import itertools as itt
c. from itertools import cycle


2. On souhaite écrire une portion de code qui permette de savoir si une
année est bissextile ou non. Qu'est-ce qui est Le plus approprié ?

a. Écrire un programme principal qui demande à l'utilisateur de taper
une année et qui indique si elle est bissextile.
b. Écrire une fonction qui indique si une année passée en paramètre
est bissextile ou non en renvoyant un booléen.
c. Écrire un module bissextile.py qui contiendra tout ce qu'il faut
pour tester Le caractère bissextile d'une année.  


## Openweather

Le site OpenWeather propose une API permettant de consulter les données
météorologique de n'importe quel endroit sur Terre.

La requête est formulée via une adresse html selon ce modèle :

```html
https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}&units=metric
```

- *lat* et *long* désigne la latitude et la longitude de l'endroit et *appid* 
correspond à une clé d'identifiant.

- *unit* permet de spécifier les unités de mesure en mètres et en degré Celsius.

Voilà un exemple de réponse obtenue par une requête :

```json
{
  "coord": {
    "lon": 10.99,
    "lat": 44.34
  },
  "weather": [
    {
      "id": 501,
      "main": "Rain",
      "description": "moderate rain",
      "icon": "10d"
    }
  ],
  "base": "stations",
  "main": {
    "temp": 298.48,
    "feels_like": 298.74,
    "temp_min": 297.56,
    "temp_max": 300.05,
    "pressure": 1015,
    "humidity": 64,
    "sea_level": 1015,
    "grnd_level": 933
  },
  "visibility": 10000,
  "wind": {
    "speed": 0.62,
    "deg": 349,
    "gust": 1.18
  },
  "rain": {
    "1h": 3.16
  },
  "clouds": {
    "all": 100
  },
  ...
}                        
```

Le format JSON est un fichier texte facilement manipulable par d'autres 
langages (que JavaScript) car il est basé sur des paires clé/valeur et est 
donc très proche d'un point de vue syntaxique des dictionnaires de Python, 
des tableaux associatifs de PHP, etc.

Pour importer dans Python le contenu d'un fichier JSON, on peut utiliser la 
méthode loads du module json à partir d'un fichier ouvert.

```python
import json
fichier = open("data.json") # ouverture du flux de lecture
contenu = json.load(fichier) # mémorisation du fichier json dans la variable contenu
fichier.close() # on ferme le flux de lecture
contenu
```

Ici, la variable *contenu* est un dictionnaire Python (présence des accolades).

On peut alors accéder à tous les éléments du fichier JSON de départ en utilisant la 
notation avec les crochets et en utilisant les clés ou les indices selon le type d'objet.

### Questions

1. Écrire la requête permettant d'obtenir les données météorologiques sur Salon-de-Provence.
2. On supose que l'on a récupéré les données météo dans un dictionnaire. Écrire les lignes de codes 
   permettant de récupérer :
   a.  la température min, max, l'humidité et la pression dans un dictionnaire.
   b. la vitesse et la direction du vent dans un tableau.