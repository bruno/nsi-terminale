---
title: "Langage et programmation : Mise au point de programme"
author: "d'après NSI Prépabac, Hatier"
date: "2023-09-04"
subject: "MiseAuPoint"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...



## Activité 1 : identifier l'exception

**Consigne** :

Pour chacun des programmes ci-dessous identifier le type d'exception qui sera levée
si on tente de l'exécuter. Préciser l'origine de l'erreur.

*Types d'exception possibles: NameError, SyntaxError, IndexError, IndentationError.*

### Programme 1

```python
a = 1
for i in range(3):
    print("i = {}, a = {}".format(i, a)
    a = 2 * a
```

### Programme 2

```python
v=i
while v < 100 :
    if v % 7 == 0:
        print(v, "est un multiple de 7")
        else :
        print(v, "n'est pas un multiple de 7")
    v = v + 1
```

### Programme 3

```python
f = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
for i in range(1, 10):
    f[i +1] = f[i] + f[i-1]
print(f)
```

\newpage


## Activité 2 : rechercher les erreurs courantes

Les programme ci-dessous contient quelques erreurs courantes.

**Consigne** : 

 - relever et expliquer les erreurs; 
 - classer les erreurs selon leur type.

```python
import string

def traverse_rotor(alphabet, lettre):
    pos = alphabet.index(lettre
    return rotor[pos + 1]

def chiffre_lettre(rotors, lettre):
    alphabet = string.ascii_uppercase
    for rot in rotors:
        lettre = traversee_roto(alphabet, rot, lettre)
      for rot in reversed(rotors[:-1]):
        lettre = traverse_rotor(rot, alphabet, lettre)
    return lettre

def tourne_rotor(rotor):
    return rotor[1:] + rotor[0]

def chiffre_message(rotors, message):
    c_rotors = [r for r in rotors]
    res = []
    for lettre in message
        res.appand(chiffre_lettre(c_rotors, lettre))
        for k in range(len(c_rotors)) - 1 :
            c_rotors[k] = tourne_rotor(c_rotors[k)]
            if c_rotors[k][0] != rotors[k][0]:
                break
    return "".join(res)


rotor1 = "EKMFLGDQVZNTOWYHXUSPAIBRCJ
rotor2 = "AJDKSIRUXBLHWTMCQGZNPYFVOE"
rotor3 = "BDFHLJLCPRTXVZNYEIWGAKMUSQO"
deflec = "YRUHQSLDPXNGOKMIEBFZCWJVAT"
rotors = [rotor1, rotor2, rotor3, deflec]
chiffre_message(rotors, 

'LALFVLTXEKFZLUITBUSBDAJHBKYTAYTVU')
```