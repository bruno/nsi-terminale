import string

def traverse_rotor(alphabet, rotor, lettre):
    pos = alphabet.index(lettre)
    print(f"pos : {pos}, lettre : {lettre}, rotor pos : {rotor[pos]}")
    return rotor[pos-1]

def chiffre_lettre(rotors, lettre):
    alphabet = string.ascii_uppercase
    for rot in rotors:
        lettre = traverse_rotor(alphabet, rot, lettre)
    for rot in reversed(rotors[:-1]):
        lettre = traverse_rotor(rot, alphabet, lettre)
    return lettre

def tourne_rotor(rotor):
    return rotor[1:] + rotor[0]

def chiffre_message(rotors, message):
    c_rotors = [r for r in rotors]
    res = []
    for lettre in message :
        res.append(chiffre_lettre(c_rotors, lettre))
        for k in range(len(c_rotors) - 1) :
            c_rotors[k] = tourne_rotor(c_rotors[k])
            if c_rotors[k][0] != rotors[k][0]:
                break
    return "".join(res)

rotor1 = "EKMFLGDQVZNTOWYHXUSPAIBRCJ"
rotor2 = "AJDKSIRUXBLHWTMCQGZNPYFVOE"
rotor3 = "BDFHLJLCPRTXVZNYEIWGAKMUSQO"
deflec = "YRUHQSLDPXNGOKMIEBFZCWJVAT"
rotors = [rotor1, rotor2, rotor3, deflec]
print(chiffre_message(rotors,
'LARMYBTEVLJEPGIFYJJPYDKLXHRKOGCWUNTKYSHMUFANLDDUT'))