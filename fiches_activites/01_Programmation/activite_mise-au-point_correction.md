---
title: "Langage et programmation : Mise au point de programme"
author: "d'après NSI Prépabac, Hatier"
date: "2023-09-04"
subject: "MiseAuPoint"
keywords: [NSI]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
header-includes:
- |
  ```{=latex}
  \usepackage{tcolorbox}

  \newtcolorbox{info-box}{colback=cyan!5!white,arc=0pt,outer arc=0pt,colframe=cyan!60!black}
  \newtcolorbox{warning-box}{colback=orange!5!white,arc=0pt,outer arc=0pt,colframe=orange!80!black}
  \newtcolorbox{error-box}{colback=red!5!white,arc=0pt,outer arc=0pt,colframe=red!75!black}
  ```
pandoc-latex-environment:
  tcolorbox: [box]
  info-box: [info]
  warning-box: [warning]
  error-box: [error]
...



## Activité 1 : identifier l'exception

**Consigne** :

Pour chacun des programmes ci-dessous identifier le type d'exception qui sera levée
si on tente de l'exécuter. Préciser l'origine de l'erreur.

*Types d'exception possibles: NameError, SyntaxError, IndexError, IndentationError.*

### Programme 1

```python
a = 1
for i in range(3):
    print("i = {}, a = {}".format(i, a)
    a = 2 * a
```
::: info
L'exception SyntaxError est levée ligne 4. Mais l’erreur est en fait
ligne 3 : il manque une parenthèse fermante.
:::

### Programme 2

```python
v=i
while v < 100 :
    if v % 7 == 0:
        print(v, "est un multiple de 7")
        else :
        print(v, "n'est pas un multiple de 7")
    v = v + 1
```

::: info
L'exception IndentationError est levée. La ligne 5 est mal indentée, elle 
doit être alignée avec le `if` de la ligne 3.
:::

### Programme 3

```python
f = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
for i in range(1, 10):
    f[i +1] = f[i] + f[i-1]
print(f)
```
::: info
L'exception IndexError est levée. Lors du dernier tour de boucle, `i` vaut 9. 
On essaie alors de mettre à jour la valeur d'indice `i + 1` (c’est-à-dire 10) de `f`. 
Or `f` est un tableau de longueur 10 dont les indices valides vont de 0 à 9 inclus. Tenter d'accéder à l'élément 10 provoque donc la levée d’une exception.
:::

\newpage


## Activité 2 : rechercher les erreurs courantes

Les programme ci-dessous contient quelques erreurs courantes.

**Consigne** : 

 - relever et expliquer les erreurs; 
 - classer les erreurs selon leur type.

```python
import string

def traverse_rotor(alphabet, lettre):
    pos = alphabet.index(lettre
    return rotor[pos + 1]

def chiffre_lettre(rotors, lettre):
    alphabet = string.ascii_uppercase
    for rot in rotors:
        lettre = traversee_roto(alphabet, rot, lettre)
      for rot in reversed(rotors[:-1]):
        lettre = traverse_rotor(rot, alphabet, lettre)
    return lettre

def tourne_rotor(rotor):
    return rotor[1:] + rotor[0]

def chiffre_message(rotors, message):
    c_rotors = [r for r in rotors]
    res = []
    for lettre in message
        res.appand(chiffre_lettre(c_rotors, lettre))
        for k in range(len(c_rotors)) - 1 :
            c_rotors[k] = tourne_rotor(c_rotors[k)]
            if c_rotors[k][0] != rotors[k][0]:
                break
    return "".join(res)


rotor1 = "EKMFLGDQVZNTOWYHXUSPAIBRCJ
rotor2 = "AJDKSIRUXBLHWTMCQGZNPYFVOE"
rotor3 = "BDFHLJLCPRTXVZNYEIWGAKMUSQO"
deflec = "YRUHQSLDPXNGOKMIEBFZCWJVAT"
rotors = [rotor1, rotor2, rotor3, deflec]
chiffre_message(rotors, 

'LALFVLTXEKFZLUITBUSBDAJHBKYTAYTVU')
```

::: info

**Erreurs de syntaxe**

*SyntaxError* : erreur de parenthèses, ponctuation manquante.

- `invalid syntax`: (ligne 4) il manque une parenthèse.
Cette erreur est détectée ligne 5 même si elle figure sur la ligne 4.
- `invalid syntax`: (ligne 24) le parenthésage est incorrect, (ligne 21)
il manque « : » en fin de ligne.
- `EOL while scanning string litteral` : (ligne 30) guillemets
non refermés avant la fin de la ligne.  

**Erreurs d'indexation**  

*IndexError* : accès à une position en dehors d’une liste.  

`string index out of range`: (ligne 5) rotor et alphabet
contiennent tous les deux 26 lettres. Écrire pos + 1 implique
qu'on essaie parfois d'accéder à la 27° lettre.

**Erreurs de nom**  

*NameError* : nom de fonction ou de variable mal orthographié.  

`name 'traversee rotor' is not defined`:(ligne 10)
il y a une faute dans le nom de la fonction `traverse_rotor`.  

**Erreurs d’indentation**  

*IndentationError* : indentation oubliée ou au contraire ajoutée.  

`unindent does not match any outer indentation level`: (ligne 11) ligne mal 
indentée, l'interpréteur ne peut pas déterminer si elle est dans le bloc `for` 
de la ligne 9 ou après.

**Erreurs d’attribut**  

*AttributeError* : accès à une méthode ou à un attribut inconnu.  

`'list' object has no attribute 'appand'` : (ligne 22)
`res` est une liste, et le nom de la méthode `append` a été mal orthographié.  

**Erreurs de type**  

*TypeError* : types incompatibles pour l'opération demandée.  

`unsupported operand type(s) for '-': 'range' and 'int'` :
(ligne 23) la dernière parenthèse fermante est mal placée, Python
se retrouve à devoir soustraire 1 (un entier) à un objet de type `range`.
La valeur 1 devrait être soustraite à `len(c_rotors)`.
::: 