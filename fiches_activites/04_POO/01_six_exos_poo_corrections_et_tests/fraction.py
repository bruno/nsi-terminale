"""
---
## Exercice 6 Des fractions
On représente une fraction par un objet qui a deux attributs : numerateur et denominateur qui sont des entiers.<br>
 1. Ecrire le début du code d'une classe Fraction.<br>
On doit pouvoir écrire <br>
`f = Fraction( 2, 3) `

2. Ajouter une méthode `representation` qui renvoie une chaine de caractères représentant la fraction sous forme a/b <br>
 `>>> f.representation()`<br>
 `2/3`
 
 **Remarque (hors programme)** En python la méthode `__repr__(self)` est appelée pour représenter un objet lors d'un print.<br>
 Si vous appelez votre méthode `__repr__(self)` alors elle sera automatiquement appelée par python dans :<br>
 `print(f)`<br>
 `2/3`
3. Programmer une méthode `egale(self, g)` qui renvoie True si et seulement si les deux fractions sont égales. <br>
4. Programmer les méthodes `somme(self, f)` et `produit(self,f)` qui vont effectuer la somme et le produit de la fraction `self`avec la fraction `f` et renvoyer l'objet fraction représentant le résultat. <br>
5. Programmer une méthode ```simplifier``` qui rend la fraction irréductible. 
"""

############## Réponse ###########
from doctest import testmod

class Fraction:
    """ Classe de fraction

    Parameters
    ----------
    numerateur : (int)
        numérateur de la fraction
    denominateur : (int)
        dénominateur de la fraction
    """
    def __init__(self, numerateur, denominateur):
        self.num = numerateur
        self.denom = denominateur


    def representation(self) -> str:
        """Affichage d'une fraction sous la forme 'n/d'

        Returns
        -------
        str
            fraction affichée au format 'num/denom'
        
        Examples
        --------
            >>> f = Fraction(2, 3)
            >>> str(f)
            '2/3'
        """
        texte = str(self.num) + "/" + str(self.denom)
        return texte


    def __repr__(self):
        """ Sucre syntaxique Python """
        return  self.representation()


    def egale(self, f) -> bool:
        """ Égalité entre fraction

        Parameters
        ----------
        f : autre fraction

        Returns
        -------
        bool
            True ssi les deux fractions représentent le même nombre rationnel
        
        Example
        -------
            >>> f_0 = Fraction(2, 3)
            >>> f_1 = Fraction(4, 6)
            >>> f_0 == f_1
            True
            >>> f_2 = Fraction(1, 2)
            >>> f_0 == f_2
            False
        """
        return self.num * f.denom == self.denom * f.num


    def __eq__(self, f) -> bool:
        """ Sucre syntaxique Python """
        return self.egale(f)


    def simplifier(self):
        """
        >>> f = Fraction(2, 3)
        >>> f.simplifier()
        >>> str(f)
        '2/3'
        >>> f = Fraction(4, 6)
        >>> f.simplifier()
        >>> str(f)
        '2/3'
        """
        d = self.denom
        n = self.num
        # calcul du PGCD
        pgcd = min(d, n)
        while not ( pgcd == 1 or (n % pgcd ==0 and d % pgcd ==0)):
            pgcd -= 1
        # simplification
        self.num = n //pgcd
        self.denom = d // pgcd


    def somme (self, f):
        """
            >>> f_0 = Fraction(2, 3)    # 2/3
            >>> f_1 = Fraction(4, 6)    # 4/6
            >>> somme = f_0 + f_1       # AVEC simplification
            >>> str(somme)                  # 2/3 + 4/6 = 8/6 = 4/3
            '4/3'
            >>> f_2 = Fraction(2,5)     # 2/5
            >>> somme = f_0 + f_2       # SANS simplification
            >>> str(somme)                  # 2/3 + 2/5 = 16/15
            '16/15'
        """
        num = self.num * f.denom + f.num * self.denom
        denom = self.denom * f.denom
        somme = Fraction(num, denom)
        somme.simplifier()
        return somme


    def __add__ (self, f):
        """ Sucre syntaxique Python """
        return self.somme(f)


    def produit(self, f):
        """
            >>> f_0 = Fraction(2, 3)    # 2/3
            >>> f_1 = Fraction(4, 6)    # 4/6
            >>> prod = f_0 * f_1
            >>> str(prod)
            '4/9'
        """
        num = self.num * f.num
        denom = self.denom * f.denom
        prod = Fraction(num, denom)
        prod.simplifier()
        return prod


    def __mul__(self, f):
        """ Sucre syntaxique Python """
        return self.produit(f)


testmod()
