"""
## Exercice 2 La serrure.
Une serrure fonctionne avec un **code** secret qui est une séquence de 4 chiffres. La serrure peut être **verrouillée ou déverrouillée**. Pour déverrouiller la serrure il faut fournir le bon code.
 1. Quelles sont les données décrivant l'état d'un objet serrure ? 
 2. Quels types python peut-on utiliser pour ces données ?
 3. Quels attributs sont nécessaires pour un objet serrure ?
 4. Ecrire le **squelette** d'une classe Serrure.
  Cette classe doit implémenter les méthodes suivantes :
  - `verrouiller()` met la serrure en etat **verrouillée**. Il n'y a pas besoin de fournir le code pour verrouiller une serrure.
  - `deverrouiller(code) ` deverrouille la serrure si le code est egale au code secret.<br>
    Renvoie True si le deverrouillage a réussi, False sinon.
  - `changer_code_secret(ancien_code, nouveau_code)` remplace le code secret par `nouveau_code` si ancien_code est le code secret et si `nouveau_code` est valide,
    renvoie True si le changement a réussi, False sinon
    
 Cette classe doit permettre d'écrire le code suivant :
 
 `>>> ma_serrure = Serrure('1925')      # le code de la serrure est 1925` <br>
 `>>> ma_serrure.verrouillee           # etat de la serrure`     
 `True`<br>
 `>>> ma_serrure.deverrouiller('1965')  # tentative de deverouillage`<br>
 `    False` 
 5. Compléter le code de chaque méthode.
 6. Documenter le code.
 7. Proposer un petit scénario de test.
"""

############### réponses ###########

"""
question 1
----------
L'état d'une serrure est donné par son code et son état de verrouillé ou non.


Question 2
----------
* Pour le code il faut une chaîne de caractère. En effet, si on utilise
un nombre entier, tout code commençant par '0' ne sera pas un nombre entier
à 4 chiffres, mais à 3 chiffres.
* Pour l'état, un booléen suffit puisqu'il y a deux états possibles.

Question 3
---------
Les attributs sont :
* code:str
* est_verrouille:bool
"""

# Question 4
# ----------
from doctest import testmod

class Serrure:
    """
    Classe qui défini les objets de type Serrure.
    La serrure est initialement verrouillée.

    Parameters
    ----------
    code : (str)
        code de la serrure
    
    Examples
    --------
        >>> s = Serrure('1925')
        >>> print(s.est_verrouillee)
        True
    """
    def __init__(self, code: str):
        self.code: str = code
        self.est_verrouillee: bool = True


    def verrouiller(self) -> bool:
        """Verrouille la serrure.

        Returns
        -------
        bool
            True lorsque la serrure est verrouillée
        """
        self.est_verrouillee = True
        return True


    def deverouiller(self, test_code: str) -> bool:
        """ Tente de déverrouiller la serrure avec le `test_code` proposé.

        Parameters
        ----------
        test_code : (str)
            code à tester pour déverrouiller la serrure.
        
        Examples
        --------
            >>> s = Serrure('1925')
            >>> s.deverouiller('1965')
            False
            >>> s.est_verrouillee
            True
            >>> s.deverouiller('1925')
            True
            >>> s.est_verrouillee
            False
        """
        if self.code != test_code:
            return False
        
        self.est_verrouillee = False
        return True


    def changer_code_secret(self, ancien_code:str, nouveau_code:str) -> bool:
        """Change le code de la serrure si l'ancien_code est correct.

        Parameters
        ----------
        ancien_code : (str)
            Code avec modification. Il faut qu'il corresponde au code de la serrure pour que celle-ci soit modifiée
        nouveau_code : (str)
            Nouveau code mis à jour

        Returns
        -------
        (bool)
            True ssi le code a été modifié
        
        Examples
        --------
            >>> s = Serrure('1925')
            >>> s.deverouiller('1925')
            True
            >>> s.changer_code_secret('1900', '1984')
            False
            >>> s.deverouiller('1925')
            True
            >>> s.changer_code_secret('1925', '1984')
            True
            >>> s.deverouiller('1925')
            False
            >>> s.deverouiller('1984')
            True
        """
        if ancien_code != self.code:
            return False
        
        self.code = nouveau_code
        return True


testmod()
