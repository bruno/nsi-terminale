"""
##  Exercice 4 Le coffre fort.
Un coffre fort contient un montant. Ce montant est nul si le coffre est vide.
Le coffre peut être ouvert ou fermé.
Si le coffre est ouvert, on peut le consulter, y déposer de l’argent ou en retirer.
Si le coffre est fermé, ces opérations sont impossibles.
On ne peut pas retirer plus que ce que contient le coffre.

 1. Compléter le code de la classe Coffre
"""

from doctest import testmod

class Coffre:
    """ Une classe pour les coffres-forts.
        
        Parameters
        ----------
        montant : float
            quantité d'argent contenue dans le coffre
    """
    def __init__(self,  montant: float) :
        """ Coffre(montant) , crée un coffre fermé avec montant comme contenu initial """
        assert montant >= 0       # Le montant initial doit être positif ou nul
        self.est_ouvert: bool = False       # par défaut le coffre est fermé
        self.contenu: float = montant


    def ouvrir(self) -> None :
        """
        Met le coffre en état ouvert.

        Examples
        --------
            >>> c = Coffre(1000)
            >>> c.est_ouvert
            False
            >>> c.ouvrir()
            >>> c.est_ouvert
            True
        """
        self.est_ouvert = True


    def fermer(self) -> None:
        """
        Met le coffre en etat fermé.

        Examples
        --------
            >>> c = Coffre(1000)
            >>> c.ouvrir()
            >>> c.est_ouvert
            True
            >>> c.fermer()
            >>> c.est_ouvert
            False
        """
        self.est_ouvert = False


    def consulter(self) -> float:
        """
        Donne le contenu du coffre s'il est ouvert, -1 sinon.

        Examples
        --------
            >>> c = Coffre(1000)
            >>> c.consulter()
            -1
            >>> c.ouvrir()
            >>> c.consulter()
            1000
        """
        if not self.est_ouvert:
            return -1

        return self.contenu


    def deposer(self, montant: float) -> float:
        """
        Ajoute le montant au contenu si le coffre est ouvert.
        Renvoie le montant déposé (0 si fermé).

        Parameters
        ----------
        montant : (float)
            Somme d'argent à déposer dans le coffre.
        
        Returns
        -------
        (float)
            Montant déposé (ou 0 si coffre fermé)

        Examples
        --------
            >>> c = Coffre(1000)
            >>> c.deposer(500)
            0
            >>> c.ouvrir()
            >>> c.consulter()
            1000
            >>> c.deposer(500)
            500
            >>> c.consulter()
            1500
        """
        if not self.est_ouvert:
            return 0

        self.contenu += montant
        return montant


    def retirer(self, montant) :
        """ Retire le montant du coffre s'il est ouvert .

        Parameters
        ----------
        montant : (float)
            Montant à retirer dans la limite du contenu disponible
        
        Returns
        -------
        (float)
            Montant demandé s'il y a assez d'argent
            S'il n'y a pas assez d'argent, renvoie le contenu qu'il restait
            (0 si le coffre n'est pas ouvert)

        Examples
        --------
            >>> c = Coffre(1000)
            >>> c.retirer(250)
            0
            >>> c.ouvrir()
            >>> c.retirer(250)
            250
            >>> c.consulter()
            750
            >>> c.retirer(5000)
            750
            >>> c.consulter()
            0
        """
        if not self.est_ouvert:
            return 0

        if montant <= self.contenu:
            self.contenu -= montant
            return montant

        retrait_reel = self.contenu
        self.contenu = 0
        return retrait_reel


testmod()


# Question 3
"""
Si on écrit `self.ouvret = False` on effectue une affectation.
Donc l'objet aura un attribut de plus : `ouvret`.
"""
