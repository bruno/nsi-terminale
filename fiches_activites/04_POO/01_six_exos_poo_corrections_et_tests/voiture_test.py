import unittest


import A_voiture_eleve as eleve
import A_voiture as prof


def error(txt):
    tab = txt.split('\n')
    result = "\n/!\ \n"
    for l in tab:
        result += '/!\ ' + l + '\n'
    return result + '/!\\'



class Validation(unittest.TestCase):
    def test_exemple(self):
        v_p = prof.Voiture(1957, "grise", 1100)
        try:
            v_e = eleve.Voiture(1957, "grise", 110)
        except:
            e = error("erreur lors de l'utilisation de la classe 'Voiture'\ninstanciation avec 'Voiture(1957, 'grise', 110)' impossible")
            self.fail(e)
        
        a_e = v_e.petite_annonce()
        a_p = v_p.petite_annonce()
        e = error("L'annonce n'est pas exactement celle attentue\nTon annonce est : '{}'\nalors qu'on veut: '{}'".format(a_e, a_p))
        self.assertEqual(v_e.petite_annonce(), v_p.petite_annonce(), msg=e)

    def test_variable_annonce(self):
        try:
            mon_annonce_e = eleve.mon_annonce
        except AttributeError:
            e = error("la variable 'mon_annonce' n'existe pas !")
            self.assertTrue(False, msg=e)
    

    def test_variable_voiture(self):
        try:
            ma_voiture_e = eleve.ma_voiture
        except AttributeError:
            e = error("la variable 'ma_voiture' n'existe pas !")
            self.fail(msg=e)

    def test_annonce(self):
        mon_annonce_p = prof.mon_annonce
        try:
            mon_annonce_e = eleve.mon_annonce
        except AttributeError:
            e = error("la variable 'mon_annonce' n'existe pas !")
            self.assertTrue(False, msg=e)

        e = error("L'annonce n'est pas exactement celle attentue\nTon annonce est : '{}'\nalors qu'on veut: '{}'".format(mon_annonce_e, mon_annonce_p))
        self.assertEqual(mon_annonce_e, mon_annonce_p, msg=e)        
     


if __name__ == '__main__':
    unittest.main()

