"""
---
## Exercice 3 Une Alarme
Une alarme est un système qui émet un son lorsqu'elle est armée et déclenchée.
Par défaut l'alarme est armée mais on peut la désarmer. Dans ce cas elle n'émettra pas de son si on la déclenche.

On souhaite écrire une classe Alarme qui ait le comportement suivant : <br>
`>>> mon_alarme = Alarme ("Pin Pon Pin Pon Pin Pon") `<br>
`>>> mon_alarme.declencher()`<br>
`   Pin Pon Pin Pon Pin Pon `<br>
`>>> mon_alarme.desarmer()`<br>
`>>> mon_alarme.declencher()    # rien ne se passe car l'alarme est désarmée` <br> 

1. Quels sont les attributs et les méthodes des instances de cette classe ? <br>
2. Écrire le squelette de la classe <br>
3. Compléter le code des méthodes. <br>
4. Proposer un scénario de test 
"""

############### réponses ###########

# Question 1
"""
les attributs :
  * sonnerie : str => son qui est émis par l'alarme lorsqu'elle se déclenche
  * est_armee : bool => booléen qui indique si l'alarme est armée ou non
les méthodes:
  * declencher : declencher l'alarme sui sonnera si elle est armée
  * armer : arme l'alarme
  * desarmer : désarme l'alarme
"""

# Question 2 - 3 - 4

from doctest import testmod

class Alarme:
    """Alarme initialement armée.

    Parameters
    ----------
    son : str
        son qui sera émis par l'alarme lorsqu'elle se déclenche

    Examples
    --------
        >>> a = Alarme("Pin Pon Pin Pon Pin Pon")
        >>> a.sonnerie              #valeur de la sonnerie
        'Pin Pon Pin Pon Pin Pon'
        >>> a.est_armee             #est ce que l'alarme est armée ?
        True
    """

    def __init__(self, son: str):
        self.est_armee: bool = True
        self.sonnerie: str = son


    def armer(self) -> None:
        """ Arme l'alarme pour qu'elle puisse sonner

        Examples
        --------
            >>> a = Alarme("Pin Pon Pin Pon Pin Pon")
            >>> a.armer()
            >>> a.declencher()
            'Pin Pon Pin Pon Pin Pon'
        """
        self.est_armee = True


    def desarmer(self) -> None:
        """ Désarme l'alarme qui ne sonnera donc pas.

        Examples
        --------
            >>> a = Alarme("Pin Pon Pin Pon Pin Pon")
            >>> a.desarmer()
            >>> a.declencher()          #test de sonnerie
            'désarmée'
        """
        self.est_armee = False


    def declencher(self) -> str:
        """ Fait sonner l'alarme si celle-ci est armée.

        Returns
        -------
        (str)
            Sonnerie (ou 'désarmée' si l'alarme est désarmée)
            
        Examples
        --------
            >>> a = Alarme("Pin Pon Pin Pon Pin Pon")
            >>> a.declencher()          #test de sonnerie
            'Pin Pon Pin Pon Pin Pon'
            >>> a.desarmer()     #modifie l'état de l'alarme
            >>> a.declencher()          #test de sonnerie
            'désarmée'
        """
        if self.est_armee:
            return self.sonnerie
        else:
            return "désarmée"


testmod()
