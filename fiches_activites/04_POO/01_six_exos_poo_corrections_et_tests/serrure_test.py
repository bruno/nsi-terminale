import unittest


import A_serrure_eleve as eleve
import A_serrure as prof


def error(txt):
    tab = txt.split('\n')
    result = "\n/!\ \n"
    for l in tab:
        result += '/!\ ' + l + '\n'
    return result + '/!\\'


class Validation(unittest.TestCase):
    def test_instance_possible(self):
        s = ""
        prop = ""

        try:
            s = eleve.Serrure('1925')
        except:
            e = error("impossible d'instancier 'Serrure'\navec 's = eleve.Serrure('1925')'")
            self.fail(e)
    

    def test_attribut_existe(self):
        s = ""
        prop = ""

        try:
            s = eleve.Serrure('1925')
        except:
            e = error("impossible d'instancier 'Serrure'\navec 's = eleve.Serrure('1925')'")
            self.fail(e)

        try:
            prop = s.est_verrouillee
        except:
            e = error("l'attribut 'est_verrouillee' n'est pas accessible")
            self.fail(e)
    

    def test_initialement_est_verrouillee(self):
        s = ""
        prop = ""

        try:
            s = eleve.Serrure('1925')
        except:
            e = error("impossible d'instancier 'Serrure'\navec 's = eleve.Serrure('1925')'")
            self.fail(e)

        try:
            prop = s.est_verrouillee
        except:
            e = error("l'attribut 'est_verrouillee' n'est pas accessible")
            self.fail(e)

        e = error("initialement, la serrure doit être verrouillée")
        self.assertTrue(prop, msg=e)


    def test_verrouiller(self):
        try:
            s = eleve.Serrure('1925')
            s.est_verrouillee = False
            res = s.verrouiller()
            e = error("la methode 'verrouiller()' doit renvoyer True")
            self.assertTrue(res, msg=e)
            e=error("apres un appel a la methode 'verrouiller()'\nl'attribut est_verrouillee' doit etre a True")
            self.assertTrue(s.est_verrouillee, e)
        except AttributeError:
            e =error("methode 'verrouiller()' inaccessible")
            self.fail(e)


    def test_deverrouiller(self):
        try:
            s = eleve.Serrure('1925')
            e = error("initialement, la serrure doit être verrouillee")
            self.assertTrue(s.est_verrouillee, msg=e)
            res = s.deverouiller('1965')
            e = error("deverouiller(code) renvoie False\nlorsque code n'est pas le bon")
            self.assertFalse(res, msg=e)
            e=error("si deverrouiller() renvoie False,\nalors la serrure ne doit plus être deverrouillee")
            self.assertTrue(s.est_verrouillee, msg=e)
            res = s.deverouiller('1925')
            e = error("deverrouiller(code) doit renvoyer True\nlorsque le code est correct")
            self.assertTrue(res,msg=e)
            e=error("si deverrouiller() renvoie True,\nalors la serrure doit être deverrouillee")
            self.assertFalse(s.est_verrouillee, msg=e)
            
        except AttributeError:
            e = error("methode 'deverrouiller()' incorrecte ou inaccessible")
            self.fail(e)

    def test_changer_code(self):
        try:
            s = eleve.Serrure('1925')
            res = s.changer_code_secret('1900', '1984')
            e=error("'changer_code_secret()' doit renvoyer False\ncar l'ancien code est incorrect")
            self.assertFalse(res, msg=e)
            e=error("si 'changer_code_secret()' renvoie False,\nle code ne doit pas changer")
            self.assertEqual(s.code, '1925', msg=e)
            res = s.changer_code_secret('1925', '1984')
            e=error("'changer_code_secret()' doit renvoyer True\ncar l'ancien code est correct")
            self.assertTrue(res, msg=e)
            e=error("si 'changer_code_secret()' renvoie True,\nle code doit changer")
            self.assertEqual(s.code, '1984', msg=e)
        except AttributeError:
            e=error("methode 'changer_code_secret()' incorrecte ou inaccessible")
            self.fail(e)

if __name__=='__main__':
    unittest.main()