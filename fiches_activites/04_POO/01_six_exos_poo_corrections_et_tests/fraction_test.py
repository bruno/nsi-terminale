import unittest
import random
from math import gcd #avec codeboard changer par la bibliothèque 'fractions'

import fraction as eleve


def error(txt):
    tab = txt.split('\n')
    result = "\n/!\ \n"
    for l in tab:
        result += '/!\ ' + l + '\n'
    return result + '/!\\'


class Validation(unittest.TestCase):

    def exemple(self):
        try:
            return eleve.Fraction(2, 3)
        except AttributeError:
            e = error("impossible d'instancier `Fraction`\n'f = Fraction(2, 3)' renvoie une erreur")
            self.fail(e)
        except TypeError:
            e = error("impossible d'instancier `Fraction`\n'f = Fraction(2, 3)' doit posséder 2 paramètres")
            self.fail(e)



    def test_00_instanciation(self):
        self.exemple()


    def test_10_representation_methode(self):
        f = self.exemple()
        try:
            f.representation()
        except AttributeError:
            e = error("erreur de méthode\n'representation()' inaccessible")
            self.fail(e)
        except TypeError:
            e = error("erreur de méthode\n'representation()' n'admet pas de paramètre")
            self.fail(e)


    def test_11_representation_simple(self):
        f = self.exemple()
        res = f.representation()
        e = error("erreur de méthode\n'representation():' Fraction(2,3).representation() doit renvoyer '2/3'")
        self.assertEqual(res, '2/3', e)


    def test_12_representation_50_alea(self):
        for _ in range(50):
            num = random.randint(-20, 20)
            denom = 0 
            while denom == 0:
                denom = random.randint(-10,10)
            f = eleve.Fraction(num, denom)
            prop = f.representation()
            res = "{}/{}".format(num, denom)
            txt = "erreur de méthode\n'representation():' Fraction({},{}).representation() doit renvoyer '{}/{}'".format(num, denom, num, denom)
            e = error(txt)
            self.assertEqual(prop, res, e)


    def test_20_egale_methode(self):
        f = self.exemple()
        try:
            f.egale(eleve.Fraction(2,3))
        except AttributeError:
            e = error("erreur de méthode\n'egale()' inaccessible")
            self.fail(e)
        except TypeError:
            e = error("erreur de méthode\n'egale()' doit avoir 1 paramètre")
            self.fail(e)


    def test_21_egale_simple(self):
        n0 = 2
        d0 = 3
        f = eleve.Fraction(n0, d0)
        prop = f.egale( eleve.Fraction(n0, d0) )
        res = True
        txt = "erreur de méthode\n'egale():' Fraction({},{}).egale(Fraction({}, {})) doit renvoyer True".format(n0, d0, n0, d0)
        e = error(txt)
        self.assertEqual(prop, res, e)


    def test_22_egale_simple_2(self):
        n0 = 2
        d0 = 5
        f = eleve.Fraction(n0, d0)
        prop = f.egale( eleve.Fraction(n0, d0) )
        res = True
        txt = "erreur de méthode\n'egale():' Fraction({},{}).egale(Fraction({}, {})) doit renvoyer True".format(n0, d0, n0, d0)
        e = error(txt)
        self.assertEqual(prop, res, e)


    def test_23_egale_non_simplifiee_1(self):
        n0 = 2
        d0 = 3
        f = eleve.Fraction(n0, d0)
        n1 = 4
        d1 = 6
        prop = f.egale( eleve.Fraction(n1, d1) )
        res = True
        txt = "erreur de méthode\n'egale():' Fraction({},{}).egale(Fraction({}, {})) doit renvoyer True\nen effet : 2/3 == 4/6 ".format(n0, d0, n1, d1)
        e = error(txt)
        self.assertEqual(prop, res, e)


    def test_24_egale_non_simplifiee_2(self):
        n0 = 1
        d0 = 2
        f = eleve.Fraction(n0, d0)
        n1 = 10
        d1 = 20
        prop = f.egale( eleve.Fraction(n1, d1) )
        res = True
        txt = "erreur de méthode\n'egale():' Fraction({},{}).egale(Fraction({}, {})) doit renvoyer True\nen effet : 1/2 == 10/20 ".format(n0, d0, n1, d1)
        e = error(txt)
        self.assertEqual(prop, res, e)


    def test_25_egale_non_egale_1(self):
        n0 = 1
        d0 = 2
        f = eleve.Fraction(n0, d0)
        n1 = 2
        d1 = 3
        prop = f.egale( eleve.Fraction(n1, d1) )
        res = False
        txt = "erreur de méthode\n'egale():' Fraction({},{}).egale(Fraction({}, {})) doit renvoyer True\nen effet : 1/2 != 2/3 ".format(n0, d0, n1, d1)
        e = error(txt)
        self.assertEqual(prop, res, e)


    def test_30_simplifier_methode(self):
        f = self.exemple()
        try:
            f.simplifier()
        except AttributeError:
            e = error("erreur de méthode\n'simplifier()' inaccessible")
            self.fail(e)
        except TypeError:
            e = error("erreur de méthode\n'simplifier()' doit avoir aucun paramètre")
            self.fail(e)


    def test_31_simplifier_exemple_1(self):
        n0 = 2
        d0 = 3
        f = eleve.Fraction(n0, d0)
        f.simplifier()
        prop = str(f)
        res = '2/3'
        txt = "erreur de méthode\n'simplifier():' Fraction({},{}).simplifier() doit être la fraction 2/3".format(n0, d0)
        e = error(txt)
        self.assertEqual(prop, res, e)


    def test_32_simplifier_exemple_2(self):
        n0 = 4
        d0 = 6
        f = eleve.Fraction(n0, d0)
        f.simplifier()
        prop = str(f)
        res = '2/3'
        txt = "erreur de méthode\n'simplifier():' Fraction({},{}).simplifier() doit être la fraction 2/3".format(n0, d0)
        e = error(txt)
        self.assertEqual(prop, res, e)


    def test_33_simplifier_50alea(self):
        for _ in range(50):
            n0 = random.randint(1, 20)
            d0 = random.randint(1, 20)
            f = eleve.Fraction(n0, d0)
            f.simplifier()
            prop = str(f)
            ## prof
            pgcd = gcd(n0, d0)
            n1 = n0//pgcd
            d1 = d0//pgcd
            res = '{}/{}'.format(n1, d1)
            txt = "erreur de méthode\n'simplifier():' Fraction({},{}).simplifier() doit être la fraction {}/{}".format(n0, d0, n1, d1)
            e = error(txt)
            self.assertEqual(prop, res, e)


    def test_40_somme_methode(self):
        f = self.exemple()
        try:
            f.somme(eleve.Fraction(2,3))
        except AttributeError:
            e = error("erreur de méthode\n'somme()' inaccessible")
            self.fail(e)
        except TypeError:
            e = error("erreur de méthode\n'somme()' doit avoir 1 paramètre")
            self.fail(e)


    def test_41_somme_exemple_1(self):
        f = eleve.Fraction(2,3)
        g = f.somme(eleve.Fraction(4,6))
        g.simplifier
        prop = str(g)
        res = '4/3'
        txt = "erreur de méthode\n'somme():' Fraction({},{}).somme(Fraction({},{})) doit\nêtre la fraction (simplifiée) {}/{}".format(2, 3, 4, 6, 4, 3)
        e = error(txt)
        self.assertEqual(prop, res, e)


    def test_42_somme_exemple_2(self):
        f = eleve.Fraction(2,3)
        g = f.somme(eleve.Fraction(2,5))
        g.simplifier
        prop = str(g)
        res = '16/15'
        txt = "erreur de méthode\n'somme():' Fraction({},{}).somme(Fraction({},{})) doit\nêtre la fraction (irréductible) {}/{}".format(2, 3, 2, 5, 16, 15)
        e = error(txt)
        self.assertEqual(prop, res, e)


    def test_43_somme_50alea(self):
        for _ in range(50):
            n0 = random.randint(1,20)
            d0 = random.randint(1,20)
            f = eleve.Fraction(n0,d0)
            
            n1 = random.randint(1,20)
            d1 = random.randint(1,20)
            
            g = f.somme(eleve.Fraction(n1,d1))
            g.simplifier

            prop = str(g)
            
            n2 = n0*d1 + n1*d0
            d2 = d0 * d1
            pgcd = gcd(n2, d2)
            n2 //= pgcd
            d2 //= pgcd
            res = '{}/{}'.format(n2, d2)
            txt = "erreur de méthode\n'somme():' Fraction({},{}).somme(Fraction({},{})) doit\nêtre la fraction (simplifiée) {}/{}".format(n0, d0, n1, d1, n2, d2)
            e = error(txt)
            self.assertEqual(prop, res, e)


    def test_50_produit_methode(self):
        f = self.exemple()
        try:
            f.produit(eleve.Fraction(2,3))
        except AttributeError:
            e = error("erreur de méthode\n'produit()' inaccessible")
            self.fail(e)
        except TypeError:
            e = error("erreur de méthode\n'produit()' doit avoir 1 paramètre")
            self.fail(e)


    def test_51_produit_exemple_1(self):
        f = eleve.Fraction(2,3)
        g = f.produit(eleve.Fraction(4,6))
        g.simplifier
        prop = str(g)
        res = '4/9'
        txt = "erreur de méthode\n'produit():' Fraction({},{}).produit(Fraction({},{})) doit\nêtre la fraction (simplifiée) {}/{}".format(2, 3, 4, 6, 4, 9)
        e = error(txt)
        self.assertEqual(prop, res, e)


    def test_52_produit_50alea(self):
        for _ in range(50):
            n0 = random.randint(1,20)
            d0 = random.randint(1,20)
            f = eleve.Fraction(n0,d0)
            
            n1 = random.randint(1,20)
            d1 = random.randint(1,20)
            
            g = f.produit(eleve.Fraction(n1,d1))
            g.simplifier

            prop = str(g)
            
            n2 = n0 * n1
            d2 = d0 * d1
            pgcd = gcd(n2, d2)
            n2 //= pgcd
            d2 //= pgcd
            res = '{}/{}'.format(n2, d2)
            txt = "erreur de méthode\n'produit():' Fraction({},{}).produit(Fraction({},{})) doit\nêtre la fraction (simplifiée) {}/{}".format(n0, d0, n1, d1, n2, d2)
            e = error(txt)
            self.assertEqual(prop, res, e)




if __name__=='__main__':
    unittest.main()
