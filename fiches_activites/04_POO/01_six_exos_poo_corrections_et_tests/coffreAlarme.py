"""
--- 
## Exercice 5 Mini projet 
Écrire la définition d'une classe CoffreAlarme. C'est un coffre qui est pourvu d'une serrure et d'une alarme.
Pour ouvrir le coffre il faut que la serrure soit déverrouillée.
Si l'on tente d'ouvrir le coffre alors que la serrure est verrouillee alors l'alarme se déclenche. 

Vous utiliserez les classes des exercices précédents en les adaptant.
"""

########## Réposnes #######
from doctest import testmod
from serrure import Serrure
from coffre import Coffre
from alarme import Alarme


class CoffreAlarme:
    """ Coffre pourvu d'une serrure et d'une alarme.

    Parameters
    ----------
    code : (str)
        code de la serrure
    son : (str)
        sonnerie émise par l'alarme
    montant : (float)
        somme initiale dans le coffre
    """

    def __init__(self, code: str, son: str, montant: float):
        self.serrure: Serrure = Serrure(code)
        self.alarme: Alarme = Alarme(son)
        self.coffre: Coffre = Coffre(montant)


    def ouvrir(self):
        """Ouvre le coffre si la serrure n'est pas verrouillée.
        Déclenche l'alarme sinon.

        Examples
        --------
            >>> c = CoffreAlarme('1234', 'pinpon!', 1000)
            >>> c.ouvrir()          #déclenche l'alarme
            'pinpon!'
            >>> c.deverrouiller('1234')
            'serrure déverrouillée'
            >>> c.ouvrir()          #ne déclenche PAS l'alarme
            'coffre ouvert'
        """
        s = self.serrure
        a = self.alarme
        if s.est_verrouillee:
            return a.declencher()

        self.coffre.ouvrir()
        return 'coffre ouvert'


    def fermer(self) -> None:
        """ Ferme le coffre

        Examples
        --------
            >>> c = CoffreAlarme('1234', 'pinpon!', 1000)
            >>> c.deverrouiller('1234')
            'serrure déverrouillée'
            >>> c.ouvrir()
            'coffre ouvert'
            >>> c.consulter()
            1000
            >>> c.fermer()      # quand le coffre est fermé...
            'coffre fermé'
            >>> c.consulter()   # ...on ne peut pas consulter le solde
            -1
        """
        self.coffre.fermer()
        return 'coffre fermé'


    def consulter(self) -> float:
        """Donne le contenu du coffre s'il est ouvert et -1 sinon.

        Returns
        -------
        float
            Montant du coffre (ou -1 s'il est fermé)

        Examples
        --------
            >>> c = CoffreAlarme('1234', 'pinpon!', 1000)
            >>> c.deverrouiller('1234')
            'serrure déverrouillée'
            >>> c.ouvrir()
            'coffre ouvert'
            >>> c.consulter()
            1000
            >>> c.fermer()      # quand le coffre est fermé...
            'coffre fermé'
            >>> c.consulter()   # ...on ne peut pas consulter le solde
            -1
        """
        return self.coffre.consulter()


    def deposer(self, montant: float) -> float:
        """Ajoute le montant au contenu si le coffre est ouvert.
        Renvoie la valeur du montant déposé (ou 0 si le coffre est fermé).

        Parameters
        ----------
        montant : float
            somme à déposer dans le coffre

        Returns
        -------
        float
            montant réellement déposé si le coffre est ouvert (et 0 sinon)

        Examples
        --------
            >>> c = CoffreAlarme('1234', 'pinpon!', 1000)
            >>> c.deverrouiller('1234')
            'serrure déverrouillée'
            >>> c.deposer(500)      # coffre fermé => on ne peut PAS déposer
            0
            >>> c.ouvrir()
            'coffre ouvert'
            >>> c.deposer(500)      # coffre ouvert => on peut déposer
            500
            >>> c.consulter()       # ... et le montant est mis à jour
            1500
        """
        return self.coffre.deposer(montant)


    def retirer(self, montant: float) -> float:
        """ Retire le montant du coffre s'il est ouvert .

        Parameters
        ----------
        montant : (float)
            Montant à retirer dans la limite du contenu disponible
        
        Returns
        -------
        (float)
            Montant demandé s'il y a assez d'argent
            S'il n'y a pas assez d'argent, renvoie le contenu qu'il restait
            (0 si le coffre n'est pas ouvert)

        Examples
        --------
            >>> c = CoffreAlarme('1234', 'pinpon!', 1000)
            >>> c.deverrouiller('1234')
            'serrure déverrouillée'
            >>> c.retirer(250)
            0
            >>> c.ouvrir()
            'coffre ouvert'
            >>> c.retirer(250)
            250
            >>> c.consulter()
            750
            >>> c.retirer(5000)
            750
            >>> c.consulter()
            0
        """
        return self.coffre.retirer(montant)


    def armer(self):
        """ Arme l'alarme pour qu'elle puisse sonner

        Examples
        --------
            >>> c = CoffreAlarme('1234', 'pinpon!', 1000)
            >>> c.armer()
            'alarme armée'
            >>> c.alarme.declencher()
            'pinpon!'
        """
        self.alarme.armer()
        return 'alarme armée'


    def desarmer(self) -> str:
        """ Désarme l'alarme qui ne sonnera donc pas.

        Examples
        --------
            >>> c = CoffreAlarme('1234', 'pinpon!', 1000)
            >>> c.desarmer()
            'pinpon!'
            >>> c.deverrouiller('1234')
            'serrure déverrouillée'
            >>> c.desarmer()
            'alarme désarmée'
            >>> c.alarme.declencher()          #test de sonnerie
            'désarmée'
        """
        if self.serrure.est_verrouillee:
            return self.alarme.declencher()

        self.alarme.desarmer()
        return 'alarme désarmée'


    def verrouiller(self) -> str:
        self.serrure.verrouiller()
        return 'serrure verrouillée'


    def deverrouiller(self, test_code:str) -> str:
        """ Tente de déverrouiller la serrure avec le `test_code` proposé.

        Parameters
        ----------
        test_code : (str)
            code à tester pour déverrouiller la serrure.
        
        Examples
        --------
            >>> c = CoffreAlarme('1234', 'pinpon!', 1000)
            >>> c.deverrouiller('1234')
            'serrure déverrouillée'
            >>> c.deverrouiller('1965')
            'pinpon!'
            >>> c.deverrouiller('1234')
            'serrure déverrouillée'
        """
        etat:bool = self.serrure.deverrouiller(test_code)
        if not etat:
            return self.alarme.declencher()
        return 'serrure déverrouillée'


testmod()
