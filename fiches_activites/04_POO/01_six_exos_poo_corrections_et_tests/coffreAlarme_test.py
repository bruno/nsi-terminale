import unittest


import coffreAlarme as eleve


def error(txt):
    tab = txt.split('\n')
    result = "\n/!\ \n"
    for l in tab:
        result += '/!\ ' + l + '\n'
    return result + '/!\\'


class Validation(unittest.TestCase):
    def exemple(self):
        try:
            c = eleve.CoffreAlarme('1234', 'pinpon!', 1000)
            return c
        except AttributeError:
            e = error("impossible d'instancier `CoffreAlarme`\n'CoffreAlarme('1234', 'pinpon!', 1000)' genere une erreur...")
            self.fail(e)
        except TypeError:
            e = error("impossible d'instancier `CoffreAlarme`\n'CoffreAlarme('1234', 'pinpon!', 1000)' genere une erreur...")
            self.fail(e)


    def test_10_instance(self):
        c = self.exemple()


    def test_11_serrure_attribut(self):
        c = self.exemple()
        try:
            res = c.serrure
        except AttributeError:
            e = error("erreur d'attribut\n'serrure' inaccessible")
            self.fail(e)


    def test_12_alarme_attribut(self):
        c = self.exemple()
        try:
            res = c.alarme
        except AttributeError:
            e = error("erreur d'attribut\n'alarme' inaccessible")
            self.fail(e)


    def test_13_coffre_attribut(self):
        c = self.exemple()
        try:
            res = c.coffre
        except AttributeError:
            e = error("erreur d'attribut\n'coffre' inaccessible")
            self.fail(e)


    def test_20_ouvrir_methode(self):
        c = self.exemple()
        try:
            c.ouvrir()
        except AttributeError:
            e = error("erreur de methode\n'ouvrir()' inaccessible")
            self.fail(e)


    def test_21_ouvrir_verrouillee(self):
        c = self.exemple()
        res = c.ouvrir()
        e = error("erreur methode\n'ouvrir()' doit déclencher l'alarme lorsque\nle coffreAlarme est verrouille")
        self.assertEqual(res, 'pinpon!', e)


    def test_22_ouvrir_deverrouillee(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        res = c.ouvrir()
        e = error("erreur methode\n'ouvrir()' ne doit pas déclencher l'alarme lorsque\nle coffreAlarme est deverrouille")
        self.assertEqual(res, 'coffre ouvert', e)
    

    def test_25_fermer_methode(self):
        c = self.exemple()
        c.ouvrir()
        try:
            c.fermer()
        except AttributeError:
            e = error("erreur de methode\n'fermer()' inaccessible")
            self.fail(e)
    

    def test_26_fermer(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        c.coffre.est_ouvert = True
        c.fermer()
        e = error("erreur methode\n'fermer()' doit avoir une influence sur l'attribut\n'est_ouvert' du 'coffre' du coffreAlarme")
        self.assertFalse(c.coffre.est_ouvert, e)
    

    def test_30_consulter_methode(self):
        c = self.exemple()
        try:
            res = c.consulter()
        except AttributeError:
            e = error("erreur de methode\n'consulter()' inaccessible")
            self.fail(e)
    

    def test_31_consulter_coffre_ouvert(self):
        c = self.exemple()
        c.coffre.est_ouvert = True
        res = c.consulter()
        e = error("erreur de methode\n'consulter()' doit renvoyer le montant quand\nle coffre est ouvert")
        self.assertEqual(res, 1000, e)
    

    def test_32_consulter_coffre_ferme(self):
        c = self.exemple()
        res = c.consulter()
        e = error("erreur de methode\n'consulter()' doit renvoyer '-1' quand\nle coffre est ferme")
        self.assertEqual(res, -1, e)


    def test_40_deposer_methode(self):
        c = self.exemple()
        try:
            res = c.deposer(500)
        except AttributeError:
            e = error("erreur de methode\n'deposer()' inaccessible")
            self.fail(e)


    def test_41_deposer_coffre_ferme(self):
        c = self.exemple()
        res = c.deposer(500)
        e = error("erreur de methode\n'deposer()' doit renvoyer '0' lorsque le coffre est ferme")
        self.assertEqual(res, 0, e)


    def test_42_deposer_coffre_ferme_montant(self):
        c = self.exemple()
        c.deposer(500)
        res = c.coffre.contenu
        e = error("erreur de methode\n'deposer()' ne doit pas modifier le contenu du montant\ndu coffre lorsque de l'argent est depose dans\nune coffre ferme")
        self.assertEqual(res, 1000, e)


    def test_43_deposer_coffre_ouvert(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        c.ouvrir()
        res = c.deposer(250)
        e = error("erreur de methode\n'deposer()' doit renvoyer la somme déposée lorsque le coffre est ouvert")
        self.assertEqual(res, 250, e)


    def test_48_deposer_coffre_ouvert(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        c.ouvrir()
        c.deposer(250)
        res = c.consulter()
        e = error("erreur de methode\n'deposer()' doit modifier le contenu du montant\ndu coffre lorsque de l'argent est depose dans\nune coffre ouvert")
        self.assertEqual(res, 1250, e)
    

    def test_50_retirer_methode(self):
        c = self.exemple()
        try:
            res = c.retirer(250)
        except AttributeError:
            e = error("erreur de methode\n'retirer()' inaccessible")
            self.fail(e)
    

    def test_51_retirer_ferme(self):
        c = self.exemple()
        res = c.retirer(250)
        e = error("erreur de methode\n'retirer()' doit renvoyer '0' lorsque le coffre est ferme")
        self.assertEqual(res, 0, e)


    def test_52_retirer_coffre_ferme_montant(self):
        c = self.exemple()
        c.retirer(250)
        c.serrure.est_verrouillee = False
        c.ouvrir()
        res = c.consulter()
        e = error("erreur de methode\n'retirer()' ne doit pas modifier le contenu du montant\ndu coffre lorsque de l'argent est depose dans\nune coffre ferme")
        self.assertEqual(res, 1000, e)
    

    def test_53_retirer_ouvert(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        c.ouvrir()
        res = c.retirer(250)
        e = error("erreur de methode\n'retirer()' doit renvoyer le montant réellement\nobtenu lorsque le coffre est ouvert")
        self.assertEqual(res, 250, e)
    

    def test_54_retirer_ouvert_montant(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        c.ouvrir()
        c.retirer(250)
        res = c.consulter()
        e = error("erreur de methode\n'retirer()' doit modifier le contenu du coffre\napres un retrait dans un coffre ouvert")
        self.assertEqual(res, 750, e)
    

    def test_55_retirer_ouvert_trop_eleve(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        c.ouvrir()
        res = c.retirer(5000)
        e = error("erreur de methode\n'retirer()' doit renvoyer le montant réellement\ndisponible lorsque le coffre est ouvert et que\nla somme demandee est trop elevee")
        self.assertEqual(res, 1000, e)
    

    def test_56_retirer_ouvert_montant_trop_eleve(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        c.ouvrir()
        c.retirer(5000)
        res = c.consulter()
        e = error("erreur de methode\n'retirer()' doit modifier le contenu du coffre\napres un retrait dans un coffre ouvert\nSi le montant est trop eleve, il reste\nalors '0' dans le coffre")
        self.assertEqual(res, 0, e)


    def test_60_armer_methode(self):
        c = self.exemple()
        try:
            res = c.armer()
        except AttributeError:
            e = error("erreur de methode\n'armer()' inaccessible")
            self.fail(e)


    def test_61_armer_coffre_init(self):
        c = self.exemple()
        res = c.armer()
        e = error("erreur de methode\n'armer()' doit renvoyer le texte 'alarme armée'")
        self.assertEqual(res, 'alarme armée', e)


    def test_62_armer_coffre_modif_attribut(self):
        c = self.exemple()
        c.alarme.est_armee = False
        c.armer()
        res = c.alarme.est_armee
        e = error("erreur de methode\n'armer()' doit modifier l'attribut 'est_armee'\nde 'alarme' du coffreAlarme")
        self.assertTrue(res, e)


    def test_70_desarmer_methode(self):
        c = self.exemple()
        try:
            res = c.desarmer()
        except AttributeError:
            e = error("erreur de methode\n'desarmer()' inaccessible")
            self.fail(e)

    def test_71_desarmer_verrouille(self):
        c = self.exemple()
        res = c.desarmer()
        e = error("erreur methode\n'desarmer()' doit renvoyer la sonnerie de l'alarme\nlorsque 'serrure' est verrouillee")
        self.assertEqual(res, 'pinpon!', e)


    def test_72_desarmer_deverrouille(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        res = c.desarmer()
        e = error("erreur methode\n'desarmer()' doit renvoyer le texte 'alarme désarmée'\nlorsque 'serrure' est deverrouillee")
        self.assertEqual(res, 'alarme désarmée', e)


    def test_73_desarmer_pas_d_alarme(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        c.desarmer()
        res = c.alarme.declencher()
        e = error("erreur methode\n'desarmer()' doit desarmer l'alarme qui\nn'affiche plus la sonnerie mais le texte 'désarmée'\nlorsque l'alarme est declenchée")
        self.assertEqual(res, 'désarmée', e)


    def test_80_verrouiller_methode(self):
        c = self.exemple()
        try:
            res = c.verrouiller()
        except AttributeError:
            e = error("erreur de methode\n'deverrouiller()' inaccessible")
            self.fail(e)


    def test_81_verrouiller_renvoie(self):
        c = self.exemple()
        res = c.verrouiller()
        e = error("erreur methode\n'verrouiller()' renvoyer le texte 'serrure verrouillée'")
        self.assertEqual(res, 'serrure verrouillée', e)


    def test_82_verrouiller_modifie_serrure(self):
        c = self.exemple()
        c.serrure.est_verrouillee = False
        c.verrouiller()
        res = c.serrure.est_verrouillee
        e = error("erreur methode\n'verrouiller()' doit modifier l'attribut\n'est_verrouillee' de la 'serrure' du coffreAlarme")
        self.assertTrue(res, e)


    def test_90_deverrouiller_methode(self):
        c = self.exemple()
        try:
            res = c.deverrouiller('1234')
        except AttributeError:
            e = error("erreur de methode\n'deverrouiller()' inaccessible")
            self.fail(e)
        except TypeError:
            e = error("erreur de methode\n'deverrouiller()' nécessite 1 argument")
            self.fail(e)


    def test_91_deverrouiller_bon_code(self):
        c = self.exemple()
        res = c.deverrouiller('1234')
        e = error("erreur methode\n'deverrouiller()' renvoyer le texte 'serrure déverrouillée'")
        self.assertEqual(res, 'serrure déverrouillée', e)


    def test_92_deverrouiller_bon_code_modifie_serrure(self):
        c = self.exemple()
        c.deverrouiller('1234')
        res = c.serrure.est_verrouillee
        e = error("erreur methode\n'deverrouiller()' avec le bon code doit modifier\nl'attribut 'est_verrouillee' de la 'serrure' du coffreAlarme")
        self.assertFalse(res, e)


    def test_93_deverrouiller_mauvais_code(self):
        c = self.exemple()
        res = c.deverrouiller('4321')
        e = error("erreur methode\n'deverrouiller()' avec le mauvais code declenche\nl'alarme")
        self.assertEqual(res, 'pinpon!', e)


    def test_93_deverrouiller_mauvais_code_aucun_changement(self):
        c = self.exemple()
        c.serrure.est_verrouillee = True
        c.deverrouiller('4321')
        res = c.serrure.est_verrouillee
        e = error("erreur methode\n'deverrouiller()' avec le mauvais code ne doit pas\nmodifier l'attribut 'est_verrouillee' de la\n'serrure' du coffreAlarme")
        self.assertTrue(res, e)



if __name__ == '__main__':
    unittest.main()
