import unittest

import coffre as eleve


def error(txt):
    tab = txt.split('\n')
    result = "\n/!\ \n"
    for l in tab:
        result += '/!\ ' + l + '\n'
    return result + '/!\\'


class Validation(unittest.TestCase):
    def exemple(self):
        try:
            c = eleve.Coffre(1000)
            return c
        except AttributeError:
            e = error("impossible d'instancier `Coffre`\n'Coffre(1000)' genere une erreur...")
            self.fail(e)
        except TypeError:
            e = error("impossible d'instancier `Coffre` avec un parametre\n'Coffre(1000)' genere une erreur...")
            self.fail(e)


    def test_10_instance(self):
        c = self.exemple()
    

    def test_11_est_ouvert_attribut(self):
        c = self.exemple()
        try:
            res = c.est_ouvert
        except AttributeError:
            e = error("erreur d'attribut\n'est_ouvert' inaccessible")
            self.fail(e)
    

    def test_12_est_ouvert(self):
        c = self.exemple()
        e = error("erreur d'attribut\ninitialement, le coffre doit etre ferme\n'self.est_ouvert' doit etre 'False'")
        self.assertFalse(c.est_ouvert, e)

    
    def test_13_contenu_attribut(self):
        c = self.exemple()
        try:
            res = c.contenu
        except AttributeError:
            e = error("erreur d'attribut\n'contenu' inaccessible")
            self.fail(e)

    
    def test_14_contenu(self):
        c = self.exemple()
        e = error("erreur d'attribut\ninitialement, le coffre doit contenir la somme passee en parametre\n'self.contenu' doit etre '1000'")
        self.assertEqual(c.contenu, 1000, e)


    def test_20_ouvrir_methode(self):
        c = self.exemple()
        try:
            c.ouvrir()
        except AttributeError:
            e = error("erreur de methode\n'ouvrir()' inaccessible")
            self.fail(e)


    def test_21_ouvrir(self):
        c = self.exemple()
        c.ouvrir()
        e = error("erreur methode\n'self.ouvrir()' doit avoir une influence sur l'attribut 'est_ouvert'")
        self.assertTrue(c.est_ouvert, e)
    

    def test_25_fermer_methode(self):
        c = self.exemple()
        c.ouvrir()
        try:
            c.fermer()
        except AttributeError:
            e = error("erreur de methode\n'fermer()' inaccessible")
            self.fail(e)
    

    def test_26_fermer(self):
        c = self.exemple()
        c.ouvrir()
        c.fermer()
        e = error("erreur methode\n'self.fermer()' doit avoir une influence sur l'attribut 'est_ouvert'")
        self.assertFalse(c.est_ouvert, e)
    

    def test_30_consulter_methode(self):
        c = self.exemple()
        try:
            res = c.consulter()
        except AttributeError:
            e = error("erreur de methode\n'consulter()' inaccessible")
            self.fail(e)
    

    def test_35_consulter_apres_init(self):
        c = self.exemple()
        res = c.consulter()
        e = error("erreur de methode\napres initialisation, la methode\n'consulter()' doit renvoyer '-1' car le coffre est ferme")
        self.assertEqual(res, -1, e)
    

    def test_36_consulter_apres_ouvrir(self):
        c = self.exemple()
        c.ouvrir()
        res = c.consulter()
        e = error("erreur de methode\n'consulter()' doit le montant quand le coffre est ouvert")
        self.assertEqual(res, 1000, e)


    def test_40_deposer_methode(self):
        c = self.exemple()
        try:
            res = c.deposer(500)
        except AttributeError:
            e = error("erreur de methode\n'deposer()' inaccessible")
            self.fail(e)


    def test_45_deposer_coffre_ferme(self):
        c = self.exemple()
        res = c.deposer(500)
        e = error("erreur de methode\n'deposer()' doit renvoyer '0' lorsque le coffre est ferme")
        self.assertEqual(res, 0, e)


    def test_46_deposer_coffre_ferme_montant(self):
        c = self.exemple()
        c.deposer(500)
        c.ouvrir()
        res = c.consulter()
        e = error("erreur de methode\n'deposer()' ne doit pas modifier le contenu du montant\ndu coffre lorsque de l'argent est depose dans\nune coffre ferme")
        self.assertEqual(res, 1000, e)


    def test_47_deposer_coffre_ouvert(self):
        c = self.exemple()
        c.ouvrir()
        res = c.deposer(500)
        e = error("erreur de methode\n'deposer()' doit renvoyer la somme déposée lorsque le coffre est ouvert")
        self.assertEqual(res, 500, e)


    def test_48_deposer_coffre_ouvert(self):
        c = self.exemple()
        c.ouvrir()
        c.deposer(500)
        res = c.consulter()
        e = error("erreur de methode\n'deposer()' doit modifier le contenu du montant\ndu coffre lorsque de l'argent est depose dans\nune coffre ouvert")
        self.assertEqual(res, 1500, e)
    

    def test_50_retirer_methode(self):
        c = self.exemple()
        try:
            res = c.retirer(250)
        except AttributeError:
            e = error("erreur de methode\n'retirer()' inaccessible")
            self.fail(e)
    

    def test_55_retirer_ferme(self):
        c = self.exemple()
        res = c.retirer(250)
        e = error("erreur de methode\n'retirer()' doit renvoyer '0' lorsque le coffre est ferme")
        self.assertEqual(res, 0, e)


    def test_56_retirer_coffre_ferme_montant(self):
        c = self.exemple()
        c.retirer(250)
        c.ouvrir()
        res = c.consulter()
        e = error("erreur de methode\n'retirer()' ne doit pas modifier le contenu du montant\ndu coffre lorsque de l'argent est depose dans\nune coffre ferme")
        self.assertEqual(res, 1000, e)
    

    def test_57_retirer_ouvert(self):
        c = self.exemple()
        c.ouvrir()
        res = c.retirer(250)
        e = error("erreur de methode\n'retirer()' doit renvoyer le montant réellement\ndisponible lorsque le coffre est ouvert")
        self.assertEqual(res, 250, e)
    

    def test_58_retirer_ouvert_montant(self):
        c = self.exemple()
        c.ouvrir()
        c.retirer(250)
        res = c.consulter()
        e = error("erreur de methode\n'retirer()' doit modifier le contenu du coffre\napres un retrait dans un coffre ouvert")
        self.assertEqual(res, 750, e)
    

    def test_58_retirer_ouvert_trop_eleve(self):
        c = self.exemple()
        c.ouvrir()
        res = c.retirer(5000)
        # res = c.consulter()
        e = error("erreur de methode\n'retirer()' doit renvoyer le montant réellement\ndisponible lorsque le coffre est ouvert et que\nla somme demandee est trop elevee")
        self.assertEqual(res, 1000, e)
    

    def test_59_retirer_ouvert_montant_trop_eleve(self):
        c = self.exemple()
        c.ouvrir()
        c.retirer(5000)
        res = c.consulter()
        e = error("erreur de methode\n'retirer()' doit modifier le contenu du coffre\napres un retrait dans un coffre ouvert\nSi le montant est trop eleve, il reste\nalors '0' dans le coffre")
        self.assertEqual(res, 0, e)



        

if __name__ == '__main__':
    unittest.main()