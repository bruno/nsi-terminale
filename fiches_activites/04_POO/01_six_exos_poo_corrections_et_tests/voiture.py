""" 
Question 1 
----------
attributs : annee ; couleur ; vitesse ; age
méthodes  : petite_annonce()
"""


"""
Question 2
----------
cf code pour voir types et documentation
"""


class Voiture :
    """ Classe de Voiture """
    def __init__(self, annee: int, coul: str, vmax: int):
        self.annee: int = annee    
        self.couleur: str = coul
        self.vitesse_max: int = vmax
        self.age: int = 2023 - self.annee

    def petite_annonce(self) -> str :
        """Texte correspondant à l'annonce à publier.

        Returns
        -------
        str
            Texte de l'annonce
        """
        return "À vendre voiture "+ str(self.couleur)+ " de " + str(self.annee) +\
             ", vitesse maximale "+ str(self.vitesse_max)+ " km/h."


""" 
Question 3
----------
"""

ma_voiture = Voiture(1957, "grise", 110)
mon_annonce = ma_voiture.petite_annonce()

print(mon_annonce)
