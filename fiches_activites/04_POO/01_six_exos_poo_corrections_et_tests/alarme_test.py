import unittest

import alarme_eleve as eleve
import alarme as prof


def error(txt):
    tab = txt.split('\n')
    result = "\n/!\ \n"
    for l in tab:
        result += '/!\ ' + l + '\n'
    return result + '/!\\'


class Validation(unittest.TestCase):
    def test_instance(self):
        try:
            a = eleve.Alarme("pinponpin")
        except AttributeError:
            e=error("creation d'une instance de Alarm impossible")
            self.fail(e)
    

    def test_init(self):
        try:
            a = eleve.Alarme("pinponpin")
            e=error("l'attribut 'sonnerie' est incorrect\n\t{}\n\t{}".format(a.sonnerie, "pinponpin"))
            self.assertEqual(a.sonnerie, "pinponpin", msg=e)
            e=error("initialement l'alarme doit être armee")
            self.assertTrue(a.est_armee, e)
        except AttributeError:
            e=error("attributs et/ou methode absents")
            self.fail(e)
    
    def test_armer(self):
        try:
            a = eleve.Alarme("pinponpin")
            a.est_armee = False
            a.armer()
            e=error("'armer()' doit armer l'alarme")
            self.assertTrue(a.est_armee,e)
        except AttributeError:
            e=error("attributs et/ou methode absents")
            self.fail(e)
    
    def test_desarmer(self):
        try:
            a = eleve.Alarme("pinponpin")
            a.desarmer()
            e=error("'desarmer()' doit desarmer l'alarme\nl'attribut 'est_armee' doit être False")
            self.assertFalse(a.est_armee,e)
        except AttributeError:
            e=error("attributs et/ou methode absents")
            self.fail(e)
        
    def test_declencher(self):
        try:
            a = eleve.Alarme("pinponpin")
            res = a.declencher()
            correct="pinponpin"
            e=error("'declencher()' doit afficher le son de l'alarme\nlorsque l'alarme est armee :\n\t{}\n\t{}".format(res, correct))
            self.assertEqual(res, correct, e)
            a.desarmer()
            res = a.declencher()
            correct='désarmée'
            e=error("'declencher()' doit afficher 'désarmée'\nlorsque l'alarme est desarmee :\n\t{}\n\t{}".format(res, correct))
            self.assertEqual(res, correct, e)
        except AttributeError:
            e=error("attributs et/ou methode absents")
            self.fail(e)



if __name__=='__main__':
    unittest.main()