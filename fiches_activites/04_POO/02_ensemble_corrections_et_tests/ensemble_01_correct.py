'''
structure d'ensembles
implémentatio avec la POO
'''


class Ensemble:
    def __init__(self):
        self.data = []


    def to_str(self) -> str:
        texte = "E{"
        for n in self.data:
            texte += str(n) + " ; "
        if len(texte) > 2:
            texte = texte[:-3]  # suppression des 3 derniers caractères
        return texte + "}"


    def contient(self, x: int) -> bool:
        data = self.data
        for i in range(len(data)):
            if data[i] == x:
                return True
        return False
    

    def ajoute(self, x: int) -> None:
        if not self.contient(x):
            self.data.append(x)
        
    
    def supprime(self, x: int) -> None:
        if not self.contient(x):
            raise IndexError
        self.data.remove(x)


    def taille(self) -> int:
        return len(self.data)



