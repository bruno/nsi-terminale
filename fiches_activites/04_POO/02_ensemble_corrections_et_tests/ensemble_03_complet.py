'''
structure d'ensembles
implémentatio avec la POO
'''

from doctest import testmod


class Ensemble:
    """ Classe d'ensemble de nombres entiers
    Sans doublon et initialement vide.
    """
    def __init__(self):
        self.data = []


    def to_str(self) -> str:
        """Renvoie l'ensemble sous la forme d'une chaîne de caractère.

        Returns
        -------
        (str) chaine de la forme 'E{1 ; 4 ; 3 ; 2}' ou 'E{}'

        Examples
        --------
            >>> e = Ensemble()
            >>> e.to_str()
            'E{}'
        """
        texte = "E{"
        for n in self.data:
            texte += str(n) + " ; "
        if len(texte) > 2:
            texte = texte[:-3]  # suppression des 3 derniers caractères
        return texte + "}"


    def contient(self, x: int) -> bool:
        """Indique si x appartient ou pas à l'ensemble e.

        Parameters
        ----------
        x : (int)
            Nombre entier dont on souhaite vérifier l'appartenance à l'ensemble
        Returns
        -------
        (bool) `True` ssi x appartient à e

        Example
        -------
            >>> e = Ensemble()
            >>> e.contient(0)
            False
            >>> e.contient(1)
            False
            >>> e.ajoute(0)
            >>> e.contient(0)
            True
            >>> e.contient(1)
            False
        """
        data = self.data
        for i in range(len(data)):
            if data[i] == x:
                return True
        return False


    def ajoute(self, x: int) -> None:
        """ Modifie l'ensemble e en y ajoutant l'élément x (sans doublon)

        Parameters
        ----------
        e : (Ensemble)
        x : (int)

        Example
        -------
            >>> e = Ensemble()
            >>> e.ajoute(0)
            >>> e.to_str()
            'E{0}'
            >>> e.ajoute(0)
            >>> e.to_str()
            'E{0}'
            >>> e.ajoute(4)
            >>> e.to_str()
            'E{0 ; 4}'
            >>> e.ajoute(3)
            >>> e.ajoute(3)
            >>> e.ajoute(3)
            >>> e.ajoute(3)
            >>> e.to_str()
            'E{0 ; 4 ; 3}'
        """
        if not self.contient(x):
            self.data.append(x)
        
    
    def supprime(self, x: int) -> None:
        """ Modifie l'ensemble en supprimant l'élément x présent.
        Erreur si x n'est pas présent

        Parameters
        ----------
        x : (int)

        Example
        -------
            >>> e = Ensemble()
            >>> e.ajoute(0)
            >>> e.ajoute(4)
            >>> e.supprime(0)
            >>> e.contient(0)
            False
            >>> e.contient(4)
            True
            >>> e = Ensemble()
            >>> e.supprime(0)
            Traceback (most recent call last):
            ...
            IndexError
        """
        if not self.contient(x):
            raise IndexError
        
        self.data.remove(x)


    def taille(self) -> int:
        """Renvoie le nombre d'élément de l'ensemble.

        Returns
        -------
        (int) 0 si l'ensemble est vide et le nombre d'élément 
                de e (sans doublon) sinon
        
        Exemple
        -------
            >>> e = Ensemble()
            >>> e.taille()
            0
            >>> e.ajoute(0)
            >>> e.taille()
            1
        """
        return len(self.data)


testmod()
