'''
structure d'ensembles
implémentatio avec la POO
'''

from doctest import testmod


class Ensemble:
    def __init__(self):
        self.data = []


    def to_str(self) -> str:
        """
        >>> e = Ensemble()
        >>> e.to_str()
        'E{}'
        """
        texte = "E{"
        for n in self.data:
            texte += str(n) + " ; "
        if len(texte) > 2:
            texte = texte[:-3]  # suppression des 3 derniers caractères
        return texte + "}"


    def contient(self, x: int) -> bool:
        """
        >>> e = Ensemble()
        >>> e.contient(0)
        False
        >>> e.contient(1)
        False
        >>> e.ajoute(0)
        >>> e.contient(0)
        True
        >>> e.contient(1)
        False
        """
        data = self.data
        for i in range(len(data)):
            if data[i] == x:
                return True
        return False
    

    def ajoute(self, x: int) -> None:
        """
        >>> e = Ensemble()
        >>> e.ajoute(0)
        >>> e.to_str()
        'E{0}'
        >>> e.ajoute(0)
        >>> e.to_str()
        'E{0}'
        >>> e.ajoute(4)
        >>> e.to_str()
        'E{0 ; 4}'
        >>> e.ajoute(3)
        >>> e.ajoute(3)
        >>> e.ajoute(3)
        >>> e.ajoute(3)
        >>> e.to_str()
        'E{0 ; 4 ; 3}'
        """
        if not self.contient(x):
            self.data.append(x)
        
    
    def supprime(self, x: int) -> None:
        """
        >>> e = Ensemble()
        >>> e.ajoute(0)
        >>> e.ajoute(4)
        >>> e.supprime(0)
        >>> e.contient(0)
        False
        >>> e.contient(4)
        True
        >>> e = Ensemble()
        >>> e.supprime(0)
        Traceback (most recent call last):
        ...
        IndexError
        """
        if not self.contient(x):
            raise IndexError
        
        self.data.remove(x)


    def taille(self) -> int:
        """
        >>> e = Ensemble()
        >>> e.taille()
        0
        >>> e.ajoute(0)
        >>> e.taille()
        1
        """
        return len(self.data)






testmod()
