from tutor import tutor

class Noeud :
    def __init__(self, valeur : int, gauche = None, droit = None) :
        self.valeur = valeur
        self.gauche = gauche
        self.droit = droit

    def __repr__(self) :
        return str(self.valeur)

    def __eq__(self, arbre) :
        print(self, self.gauche, self.droit, arbre.gauche, arbre.droit)
        return arbre is not None and self.valeur == arbre.valeur \
            and self.gauche==arbre.gauche \
            and self.droit==arbre.droit

    def creer_enfant_gauche(self, val) :
        self.gauche = Noeud(val)

    def creer_enfant_droit(self, val) :
        self.droit = Noeud(val)

def parfait(h) :
    """créer un arbre binaire parfait de hauteur h"""
    if h == 0 :
        return None
    return Noeud(h, Noeud(parfait(h-1)), Noeud(parfait(h-1)))

n1 = Noeud(1)

n1.creer_enfant_gauche(2)

n1.creer_enfant_droit(3)

n2 = Noeud(1)

n2.creer_enfant_gauche(2)

n2.creer_enfant_droit(3)

tutor()