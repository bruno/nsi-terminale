---
title: "Structure de données : Arbres"
author: 
date: ""
subject: "Markdown"
keywords: [Markdown]
lang: "fr"
geometry:
- top=20mm
- left=20mm
- right=20mm
...


## Exercice 1 : représenter

**Dessiner** tous les arbres binaires ayant respectivement 3 et 4 nœuds.


## Exercice 2 : exécuter un algorithme

1. Appliquer les trois algorithmes de parcours en profondeur (préfixe, postfixe et infixe)
   depuis la racine des arbres binaires suivants, en montrant les valeurs aﬀichées.

2. Trouver un arbre binaire dont le parcours préfixe est A, B, C, D, E, F et le parcours 
infixe est C, B, E, D, F, A. (Attention, on cherche bien un seul arbre qui admet ces deux 
parcours à la fois !).

![](exercices-parcours-arbres.png){width=400px}

3. Trouver deux arbres binaires différents dont le parcours préfixe est
A, B, D, C, E, G, F et le parcours postfixe est D, B, G, E, F, C, A.

Pour les exercices 3 et 4 on utilisera l'implémentation d'un nœud telle que 
définie ci-dessous :

```python
class Noeud :
   """classe dont les attributs gauche et droit sont eux-mêmes des objets de 
   la classe Noeud"""
   def __init__(self, valeur : int, gauche = None, droit = None) :
      self.valeur = valeur
      self.gauche = None
      self.droit = None
```
## Exercice 3 : comparer deux arbres

** Écrire** pour la classe `Noeud`, une méthode `__eq__` permettant de tester 
l'égalité entre deux arbres binaires (cette méthode est celle appelée par 
l'opérateur `==`).

## Exercice 4 : générer un arbre

**Écrire** une fonction `parfait(h)` qui reçoit en argument un entier `h` supérieur ou 
égal à zéro et renvoie un arbre binaire parfait de hauteur `h`. La valeur affectée à chaque
nœud pourra être un entier correspondant à son ordre d'apparition.

