/* création de la table auteur*/
DROP TABLE IF EXISTS auteur;            -- au cas où une table auteur existerait déjà
CREATE TABLE auteur (                   
    id INTEGER PRIMARY KEY NOT NULL,       -- on définit l'attribut id comme clef primaire
    nom TEXT NOT NULL,                   -- le NOT NULL signifie qu'on n'accepte pas d'absence de valeur pour tel attribut
    prenom TEXT NOT NULL, 
    ann_naiss INTEGER,
    langue_ecrit TEXT         
    );

/* création de la table livre*/
DROP TABLE IF EXISTS livre;            -- au cas où une table livre existerait déjà
CREATE TABLE livre (                   -- DROP TABLE livre supprime la table livre (là on le fait seulement si elle existe)
    id INTEGER PRIMARY KEY NOT NULL,       -- on définit l'attribut id comme clef primaire
    titre TEXT NOT NULL,                   -- le NOT NULL signifie qu'on n'accepte pas d'absence de valeur pour tel attribut
    id_auteur INTEGER REFERENCES auteur (id) NOT NULL,  -- on définit l'attribut id_auteur comme clef étrangère
    ann_publi INTEGER,
    note INTEGER,
    CHECK (note>=0 AND note<=10)           -- pour spécifier que la note devra être comprise entre 0 et 10
    );

/* remplissage de la table auteur */
INSERT INTO auteur
VALUES 
(1,'Orwell','Georges',1903,'anglais'),
(2,'Herbert','Frank',1920,'anglais'),
(3,'Asimov','Isaac',1920,'anglais'),
(4,'Bradbury','Ray',1920,'anglais'),
(5,'K. Dick','Philip',1928,'anglais'),
(6,'Barjavel','René',1911,'français'),
(7,'Boulle','Pierre',1912,'français'),
(8,'Verne','Jules',1828,'français'),
(9,'Daudet','Alphonse',1840,'français');


/* remplissage de la table livre */
INSERT INTO livre
VALUES
(1,'1984',1,1949,10),
(2,'Dune',2,1965,8),
(3,'Fondation',3,1951,9),
(4,'Fahrenheit 451',4,1953,7),
(5,'Ubik',5,1969,9),
(6,'Chroniques martiennes',4,1950,8),
(7,'La nuit des temps',6,1968,7),
(8,'Blade Runner',5,1968,8),
(9,'Les Robots',3,1950,9),
(10,'La planète des singes',7,1963,8),
(11,'Ravage',6,1943,8),
(12,'Le Maître du Haut Château',5,1962,8),
(13,'La fin de l''éternité',3,1955,8),
(14,'De la Terre à la Lune',8,1865,10),
(15, 'Les lettres de mon moulin',9,1869,6);
