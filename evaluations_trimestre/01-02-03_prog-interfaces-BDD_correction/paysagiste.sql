CREATE TABLE client (
    id_client INT PRIMARY KEY,
    nom_client TEXT,
    tel TEXT,
    ville TEXT
);

CREATE TABLE employe (
    id_employe INT PRIMARY KEY,
    nom_employe TEXT,
);

CREATE TABLE Prestation (
    id INT PRIMARY KEY,
    id_client INT REFERENCES client (id_client),
    date TEXT,
    h_debut TEXT,
    duree INT,
    type TEXT,
    id_employe INT REFERENCES employe (id_employe),
);