exo1 : 2022, Métropole, sujet 1, ex3
exo2 : 2022, Métropole, sujet 1, ex4
exo3 : 2022, Amérique du Nord (mai), sujet 2

Ex1: 5/6
1. a) 1/1
   b) 1/1
2. 0,5/1 -> manque unité du débit
3. a) 1/1,5 -> pas de justification
   b) 1,5/1,5

Ex2: 3,5/8
1. 0/0,5
2. 1/1
3. 0,5/0,5
4. 0,5/1
5. 0/1
6. 0,5/0,5
7. 0/1
8. 1/2

Ex3: 4/6
1. 1,5/2
2. a)  0,5/1
   b) 1/1
3. 1/2
