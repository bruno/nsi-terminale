def tri_fusion(L : list) -> list:
    n = len(L)
    if n<=1:
        return L
    mg = moitie_gauche(L)
    md = moitie_droite(L)
    L1 = tri_fusion(mg)
    L2 = tri_fusion(md)
    return fusion(L1, L2)

