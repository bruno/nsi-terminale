class Noeud:
    def __init__(self, val = None):
        self.valeur = val
        self.gauche = None
        self.droit = None
        

a = Noeud(4)
a.gauche = Noeud(5)
a.droit = Noeud(14)
a.gauche.droit = Noeud(8)

def contient(arbre ,x : int) ->  bool:
    if arbre is None :
        return False
    if arbre.valeur == x :
        return True
    return contient(arbre.gauche,x) or contient(arbre.droit,x)

def chemin(arbre : object,n :object) -> list:
    if arbre is None :
        return []
    else :
        if arbre.valeur == n :
            return [arbre.valeur]
        elif contient(arbre.gauche,n):
            return [arbre.valeur] + chemin(arbre.gauche, n)
        elif contient(arbre.droit,n) :
            return [arbre.valeur] + chemin(arbre.droit, n)

def ppac(arbre, n1, n2) :
    chemin_n1 = chemin(arbre, n1)
    chemin_n2 = chemin(arbre, n2)
    i = 0
    while chemin_n1[i] == chemin_n2[i] :
        i += 1
    return chemin_n1[i]

def distance(arbre, n1, n2) -> int :
    ancetre_commun = ppac(arbre, n1, n2)
    d1 = len(chemin(arbre, n1))
    d2 = len(chemin(arbre, n2))
    d_ac = len(chemin(arbre,ancetre_commun))
    return d1 + d2 - 2*d_ac
