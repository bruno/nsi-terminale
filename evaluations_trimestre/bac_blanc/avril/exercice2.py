tab_itineraires=[]
def cherche_itineraires(G, start, end, chaine=[]):
    chaine = chaine + [start]
    if start == end :
        return chaine
    for u in G[start] :
        if u not in chaine:
            nchemin = cherche_itineraires(G, u, end, chaine)
            if len(nchemin) != 0:
                tab_itineraires.append(nchemin)
    return []

def itineraires_court(G,dep,arr):
    cherche_itineraires(G, dep, arr)
    tab_court = ...
    mini = float('inf')
    for v in tab_itineraires:
        if len(v) <= ... :
            mini = ...  
    for v in tab_itineraires:
        if len(v) == mini:
            tab_court.append(...)
    return tab_court

G2 = {
    'A':['B','C','H'],
    'B':['A','I'],
    'C':['A','D','E'],
    'D':['C','E'],
    'E':['C','D','G'],
    'F':['I','G'],
    'G':['E','H','F'],
    'H':['A','G','I'],
    'I':['B','F','H']
}

iti = itineraires_court(G2,'A','F')
print(iti)
print(tab_itineraires)