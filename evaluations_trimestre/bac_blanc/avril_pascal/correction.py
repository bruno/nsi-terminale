g1 = [
    [0, 4, 0, 0, 4, 0, 0],
    [4, 0, 0, 0, 0, 7, 5],
    [0, 0, 0, 4, 8, 0, 0],
    [0, 0, 4, 0, 6, 8, 0],
    [4, 0, 8, 6, 0, 0, 0],
    [0, 7, 0, 8, 0, 0, 3],
    [0, 5, 0, 0, 0, 3, 0]
]

g2 = {
    'A': ['B', 'C', 'H'],
    'B': ['A', 'I'],
    'C': ['A', 'D', 'E'],
    'D': ['C', 'E'],
    'E': ['C', 'D', 'G'], 
    'F': ['I', 'G'],
    'G': ['E', 'H', 'F'], 
    'H': ['A', 'I', 'G'], 
    'I': ['B', 'H', 'F']
}

tab_itineraires=[]
def cherche_itineraires(G, start, end, chaine=[]):
    chaine = chaine + [start]
    if start == end :
        return chaine
    for u in G[start] :
        if u not in chaine:
            nchemin = cherche_itineraires(G, u, end, chaine)
            if len(nchemin) != 0:
                tab_itineraires.append(nchemin)
    return []

def itineraires_court(G,dep,arr):
    cherche_itineraires(G, dep, arr)
    tab_court = []      # initialement, il n'y a aucun...
    mini = float('inf') # ...itinéraires le plus court
    for v in tab_itineraires:
        if len(v) <= mini : # `mini` est la taille du...
            mini = len(v)   #  ...plus petit tableau...
    for v in tab_itineraires:# ...parcourus jusqu'à présent
        if len(v) == mini:
            tab_court.append(v)  # ajoute le tableau de taille minimale
    return tab_court

G2 = {
    'A':['B','C','H'],
    'B':['A','I'],
    'C':['A','D','E'],
    'D':['C','E'],
    'E':['C','D','G'],
    'F':['I','G'],
    'G':['E','H','F'],
    'H':['A','G','I'],
    'I':['B','F','H']
}

iti = itineraires_court(G2,'A','F')
print(iti)
print(tab_itineraires)
