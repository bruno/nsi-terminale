""" correction ex3
extrait : sujet 2023, Journée 1
"""

class Aliment:
    def __init__(self, e, p, g, l):
        self.energie = e
        self.proteines = p
        self.glucides = g
        self.lipides = l

    def energie_reelle(self , masse):
        """
        énergie du lait :245 * 65.1 / 100
        """
        return masse * self.energie / 100

# 1.a
lait = Aliment(65.1, 3.32, 4.85, 3.63)

# 1.b
print("énergie du lait:", lait.energie)

# 1.c
print("protéine avant:", lait.proteines)
lait.proteines = 3.4
print("protéine après", lait.proteines)

# 2
print(round(lait.energie_reelle(245),3))

# 3a
nutrition = {
    'lait' : Aliment(65.1,3.4,4.85,3.63),
    'farine' : Aliment(343,11.7,69.3,0.8),
    'huile' : Aliment(900,0,0,100)
}

lait_dico = nutrition['lait']
print("lait dico (énergie)", lait_dico.energie)
print(nutrition['lait'].energie )

# 3b
print(220 * nutrition['lait'].energie / 100 )

# 4
energie_lait = lait.energie_reelle(220)

farine = nutrition['farine']
energie_farine = farine.energie_reelle(230)

huile = nutrition['huile']
energie_huile = huile.energie_reelle(100)

energie_total = energie_lait + energie_farine + energie_huile
print(energie_total)
print(type(12))