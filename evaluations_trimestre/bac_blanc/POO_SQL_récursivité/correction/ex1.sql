PRAGMA foreign_keys = ON;
DROP TABLE IF EXISTS animal;
DROP TABLE IF EXISTS espece;
DROP TABLE IF EXISTS enclos;

CREATE TABLE enclos (
    num_enclos INTEGER,
    ecosystem TEXT,
    surface INTEGER,
    struct TEXT,
    date_entretien DATE,
    PRIMARY KEY (num_enclos)
    );

CREATE TABLE espece (
    nom_espece TEXT,
    classe TEXT,
    alimentation TEXT,
    num_enclos INTEGER,
    PRIMARY key (nom_espece),
    FOREIGN KEY (num_enclos) REFERENCES enclos(num_enclos)
    );

CREATE TABLE animal (
    id_animal INTEGER,
    nom TEXT,
    age INTEGER,
    taille DECIMAL,
    poids INTEGER,
    nom_espece TEXT,
    PRIMARY KEY (id_animal),
    FOREIGN KEY (nom_espece) REFERENCES espece(nom_espece)
    );



INSERT INTO animal (id_animal, nom, age, taille, poids, nom_espece) VALUES
    (145, 'Romy', 18, 2.3, 130, 'tigre du Bengale'),
    (52, 'Boris', 30, 1.10, 48, 'bonobo'),
    (225, 'Hervé', 6, 1.70, 100, 'panda')
     ;