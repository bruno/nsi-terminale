SELECT animal.nom, animal.nom_espece
FROM animal
JOIN espece ON animal.nom_espece = espece.nom_espece
JOIN enclos ON espece.num_enclos = enclos.num_enclos
WHERE enclos.struct = 'vivarium' 
AND espece.alimentation = 'carnivore';

SELECT COUNT(animal.id_animal)
FROM animal
JOIN espece ON animal.nom_espece = espece.nom_espece
WHERE espece.classe = 'oiseaux';