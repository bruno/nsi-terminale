def tester_palindrome(chaine : str) -> bool :
    if len(chaine) < 2 :
        return True
    elif chaine[0] != chaine[-1] :
        return False
    else :
        chaine = chaine[1:-1]
        return tester_palindrome(chaine)

def est_palindrome(chaine : str) -> bool :
    while len(chaine) > 2 :
        if chaine[0] != chaine[-1] :
            return False
        chaine = chaine[1:-1]
    return True
