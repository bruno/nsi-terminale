def affich_seq(sequence : File) -> None :
    stock = creer_file_vide()
    ajout(sequence)
    while not est_vide(sequence) :
        c = ...
        ...
        time.sleep(0.5)
        ...
    while ... :
        ...

def affich_seq(sequence : File) -> None :
    stock = creer_file_vide()
    ajout(sequence)
    while not est_vide(sequence) :
        c = defiler(sequence)
        affichage(c)
        time.sleep(0.5)
        enfiler(stock, c)
    while not est_vide(stock) :
        enfiler(sequence, defiler(stock))